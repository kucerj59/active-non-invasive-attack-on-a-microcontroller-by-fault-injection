# Jakub Kučera
# kucerj59@fit.cvut.cz
# Created as part of my bachelor's thesis:
# Active non-invasive attack on a microcontroller by fault injection
# Czech Technical University - Faculty of Information Technology
# 2024

import os
import sys
import subprocess

last_arg = sys.argv[len(sys.argv) - 1]

usage = f"Usage: {sys.argv[0]} <path to Vivado executable> <path to Vitis executable> [vivado | xsa | vitis]"
usage += f"\nFor example {sys.argv[0]} C:\\Vivado\\2023.2\\bin\\vivado.bat C:\\Vitis\\2023.2\\bin\\vitis.bat vivado xsa"

if len(sys.argv) > 1 and (last_arg == "-help" or last_arg == "-h" or last_arg == "--help"):
    print(usage)
    sys.exit(0)

if len(sys.argv) < 3:
    print("Invalid argument count")
    print(usage)
    sys.exit(2)

if len(sys.argv) > 3:
    additional_args = sys.argv[3:]
else:
    additional_args = ["vivado", "xsa", "vitis"]

script_dir = project_dir = os.path.dirname(os.path.realpath(sys.argv[0]))

project_dir_dir = os.path.join(script_dir, "project_glitch")
if not os.path.exists(project_dir_dir):
    os.makedirs(project_dir_dir)
os.chdir(project_dir_dir)

if "vivado" in additional_args:
    result = subprocess.run([str(sys.argv[1]), "-mode", "batch", "-source", os.path.join(script_dir, "vivado_create_project.tcl")])
    print(result)
    if result.returncode != 0:
        print("\nFailed to create Vivado project")
        sys.exit(1)

if "xsa" in additional_args:
    result = subprocess.run([str(sys.argv[1]), "-mode", "batch", "-source", os.path.join(script_dir, "vivado_build_and_export.tcl")])
    print(result)
    if result.returncode != 0:
        print("\nFailed to build and generate hardware")
        sys.exit(1)

if "vitis" in additional_args:
    result = subprocess.run([str(sys.argv[2]), "-s", os.path.join(script_dir, "vitis_build.py")])
    print(result)
    if result.returncode != 0:
        print("\nFailed to create Vitis projects and build them")
        sys.exit(1)

print("\nSuccess")