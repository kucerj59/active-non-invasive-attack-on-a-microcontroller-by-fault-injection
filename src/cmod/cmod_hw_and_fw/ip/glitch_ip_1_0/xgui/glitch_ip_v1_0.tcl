
# Loading additional proc with user specified bodies to compute parameter values.
source [file join [file dirname [file dirname [info script]]] gui/glitch_ip_v1_0.gtcl]

# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "C_S00_AXI_DATA_WIDTH" -parent ${Page_0} -widget comboBox
  ipgui::add_param $IPINST -name "C_S00_AXI_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S00_AXI_BASEADDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S00_AXI_HIGHADDR" -parent ${Page_0}

  ipgui::add_param $IPINST -name "EXTERNAL_TRIGGER_WIDTH"
  set GLITCH_DURATION_WIDTH [ipgui::add_param $IPINST -name "GLITCH_DURATION_WIDTH"]
  set_property tooltip {Bits [Glitch Duration Width - 1 : 0] of elements in array.} ${GLITCH_DURATION_WIDTH}
  set GLITCH_TYPE_WIDTH [ipgui::add_param $IPINST -name "GLITCH_TYPE_WIDTH"]
  set_property tooltip {Lowest bit is used for clock glitch. Bits [Glitch Type Width + Glitch Delay Width - 1 : Glitch Delay Width] of elements in array} ${GLITCH_TYPE_WIDTH}
  set ARRAY_DATA_WIDTH [ipgui::add_param $IPINST -name "ARRAY_DATA_WIDTH"]
  set_property tooltip {Not an actual parameter. Value depends on Glitch Type Width and Glitch Duration Width.} ${ARRAY_DATA_WIDTH}
  set MAXIMUM_GLITCH_COUNT [ipgui::add_param $IPINST -name "MAXIMUM_GLITCH_COUNT"]
  set_property tooltip {Not an actual parameter. Value depends on C S00 AXI DATA WIDTH and Array Data Width.} ${MAXIMUM_GLITCH_COUNT}
  set GLITCH_ARRAY_OFFSET [ipgui::add_param $IPINST -name "GLITCH_ARRAY_OFFSET"]
  set_property tooltip {Not an actual parameter. Value depends on C S00 AXI DATA WIDTH and Array Data Width.} ${GLITCH_ARRAY_OFFSET}

}

proc update_PARAM_VALUE.ARRAY_DATA_WIDTH { PARAM_VALUE.ARRAY_DATA_WIDTH PARAM_VALUE.GLITCH_TYPE_WIDTH PARAM_VALUE.GLITCH_DURATION_WIDTH } {
	# Procedure called to update ARRAY_DATA_WIDTH when any of the dependent parameters in the arguments change
	
	set ARRAY_DATA_WIDTH ${PARAM_VALUE.ARRAY_DATA_WIDTH}
	set GLITCH_TYPE_WIDTH ${PARAM_VALUE.GLITCH_TYPE_WIDTH}
	set GLITCH_DURATION_WIDTH ${PARAM_VALUE.GLITCH_DURATION_WIDTH}
	set values(GLITCH_TYPE_WIDTH) [get_property value $GLITCH_TYPE_WIDTH]
	set values(GLITCH_DURATION_WIDTH) [get_property value $GLITCH_DURATION_WIDTH]
	set_property value [gen_USERPARAMETER_ARRAY_DATA_WIDTH_VALUE $values(GLITCH_TYPE_WIDTH) $values(GLITCH_DURATION_WIDTH)] $ARRAY_DATA_WIDTH
}

proc validate_PARAM_VALUE.ARRAY_DATA_WIDTH { PARAM_VALUE.ARRAY_DATA_WIDTH } {
	# Procedure called to validate ARRAY_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.GLITCH_ARRAY_OFFSET { PARAM_VALUE.GLITCH_ARRAY_OFFSET PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to update GLITCH_ARRAY_OFFSET when any of the dependent parameters in the arguments change
	
	set GLITCH_ARRAY_OFFSET ${PARAM_VALUE.GLITCH_ARRAY_OFFSET}
	set C_S00_AXI_ADDR_WIDTH ${PARAM_VALUE.C_S00_AXI_ADDR_WIDTH}
	set values(C_S00_AXI_ADDR_WIDTH) [get_property value $C_S00_AXI_ADDR_WIDTH]
	set_property value [gen_USERPARAMETER_GLITCH_ARRAY_OFFSET_VALUE $values(C_S00_AXI_ADDR_WIDTH)] $GLITCH_ARRAY_OFFSET
}

proc validate_PARAM_VALUE.GLITCH_ARRAY_OFFSET { PARAM_VALUE.GLITCH_ARRAY_OFFSET } {
	# Procedure called to validate GLITCH_ARRAY_OFFSET
	return true
}

proc update_PARAM_VALUE.MAXIMUM_GLITCH_COUNT { PARAM_VALUE.MAXIMUM_GLITCH_COUNT PARAM_VALUE.C_S00_AXI_ADDR_WIDTH PARAM_VALUE.ARRAY_DATA_WIDTH } {
	# Procedure called to update MAXIMUM_GLITCH_COUNT when any of the dependent parameters in the arguments change
	
	set MAXIMUM_GLITCH_COUNT ${PARAM_VALUE.MAXIMUM_GLITCH_COUNT}
	set C_S00_AXI_ADDR_WIDTH ${PARAM_VALUE.C_S00_AXI_ADDR_WIDTH}
	set ARRAY_DATA_WIDTH ${PARAM_VALUE.ARRAY_DATA_WIDTH}
	set values(C_S00_AXI_ADDR_WIDTH) [get_property value $C_S00_AXI_ADDR_WIDTH]
	set values(ARRAY_DATA_WIDTH) [get_property value $ARRAY_DATA_WIDTH]
	set_property value [gen_USERPARAMETER_MAXIMUM_GLITCH_COUNT_VALUE $values(C_S00_AXI_ADDR_WIDTH) $values(ARRAY_DATA_WIDTH)] $MAXIMUM_GLITCH_COUNT
}

proc validate_PARAM_VALUE.MAXIMUM_GLITCH_COUNT { PARAM_VALUE.MAXIMUM_GLITCH_COUNT } {
	# Procedure called to validate MAXIMUM_GLITCH_COUNT
	return true
}

proc update_PARAM_VALUE.EXTERNAL_TRIGGER_WIDTH { PARAM_VALUE.EXTERNAL_TRIGGER_WIDTH } {
	# Procedure called to update EXTERNAL_TRIGGER_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.EXTERNAL_TRIGGER_WIDTH { PARAM_VALUE.EXTERNAL_TRIGGER_WIDTH } {
	# Procedure called to validate EXTERNAL_TRIGGER_WIDTH
	return true
}

proc update_PARAM_VALUE.GLITCH_DURATION_WIDTH { PARAM_VALUE.GLITCH_DURATION_WIDTH } {
	# Procedure called to update GLITCH_DURATION_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GLITCH_DURATION_WIDTH { PARAM_VALUE.GLITCH_DURATION_WIDTH } {
	# Procedure called to validate GLITCH_DURATION_WIDTH
	return true
}

proc update_PARAM_VALUE.GLITCH_TYPE_WIDTH { PARAM_VALUE.GLITCH_TYPE_WIDTH } {
	# Procedure called to update GLITCH_TYPE_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GLITCH_TYPE_WIDTH { PARAM_VALUE.GLITCH_TYPE_WIDTH } {
	# Procedure called to validate GLITCH_TYPE_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to update C_S00_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to validate C_S00_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to update C_S00_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_S00_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_BASEADDR { PARAM_VALUE.C_S00_AXI_BASEADDR } {
	# Procedure called to update C_S00_AXI_BASEADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_BASEADDR { PARAM_VALUE.C_S00_AXI_BASEADDR } {
	# Procedure called to validate C_S00_AXI_BASEADDR
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_HIGHADDR { PARAM_VALUE.C_S00_AXI_HIGHADDR } {
	# Procedure called to update C_S00_AXI_HIGHADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_HIGHADDR { PARAM_VALUE.C_S00_AXI_HIGHADDR } {
	# Procedure called to validate C_S00_AXI_HIGHADDR
	return true
}


proc update_MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH PARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.EXTERNAL_TRIGGER_WIDTH { MODELPARAM_VALUE.EXTERNAL_TRIGGER_WIDTH PARAM_VALUE.EXTERNAL_TRIGGER_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.EXTERNAL_TRIGGER_WIDTH}] ${MODELPARAM_VALUE.EXTERNAL_TRIGGER_WIDTH}
}

proc update_MODELPARAM_VALUE.GLITCH_TYPE_WIDTH { MODELPARAM_VALUE.GLITCH_TYPE_WIDTH PARAM_VALUE.GLITCH_TYPE_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GLITCH_TYPE_WIDTH}] ${MODELPARAM_VALUE.GLITCH_TYPE_WIDTH}
}

proc update_MODELPARAM_VALUE.GLITCH_DURATION_WIDTH { MODELPARAM_VALUE.GLITCH_DURATION_WIDTH PARAM_VALUE.GLITCH_DURATION_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GLITCH_DURATION_WIDTH}] ${MODELPARAM_VALUE.GLITCH_DURATION_WIDTH}
}

