// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module target_clock_generator #(
    parameter integer DIVISOR_WIDTH = 32
    )
    (
    input wire clk,
    input wire [DIVISOR_WIDTH-1 : 0] divisor,
    output reg target_clk,
    output reg target_clk_posedge
    );

    reg [1 : 0] state;
    reg [1 : 0] next_state; // wire
    
    // State - target_clk posedge
    localparam STATE_CLK_LOW = 2'b00;
    localparam STATE_POSEDGE = 2'b01;
    localparam STATE_CLK_HIGH = 2'b10;

    reg [DIVISOR_WIDTH-1 : 0] counter;
    
    always @(*)
      begin
        next_state = state;
        case (state)
            STATE_CLK_LOW:
              begin
                if (counter + 1 >= divisor)
                  begin
                    next_state = STATE_POSEDGE;
                  end
              end
            STATE_POSEDGE:
              begin
                // Set posedge high one cycle before target clock's rising edge
                next_state = STATE_CLK_HIGH;
              end
            STATE_CLK_HIGH:
              begin
                // Set clock to low conditions
                // Handle case when posedge is every other cycle
                if (divisor < 2)
                  begin
                    next_state = STATE_POSEDGE;
                  end
                else if (counter >= divisor)
                  begin
                    next_state = STATE_CLK_LOW;
                  end
              end
            default:
              begin
                next_state = STATE_CLK_LOW;
              end
        endcase
      end
    
    always @(posedge clk)
      begin
        if (next_state == state)
          begin
            counter <= counter + 1;
          end
        else
          begin
            counter <= 1;
          end
      
        state <= next_state;
        
        case (next_state)
            STATE_CLK_LOW:
              begin
                target_clk <= 0;
                target_clk_posedge <= 0;
              end
            STATE_POSEDGE:
              begin
                target_clk <= 0;
                target_clk_posedge <= 1;
              end
            STATE_CLK_HIGH:
              begin
                target_clk <= 1;
                target_clk_posedge <= 0;
              end
          endcase
      end
      
    // Simulation
    initial
      begin
        state = STATE_CLK_LOW;
        target_clk = 1;
        counter = 1;
      end
    
endmodule
