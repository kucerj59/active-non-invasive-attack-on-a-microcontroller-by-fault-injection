// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module fifo_adapter#
    (
    parameter integer COUNT_WIDTH = 32,
    parameter integer ADDR_WIDTH = 4,
    parameter integer DATA_WIDTH = 32
    )
    (
    input wire clk,
    input wire [DATA_WIDTH-1 : 0] array_data,
    input wire next,
    input wire reset,
    input wire [COUNT_WIDTH-1 : 0] count,
    output wire [ADDR_WIDTH-1 : 0] array_addr,
    output wire [DATA_WIDTH-1 : 0] data,
    output reg empty
    );
    
    reg [ADDR_WIDTH-1 : 0] address;
    
    assign data = array_data;
    // React immediately to the change of the input next
    assign array_addr = address + next;
    
    always @(posedge clk)
      begin
        if (reset)
          begin
            address <= 0;
            empty <= (count == 0);
          end
        else if (empty)
          begin
            empty <= 1;
          end
        else if (next)
          begin
            address <= array_addr;
            
            if (array_addr == count)
              begin
                empty <= 1;
              end
          end
      end
    
endmodule
