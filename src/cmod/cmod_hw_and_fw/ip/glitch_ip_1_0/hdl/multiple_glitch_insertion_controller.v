// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module multiple_glitch_insertion_controller #(
    parameter integer GLITCH_DURATION_WIDTH = 24,
    parameter integer GLITCH_TYPE_WIDTH = 8
    )
    (
    input wire clk,
    input wire trigger,
    input wire [GLITCH_DURATION_WIDTH-1 : 0] glitch_duration,
    input wire [GLITCH_TYPE_WIDTH-1 : 0] glitch_type_in,
    input wire fifo_empty,
    output reg [GLITCH_TYPE_WIDTH-1 : 0] glitch_type_out,
    output reg done,
    output reg fifo_next,
    output reg fifo_reset
    );
    
    reg [2 : 0] state;
    reg [2 : 0] next_state; // wire
    
    localparam STATE_WAITING = 3'd0;
    localparam STATE_PREFETCHING = 3'd1;
    localparam STATE_GLITCH_NEW_END = 3'd2;
    localparam STATE_GLITCH_NEW = 3'd3;
    localparam STATE_GLITCH_CONTINUE = 3'd4;
    localparam STATE_GLITCH_END = 3'd5;
    localparam STATE_DONE = 3'd6;
    
    reg [GLITCH_DURATION_WIDTH-1 : 0] glitch_duration_reg;
    
    always @(*)
      begin
        next_state = state;
        case (state)
            STATE_WAITING:
              begin
                if (trigger)
                  begin
                    next_state = STATE_PREFETCHING;
                  end
              end
            // Enter new state depending on it's duration
            STATE_PREFETCHING, STATE_GLITCH_NEW_END, STATE_GLITCH_END:
              begin
                if (~trigger)
                  begin
                    next_state = STATE_WAITING;
                  end
                else if (fifo_empty)
                  begin
                    next_state = STATE_DONE;
                  end
                // Next glitch is longer than 1 cycle
                else if (glitch_duration > 1)
                  begin
                    next_state = STATE_GLITCH_NEW;
                  end
                // (glitch_duration <= 1)
                // Duration of the next glitch is 1 cycle
                else
                  begin
                    next_state = STATE_GLITCH_NEW_END;
                  end
              end
            // Enter the final state of a glitch or continue with the same glitch
            STATE_GLITCH_NEW, STATE_GLITCH_CONTINUE:
              begin
                if (~trigger)
                  begin
                    next_state = STATE_WAITING;
                  end
                else if (glitch_duration_reg > 2)
                  begin
                    next_state = STATE_GLITCH_CONTINUE;
                  end
                // (glitch_duration_reg <= 2)
                else
                  begin
                   next_state = STATE_GLITCH_END;
                  end
              end
            STATE_DONE:
              begin
                if (~trigger)
                  begin
                    next_state = STATE_WAITING;
                  end
              end
            default:
              begin
                next_state = STATE_WAITING;
              end
        endcase
      end
    
    always @(posedge clk)
      begin
        state <= next_state;
        
        case (next_state)
            // Wait for trigger
            STATE_WAITING:
              begin
                glitch_type_out <= 0;
                fifo_reset <= 1;
                fifo_next <= 0;
                glitch_duration_reg <= glitch_duration;
                done <= 0;
              end
            // Request next duration and type
            STATE_PREFETCHING:
              begin
                glitch_type_out <= 0;
                fifo_reset <= 0;
                fifo_next <= 1;
                glitch_duration_reg <= glitch_duration;
                done <= 0;
              end
            // Read glitch type of length 1
            STATE_GLITCH_NEW_END:
              begin
                glitch_type_out <= glitch_type_in;
                fifo_reset <= 0;
                  fifo_next <= 1;
                glitch_duration_reg <= glitch_duration;
                done <= 0;
            end
            // Read glitch type of length > 1
            STATE_GLITCH_NEW:
              begin
                glitch_type_out <= glitch_type_in;
                fifo_reset <= 0;
                fifo_next <= 0;
                glitch_duration_reg <= glitch_duration;
                done <= 0;
              end
            // Keep glitching and update duration
            STATE_GLITCH_CONTINUE:
              begin
                glitch_type_out <= glitch_type_out;
                fifo_reset <= 0;
                fifo_next <= 0;
                glitch_duration_reg <= glitch_duration_reg - 1;
                done <= 0;
              end
            STATE_GLITCH_END:
              begin
                glitch_type_out <= glitch_type_out;
                fifo_reset <= 0;
                fifo_next <= 1;
                glitch_duration_reg <= glitch_duration;
                done <= 0;
              end
            STATE_DONE:
              begin
                glitch_type_out <= 0;
                fifo_reset <= 0;
                fifo_next <= 0;
                glitch_duration_reg <= glitch_duration;
                done <= 1;
              end
        endcase
      end
    
    // Simulation
    initial
      begin
        state = STATE_WAITING;
      end
    
endmodule
