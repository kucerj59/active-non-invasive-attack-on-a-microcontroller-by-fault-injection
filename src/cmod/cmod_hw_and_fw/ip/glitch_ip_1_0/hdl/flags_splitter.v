// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module flags_splitter #(
    parameter integer FLAGS_WIDTH = 16,
    parameter integer EXTERNAL_TRIGGER_WIDTH = 1
    )
    (
    input [FLAGS_WIDTH-1:0] flags,
    output flag_skip_offset_block,
    output flag_trig_man,
    output flag_trig_rst,
    output [EXTERNAL_TRIGGER_WIDTH-1 : 0] flag_trig_ext,
    output target_rst,
    output glitch_enable
    );
    
    // Split individual bits
    assign flag_skip_offset_block = flags[0];
    assign flag_trig_man = flags[2];
    assign flag_trig_rst = flags[3];
    assign flag_trig_ext = flags[EXTERNAL_TRIGGER_WIDTH + 3: 4];
    
    assign target_rst = flags[FLAGS_WIDTH - 2];
    assign glitch_enable = flags[FLAGS_WIDTH - 1];
    
endmodule
