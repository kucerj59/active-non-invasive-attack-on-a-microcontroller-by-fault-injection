// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module status_register #(
    parameter integer DATA_WIDTH = 32,
    parameter integer EXTERNAL_TRIGGER_WIDTH = 1
    )
    (
    input clk,
    input reset,
    input target_reset,
    input glitch_trigger,
    input offset_done,
    input mgic_done,
    input [EXTERNAL_TRIGGER_WIDTH-1 : 0] external_trigger,
    output [DATA_WIDTH-1 : 0] data
    );
    
    reg [EXTERNAL_TRIGGER_WIDTH-1 : 0] external_trigger_reg;
    
    assign data = {{(DATA_WIDTH-EXTERNAL_TRIGGER_WIDTH-4){1'b0}},
        external_trigger_reg, mgic_done, offset_done, glitch_trigger, target_reset};
    
    always @(posedge clk)
      begin
        if (reset)
          begin
            external_trigger_reg <= external_trigger;
          end
        else
          begin
            external_trigger_reg <= external_trigger_reg | external_trigger;
          end
      end
    
endmodule