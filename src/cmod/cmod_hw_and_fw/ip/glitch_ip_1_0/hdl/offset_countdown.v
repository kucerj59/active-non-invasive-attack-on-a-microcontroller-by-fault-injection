// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module offset_countdown #(
    parameter integer DATA_WIDTH = 32
    )
    (
    input wire clk,
    input wire enable,
    input wire target_clk_posedge,
    input wire [DATA_WIDTH-1 : 0] offset,
    output reg done
    );
    
    reg [DATA_WIDTH-1 : 0] counter;
    
    always @(posedge clk)
      begin
        if (~enable)
          begin
            done <= 0;
            counter <= offset;
          end
        else
          begin
            // Set done high after waiting for # offset cycles
            if (target_clk_posedge && counter == 0)
              begin
                done <= 1;
              end
            // Decrease counter
            else if (target_clk_posedge && counter != 0)
              begin
                counter <= counter - 1;
              end
          end
      end
    
endmodule
