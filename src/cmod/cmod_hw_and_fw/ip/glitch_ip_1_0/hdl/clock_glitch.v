`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.03.2024 21:56:55
// Design Name: 
// Module Name: clock_glitch
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clock_glitch (
    input wire glitch,
    input wire target_clk_in,
    output wire target_clk_out
    );
    
    assign target_clk_out = target_clk_in ^ glitch;
    
endmodule
