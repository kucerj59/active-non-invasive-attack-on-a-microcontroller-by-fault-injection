`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.03.2024 12:58:53
// Design Name: 
// Module Name: asymmetric_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// Be careful when editing this file, BRAM should be inferable from the code
module asymmetric_memory #(
    // WRITE_DATA_WIDTH should be greater than or equal to READ_DATA_WIDTH
    // Also the following equation should hold true
    // 2 ** WRITE_ADDR_WIDTH * WRITE_DATA_WIDTH = 2 ** READ_ADDR_WIDTH * READ_DATA_WIDTH
    parameter integer WRITE_ADDR_WIDTH = 10,
    parameter integer WRITE_DATA_WIDTH = 32,
    parameter integer READ_ADDR_WIDTH = 12,
    parameter integer READ_DATA_WIDTH = 8
    )
    (
    input wire clk,
    input wire write_enable,
    input wire [WRITE_ADDR_WIDTH-1 : 0] write_addr,
    input wire [WRITE_DATA_WIDTH-1 : 0] write_data,
    input wire [READ_ADDR_WIDTH-1 : 0] read_addr,
    output reg [READ_DATA_WIDTH-1 : 0] read_data
    );
    
    localparam integer WRITE_DATA_WIDTH_BYTES = WRITE_DATA_WIDTH / 8;
    localparam integer READ_DATA_WIDTH_BYTES = READ_DATA_WIDTH / 8;
    
    localparam integer RATIO = WRITE_DATA_WIDTH / READ_DATA_WIDTH;
    localparam integer CLOG_RATIO = $clog2(RATIO);
    
    // A BRAM might be inferred
    reg [READ_DATA_WIDTH-1 : 0] mem [2 ** (READ_ADDR_WIDTH)-1 : 0];
    
    // Equal width of both ports
    if (RATIO == 1)
      begin
        always @(posedge clk)
          begin          
            if (write_enable)
              begin
                mem[write_addr] <= write_data;
              end
          end
      end
    // Write port is wider than read port
    else
      begin
        reg [CLOG_RATIO-1 : 0] lsb_addr;
        integer i;
        always @(posedge clk)
          begin
            for (i = 0; i < RATIO; i = i + 1)
              begin
                lsb_addr = i;
                                
                if (write_enable)
                  begin
                    mem[{write_addr, lsb_addr}] <= write_data[i * READ_DATA_WIDTH +: READ_DATA_WIDTH];
                  end
              end
          end
      end
    
    always @(posedge clk)
      begin
        read_data <= mem[read_addr];
      end
    
endmodule
