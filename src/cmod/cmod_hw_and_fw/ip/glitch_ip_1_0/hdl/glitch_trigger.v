// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module glitch_trigger #(
    parameter integer EXTERNAL_TRIGGER_WIDTH = 1
    )
    (
    input wire clk,
    input wire enable,
    input wire flag_trig_man,
    input wire flag_trig_rst,
    input wire trig_rst_enable,
    input wire [EXTERNAL_TRIGGER_WIDTH-1 : 0] flag_trig_ext,
    input wire [EXTERNAL_TRIGGER_WIDTH-1 : 0] trig_ext_enable,
    output wire trigger
    );
    
    reg [EXTERNAL_TRIGGER_WIDTH-1 : 0] trig_ext_enable_reg;
    // Old signal value used to detect rising edges
    reg [EXTERNAL_TRIGGER_WIDTH-1 : 0] trig_ext_enable_old;
    wire [EXTERNAL_TRIGGER_WIDTH-1 : 0] trig_ext_rising_edge;
    assign trig_ext_rising_edge = (~trig_ext_enable_old) & trig_ext_enable;
    
    // Detects rising edges
    always @(posedge clk)
      begin
        trig_ext_enable_old <= trig_ext_enable;
      end
    
    // Keep triggers high until reset after they are triggered
    always @(posedge clk)
      begin
        if (~enable)
          begin
            trig_ext_enable_reg <= 0;
          end
        else
          begin
            trig_ext_enable_reg <= trig_ext_enable_reg | trig_ext_rising_edge;
          end
      end
    
    // or logic of all trigger signals
    assign trig_man = flag_trig_man;
    assign trig_rst = flag_trig_rst & trig_rst_enable;
    assign trig_ext = (flag_trig_ext & trig_ext_enable_reg) != 0;
    
    assign trigger = enable & (trig_man | trig_rst | trig_ext);
    
endmodule
