// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps

module glitch_module #(
    parameter integer DATA_WIDTH = 32,
    parameter integer GLITCH_DURATION_WIDTH = 24,
    parameter integer GLITCH_TYPE_WIDTH = 8,
    parameter integer EXTERNAL_TRIGGER_WIDTH = 1
    )
    (
    input wire clk,
    input wire [DATA_WIDTH-1 : 0] offset,
    input wire [DATA_WIDTH-1 : 0] flags,
    input wire [DATA_WIDTH-1 : 0] divisor,
    input wire [GLITCH_DURATION_WIDTH-1 : 0] glitch_duration,
    // Least significant bit of glitch_type is controls clock glitch insertion
    input wire [GLITCH_TYPE_WIDTH-1 : 0] glitch_type,
    input wire fifo_empty,
    input wire [EXTERNAL_TRIGGER_WIDTH-1 : 0] external_trigger,
    output wire target_clk,
    output wire target_reset_n,
    output wire [GLITCH_TYPE_WIDTH-2 : 0] glitch,
    output wire fifo_next,
    output wire fifo_reset,
    output wire [DATA_WIDTH-1 : 0] status
    );
    
    wire [EXTERNAL_TRIGGER_WIDTH-1 : 0] flag_trig_ext;
    
    flags_splitter #(
        .FLAGS_WIDTH(DATA_WIDTH),
        .EXTERNAL_TRIGGER_WIDTH(EXTERNAL_TRIGGER_WIDTH)
    ) flags_splitter_inst (
        .flags(flags),
        .flag_skip_offset_block(flag_skip_offset_block),
        .flag_trig_man(flag_trig_man),
        .flag_trig_rst(flag_trig_rst),
        .flag_trig_ext(flag_trig_ext),
        .target_rst(target_reset_enable),
        .glitch_enable(glitch_enable)
    );
    
    target_clock_generator #(
        .DIVISOR_WIDTH(DATA_WIDTH)
    ) target_clock_generator_inst (
        .clk(clk),
        .divisor(divisor),
        .target_clk(original_target_clk),
        .target_clk_posedge(target_clk_posedge)
    );
    
    target_reset_module target_reset_module_inst (
        .clk(clk),
        .reset_enable(target_reset_enable),
        .target_clk_posedge(target_clk_posedge),
        .reset_n(target_reset_n),
        .reset(target_reset)
    );
    
    // Cascading registers to avoid metastability at trigger input
    wire [EXTERNAL_TRIGGER_WIDTH-1 : 0] stable_external_trigger;
    register_row #(
        .DATA_WIDTH(EXTERNAL_TRIGGER_WIDTH),
        .DEPTH(2)
    ) register_row_meta_inst (
        .clk(clk),
        .data_in(external_trigger),
        .data_out(stable_external_trigger)
    );
    
    glitch_trigger #(
        .EXTERNAL_TRIGGER_WIDTH(EXTERNAL_TRIGGER_WIDTH)
    ) glitch_trigger_inst (
        .clk(clk),
        .enable(glitch_enable),
        .flag_trig_man(flag_trig_man),
        .flag_trig_rst(flag_trig_rst),
        .trig_rst_enable(target_reset_n),
        .flag_trig_ext(flag_trig_ext),
        .trig_ext_enable(stable_external_trigger),
        .trigger(offset_countdown_trigger)
    );
    
    offset_countdown #(
        .DATA_WIDTH(DATA_WIDTH)
    ) offset_countdown_inst (
        .clk(clk),
        .enable(offset_countdown_trigger),
        .target_clk_posedge(target_clk_posedge),
        .offset(offset),
        .done(offset_countdown_done)
    );
    
    // Delay of glitch_type in clock cycles
    // introduced by multiple_glitch_insertion_controller
    localparam integer MGIC_DELAY = 2;
    wire mgic_trigger;
    assign mgic_trigger = (offset_countdown_done & ~flag_skip_offset_block) |
        (offset_countdown_trigger & flag_skip_offset_block);
    
    wire [GLITCH_TYPE_WIDTH-1 : 0] glitch_type_out;
    
    multiple_glitch_insertion_controller #(
        .GLITCH_DURATION_WIDTH(GLITCH_DURATION_WIDTH),
        .GLITCH_TYPE_WIDTH(GLITCH_TYPE_WIDTH)
    ) multiple_glitch_insertion_controller_inst (
        .clk(clk),
        .trigger(mgic_trigger),
        .glitch_duration(glitch_duration),
        .glitch_type_in(glitch_type),
        .fifo_empty(fifo_empty),
        .glitch_type_out(glitch_type_out),
        .fifo_next(fifo_next),
        .fifo_reset(fifo_reset),
        .done(mgic_done)
    );
    
    // Delay the target's clock to synchronize it with the glitches
    register_row #(
        .DATA_WIDTH(1),
        .DEPTH(MGIC_DELAY)
    ) register_row_inst (
        .clk(clk),
        .data_in(original_target_clk),
        .data_out(delayed_target_clk)
    );
    
    // Clock glitch
    clock_glitch clock_glitch_inst (
        .glitch(glitch_type_out[0]),
        .target_clk_in(delayed_target_clk),
        .target_clk_out(target_clk)
    );
    
    // Least significant bit of glitch_type is controls clock glitch insertion
    assign glitch = glitch_type_out[GLITCH_TYPE_WIDTH-1 : 1];
    
    status_register #(
        .DATA_WIDTH(DATA_WIDTH),
        .EXTERNAL_TRIGGER_WIDTH(EXTERNAL_TRIGGER_WIDTH)
    ) status_register_inst (
        .clk(clk),
        .reset(~glitch_enable),
        .target_reset(target_reset),
        .glitch_trigger(offset_countdown_trigger),
        .offset_done(offset_countdown_done & (~flag_skip_offset_block)),
        .mgic_done(mgic_done),
        .external_trigger(external_trigger),
        .data(status)
    );
    
endmodule
