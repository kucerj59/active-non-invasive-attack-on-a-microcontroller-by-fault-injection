// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module register_row #(
    parameter integer DATA_WIDTH = 1,
    parameter integer DEPTH = 2
    )
    (
    input wire clk,
    input wire [DATA_WIDTH-1 : 0] data_in,
    output wire [DATA_WIDTH-1 : 0] data_out
    );
    
    reg [DATA_WIDTH-1 : 0] registers [DEPTH-1 : 0];
    
    assign data_out = registers[DEPTH-1];
    
    integer i;
    always @(posedge clk)
      begin
        registers[0] <= data_in;
        
        for (i = 0; i < DEPTH - 1; i = i + 1)
          begin
            registers[i + 1] <= registers[i];
          end
      end
    
endmodule
