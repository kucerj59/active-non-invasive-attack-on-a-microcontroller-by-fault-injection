// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module target_reset_module(
    input wire clk,
    input wire reset_enable,
    input wire target_clk_posedge,
    output reg reset_n,
    output wire reset
    );
    
    assign reset = ~reset_n;
    
    always @(posedge clk)
      begin
        if (reset_enable)
          begin
            reset_n <= 0;
          end
        // Set high on rising edge
        else if (target_clk_posedge)
          begin
            reset_n <= 1;
          end
      end
    
endmodule
