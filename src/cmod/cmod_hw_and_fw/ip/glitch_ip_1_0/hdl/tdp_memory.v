// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps

// Be careful when editing this file, BRAM should be inferable from the code
module tdp_memory #(
    parameter integer ADDR_WIDTH = 10,
    parameter integer DATA_WIDTH = 32
    )
    (
    input wire clk,
    input wire write_enable,
    input wire [ADDR_WIDTH-1 : 0] write_addr,
    input wire [DATA_WIDTH-1 : 0] write_data,
    output reg [DATA_WIDTH-1 : 0] readw_data,
    input wire [ADDR_WIDTH-1 : 0] read_addr,
    output reg [DATA_WIDTH-1 : 0] read_data
    );
    
    // A BRAM might be inferred
    reg [DATA_WIDTH-1 : 0] mem [2 ** (ADDR_WIDTH)-1 : 0];
    
    always @(posedge clk)
      begin
        readw_data <= mem[write_addr];
      
        if (write_enable)
          begin
            mem[write_addr] <= write_data;
          end
      end

    always @(posedge clk)
      begin
        read_data <= mem[read_addr];
      end
    
endmodule
