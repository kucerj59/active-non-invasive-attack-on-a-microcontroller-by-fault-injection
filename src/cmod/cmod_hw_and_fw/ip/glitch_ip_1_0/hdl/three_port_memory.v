// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


`define MAX(a, b) \
    ((a >= b) ? a : b)

module three_port_memory #(
    parameter integer WRITE_READ1_ADDR_WIDTH = 10,
    parameter integer WRITE_READ1_DATA_WIDTH = 32,
    parameter integer WRITE_ENABLE_WIDTH = (WRITE_READ1_DATA_WIDTH + 7) / 8,
    parameter integer READ2_ADDR_WIDTH = 12,
    parameter integer READ2_DATA_WIDTH = 8
    )
    (
    input wire clk,
    input wire [WRITE_ENABLE_WIDTH-1 : 0] byte_write_enable,
    // Write address has to be set for 1 cycle before writing
    input wire [WRITE_READ1_ADDR_WIDTH-1 : 0] write_addr,
    input wire [WRITE_READ1_DATA_WIDTH-1 : 0] write_data,
    input wire [WRITE_READ1_ADDR_WIDTH-1 : 0] read1_addr,
    output wire [WRITE_READ1_DATA_WIDTH-1 : 0] read1_data,
    input wire [READ2_ADDR_WIDTH-1 : 0] read2_addr,
    output wire [READ2_DATA_WIDTH-1 : 0] read2_data
    );
    
    localparam integer WRITE_READ1_DATA_WIDTH_BYTES = WRITE_READ1_DATA_WIDTH / 8;
    // Count of asymmetric and dual port memory blocks
    localparam integer COUNT = (READ2_DATA_WIDTH + WRITE_READ1_DATA_WIDTH - 1) / WRITE_READ1_DATA_WIDTH;
    localparam integer CLOG_COUNT = $clog2(COUNT);
    
    // Split read1 addr into block select (bs) and block addres (ba)
    wire [WRITE_READ1_ADDR_WIDTH - CLOG_COUNT -1 : 0] ba_read1_addr;
    wire [`MAX(CLOG_COUNT-1, 1) : 0] bs_read1_addr;
    assign ba_read1_addr = read1_addr[WRITE_READ1_ADDR_WIDTH-1 : CLOG_COUNT];
    if (COUNT > 1)
        assign bs_read1_addr = read1_addr[CLOG_COUNT-1 : 0];
    else // Exception for COUNT == 1, always select block 0
        assign bs_read1_addr = 0;
        
    // Select output of a memory with msb
    wire [WRITE_READ1_DATA_WIDTH-1 : 0] read1_data_array [COUNT-1 : 0];
    assign read1_data = read1_data_array[bs_read1_addr];
    
    // Split write addr into block select (bs) and block addres (ba)
    wire [WRITE_READ1_ADDR_WIDTH - CLOG_COUNT -1 : 0] ba_write_addr;
    wire [`MAX(CLOG_COUNT-1, 1) : 0] bs_write_addr;
    assign ba_write_addr = write_addr[WRITE_READ1_ADDR_WIDTH-1 : CLOG_COUNT];
    if (COUNT > 1)
        assign bs_write_addr = write_addr[CLOG_COUNT-1 : 0];
    else // Exception for COUNT == 1, always select block 0
        assign bs_write_addr = 0;
        
    // Select readw output based on msb
    wire [WRITE_READ1_DATA_WIDTH-1 : 0] readw_data;
    wire [WRITE_READ1_DATA_WIDTH-1 : 0] readw_data_array [COUNT-1 : 0];
    assign readw_data = readw_data_array[bs_write_addr];
    
    // Simulate write byte enable because BRAM won't infer with it
    reg [WRITE_READ1_DATA_WIDTH-1 : 0] byte_write_data; // wire
    reg write_enable_array [COUNT-1 : 0]; // wire
    
    // Write address has to be set for 1 cycle before writing
    // Simulate write byte enable because BRAM won't infer with it
    // readw caontains the old value and write_data contains the new value
    integer i;
    always @(*)
      begin
        for (i = 0; i < WRITE_READ1_DATA_WIDTH_BYTES; i = i + 1)
          begin
            byte_write_data[i * 8 +: 8]
                = byte_write_enable[i] ? write_data[i * 8 +: 8] : readw_data[i * 8 +: 8];
          end
      end
    
    // Set write enable based on msb_write_addr and byte_write_enable
    always @(*)
      begin
        for (i = 0; i < COUNT; i = i + 1)
          begin
            write_enable_array[i] = (byte_write_enable != 0) && (bs_write_addr == i);
          end
      end
    
    // Instantiate multiple blocks, if the READW_DATA_WIDTH
    // is greater than WRITE_READ1_DATA_WIDTH
    genvar j;
    for (j = 0; j < COUNT; j = j + 1)
      begin
        // Dual port memory
        // Only one of the readw_data and read1_data is output
        tdp_memory #(WRITE_READ1_ADDR_WIDTH - CLOG_COUNT, WRITE_READ1_DATA_WIDTH
            ) tdp_memory_inst (
            .clk(clk),
            .write_enable(write_enable_array[j]),
            .write_addr(ba_write_addr),
            .write_data(byte_write_data),
            .readw_data(readw_data_array[j]),
            .read_addr(ba_read1_addr),
            .read_data(read1_data_array[j])
        );
        
        // Asymmetric memory
        // read2_data is concatenated
        asymmetric_memory #(WRITE_READ1_ADDR_WIDTH - CLOG_COUNT, WRITE_READ1_DATA_WIDTH, 
            READ2_ADDR_WIDTH, READ2_DATA_WIDTH / COUNT
            ) asymmetric_memory_inst (
            .clk(clk), 
            .write_enable(write_enable_array[j]),
            .write_addr(ba_write_addr),
            .write_data(byte_write_data),
            .read_addr(read2_addr),
            .read_data(read2_data[j * READ2_DATA_WIDTH / COUNT +: READ2_DATA_WIDTH / COUNT])
        );
      end
    
endmodule
