// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024
//
// AXI logic was generated by the Vivado tool

`timescale 1 ns / 1 ps


module glitch_ip_v1_0 #
(
    // Users to add parameters here
    // Address width of array registers
    // Their layout can be changed by editing glitch_fifo_data wire in this module
    parameter integer GLITCH_TYPE_WIDTH = 8,
    parameter integer GLITCH_DURATION_WIDTH = 24,
    
    parameter integer EXTERNAL_TRIGGER_WIDTH = 1,
    // User parameters ends
    // Do not modify the parameters beyond this line

    // Parameters of Axi Slave Bus Interface S00_AXI
    parameter integer C_S00_AXI_DATA_WIDTH    = 32,
    parameter integer C_S00_AXI_ADDR_WIDTH    = 5
)
(
    // Users to add ports here
    output wire target_clk,
    // First bit is reserved for a clock glitch
    output wire [GLITCH_TYPE_WIDTH-2 : 0] glitch,
    output wire target_reset_n,
    input wire [EXTERNAL_TRIGGER_WIDTH-1 : 0] external_trigger,
    // User ports ends
    // Do not modify the ports beyond this line

    // Ports of Axi Slave Bus Interface S00_AXI
    input wire  s00_axi_aclk,
    input wire  s00_axi_aresetn,
    input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
    input wire [2 : 0] s00_axi_awprot,
    input wire  s00_axi_awvalid,
    output wire  s00_axi_awready,
    input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
    input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
    input wire  s00_axi_wvalid,
    output wire  s00_axi_wready,
    output wire [1 : 0] s00_axi_bresp,
    output wire  s00_axi_bvalid,
    input wire  s00_axi_bready,
    input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
    input wire [2 : 0] s00_axi_arprot,
    input wire  s00_axi_arvalid,
    output wire  s00_axi_arready,
    output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
    output wire [1 : 0] s00_axi_rresp,
    output wire  s00_axi_rvalid,
    input wire  s00_axi_rready
);

    // X Add user logic here
    // Defines how many bits are needed to store GLITCH_TYPE_WIDTH and GLITCH_DURATION_WIDTH
    // It's rounded to a power of 2 number of bytes
    localparam integer ARRAY_DATA_WIDTH
        = (2 ** $clog2((GLITCH_TYPE_WIDTH + GLITCH_DURATION_WIDTH + 7) / 8)) * 8;
    // Maximum glitch count, depends on ARRAY_DATA_WIDTH_xAXI and C_S00_AXI_ADDR_WIDTH
    localparam integer ARRAY_ADDR_WIDTH = C_S00_AXI_ADDR_WIDTH
            - 1 - $clog2(ARRAY_DATA_WIDTH / 8);

    wire [C_S00_AXI_DATA_WIDTH-1 : 0] offset;
    wire [C_S00_AXI_DATA_WIDTH-1 : 0] flags;
    wire [C_S00_AXI_DATA_WIDTH-1 : 0] divisor;
    wire [C_S00_AXI_DATA_WIDTH-1 : 0] glitch_count;
    // Unused
    wire [C_S00_AXI_DATA_WIDTH-1 : 0] out4;
    wire [C_S00_AXI_DATA_WIDTH-1 : 0] out5;

    wire [C_S00_AXI_DATA_WIDTH-1 : 0] status;
    // Unused
    wire [C_S00_AXI_DATA_WIDTH-1 : 0] in1;

    wire [ARRAY_ADDR_WIDTH-1 : 0] glitch_array_addr;
    wire [ARRAY_DATA_WIDTH-1 : 0] glitch_array_data;

    assign in1 = 0;
    // X User logic ends

    // Instantiation of Axi Bus Interface S00_AXI
    glitch_ip_v1_0_S00_AXI # ( 
        .ARRAY_ADDR_WIDTH(ARRAY_ADDR_WIDTH),
        .ARRAY_DATA_WIDTH(ARRAY_DATA_WIDTH),
        .C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
        .C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
    ) glitch_ip_v1_0_S00_AXI_inst (
        .out0(offset),
        .out1(flags),
        .out2(divisor),
        .out3(glitch_count),
        .out4(out4),
        .out5(out5),
        .in0(status),
        .in1(in1),
        .array_addr(glitch_array_addr),
        .array_out(glitch_array_data),
        .S_AXI_ACLK(s00_axi_aclk),
        .S_AXI_ARESETN(s00_axi_aresetn),
        .S_AXI_AWADDR(s00_axi_awaddr),
        .S_AXI_AWPROT(s00_axi_awprot),
        .S_AXI_AWVALID(s00_axi_awvalid),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WDATA(s00_axi_wdata),
        .S_AXI_WSTRB(s00_axi_wstrb),
        .S_AXI_WVALID(s00_axi_wvalid),
        .S_AXI_WREADY(s00_axi_wready),
        .S_AXI_BRESP(s00_axi_bresp),
        .S_AXI_BVALID(s00_axi_bvalid),
        .S_AXI_BREADY(s00_axi_bready),
        .S_AXI_ARADDR(s00_axi_araddr),
        .S_AXI_ARPROT(s00_axi_arprot),
        .S_AXI_ARVALID(s00_axi_arvalid),
        .S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_RDATA(s00_axi_rdata),
        .S_AXI_RRESP(s00_axi_rresp),
        .S_AXI_RVALID(s00_axi_rvalid),
        .S_AXI_RREADY(s00_axi_rready)
    );

    // Add user logic here
    wire [ARRAY_DATA_WIDTH-1 : 0] glitch_fifo_data;

    fifo_adapter #(
        .COUNT_WIDTH(C_S00_AXI_DATA_WIDTH),
        .ADDR_WIDTH(ARRAY_ADDR_WIDTH),
        .DATA_WIDTH(ARRAY_DATA_WIDTH)
    ) fifo_adaper_inst (
        .clk(s00_axi_aclk),
        .array_data(glitch_array_data),
        .next(fifo_next),
        .reset(fifo_reset),
        .count(glitch_count),
        .array_addr(glitch_array_addr),
        .data(glitch_fifo_data),
        .empty(fifo_empty)   
    );

    glitch_module #(
        .DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
        .GLITCH_DURATION_WIDTH(GLITCH_DURATION_WIDTH),
        .GLITCH_TYPE_WIDTH(GLITCH_TYPE_WIDTH),
        .EXTERNAL_TRIGGER_WIDTH(EXTERNAL_TRIGGER_WIDTH)
    ) glitch_module_inst (
        .clk(s00_axi_aclk),
        .offset(offset),
        .flags(flags),
        .divisor(divisor),
        .glitch_duration(glitch_fifo_data[GLITCH_DURATION_WIDTH-1 : 0]),
        .glitch_type(glitch_fifo_data[GLITCH_TYPE_WIDTH + GLITCH_DURATION_WIDTH-1 : GLITCH_DURATION_WIDTH]),
        .fifo_empty(fifo_empty),
        .external_trigger(external_trigger),
        .target_clk(target_clk),
        .target_reset_n(target_reset_n),
        .glitch(glitch),
        .fifo_next(fifo_next),
        .fifo_reset(fifo_reset),
        .status(status)
    );
    // User logic ends

endmodule
