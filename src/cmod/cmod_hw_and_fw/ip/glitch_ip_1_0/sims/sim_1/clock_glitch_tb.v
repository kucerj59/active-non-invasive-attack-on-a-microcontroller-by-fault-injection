// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


`define ASSERT_CLK_VALUE(glitch_value) \
    if (target_clk_out !== target_clk_in ^ glitch_value) \
      begin \
        $display("incorrect value of target_clk_out"); \
        $finish; \
      end

module clock_glitch_tb;

    reg glitch;
    reg target_clk_in;
    
    clock_glitch clock_glitch_inst (
        .glitch(glitch),
        .target_clk_in(target_clk_in),
        .target_clk_out(target_clk_out)
    );
    
    initial
      begin
        glitch = 0;
        target_clk_in = 1;
        
        # 3
        glitch = 1;
        # 3
        `ASSERT_CLK_VALUE(1)
        glitch = 0;
        # 4
        `ASSERT_CLK_VALUE(0)
        
        glitch = 1;
        # 1
        `ASSERT_CLK_VALUE(1)
        # 1
        glitch = 0;
        # 6
        `ASSERT_CLK_VALUE(0)
        
        $finish;
      end

    always # 2 target_clk_in = ~target_clk_in;

endmodule
