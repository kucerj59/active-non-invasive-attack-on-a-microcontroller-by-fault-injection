// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module glitch_module_tb;

    localparam DATA_WIDTH = 32;
    localparam GLITCH_DURATION_WIDTH = 24;
    localparam GLITCH_TYPE_WIDTH = 8;
    localparam EXTERNAL_TRIGGER_WIDTH = 2;

    reg clk;
    reg [DATA_WIDTH-1 : 0] offset;
    reg [DATA_WIDTH-1 : 0] flags;
    reg [DATA_WIDTH-1 : 0] divisor;
    wire [GLITCH_DURATION_WIDTH-1 : 0] glitch_duration;
    wire [GLITCH_TYPE_WIDTH-1 : 0] glitch_type;
    reg fifo_empty;
    reg [EXTERNAL_TRIGGER_WIDTH-1 : 0] external_trigger;

    wire [GLITCH_TYPE_WIDTH-2 : 0] glitch;
    wire [DATA_WIDTH-1 : 0] status;
    
    glitch_module #(
        .DATA_WIDTH(DATA_WIDTH),
        .GLITCH_DURATION_WIDTH(GLITCH_DURATION_WIDTH),
        .GLITCH_TYPE_WIDTH(GLITCH_TYPE_WIDTH),
        .EXTERNAL_TRIGGER_WIDTH(EXTERNAL_TRIGGER_WIDTH)
    ) glitch_module_inst (
        .clk(clk),
        .offset(offset),
        .flags(flags),
        .divisor(divisor),
        .glitch_duration(glitch_duration),
        .glitch_type(glitch_type),
        .fifo_empty(fifo_empty),
        .external_trigger(external_trigger),
        .target_clk(target_clk),
        .target_reset_n(target_reset_n),
        .glitch(glitch),
        .fifo_next(fifo_next),
        .fifo_reset(fifo_reset),
        .status(status)
    );
    
    localparam integer GLITCH_COUNT = 4;
    localparam integer EXPECTED_GLITCH_DELAY = 2;
    localparam integer EXPECTED_EXTERNAL_TRIGGER_DELAY = 3;
    // Depend on the exact time at which the glitch is triggered
    localparam integer OFFSET_DELAY_1 = 1;
    localparam integer OFFSET_DELAY_2 = 4;
    localparam integer RESET_DELAY_3 = 1;
    
    reg [GLITCH_COUNT-1 : 0] counter;
    reg [GLITCH_TYPE_WIDTH-1 : 0] test_type_array [GLITCH_COUNT-1 : 0];
    reg [GLITCH_DURATION_WIDTH-1 : 0] test_duration_array [GLITCH_COUNT-1 : 0];
    
    assign glitch_type = test_type_array[counter];
    assign glitch_duration = test_duration_array[counter];
    
    always@ (posedge clk)
      begin
        if (fifo_next)
          begin
            counter <= counter + 1;
            
            if (counter + 1 >= GLITCH_COUNT)
              begin
                fifo_empty = 1;
              end
          end
      end
    
    integer i;
    integer j;
    initial
      begin
        counter = 0;
        clk = 1;
        offset = 0;
        flags = 0;
        divisor = 1;
        fifo_empty = 0;
        external_trigger = 'd0;
        for (i = 0; i < GLITCH_COUNT; i = i + 1)
          begin
            test_type_array[i] = ((i + 1) % 2) + (((i + 1) % 2) * 2);
            test_duration_array[i] = i + 1;
          end
        # 4
        
        // Test manual trigger and clock glitch
        flags = (1 << DATA_WIDTH - 1) | (1 << 2);
        
        for (i = 0; i < EXPECTED_GLITCH_DELAY + OFFSET_DELAY_1; i = i + 1)
          begin
            # 1
            if (glitch != 0)
              begin
                $display("glitch was inserted too early");
                $finish;
              end
            # 1;
          end
        
        for (i = 0; i < GLITCH_COUNT; i = i + 1)
          begin
            for (j = 0; j < test_duration_array[i]; j = j + 1)
              begin
                # 1
                if (glitch != ((i + 1) % 2))
                  begin
                    $display("unexpected glitch value at %d %d", i, j);
                    $finish;
                  end
                # 1;
              end
          end
        
        # 1
        if (glitch != 0)
          begin
            $display("unexpected glitch value");
            $finish;
          end
        if (status[3] != 1)
          begin
            $display("unexpected status - glitch done value");
            $finish;
          end
        # 3
        
        // Test external trigger and durations of length 1
        flags = 0;
        offset = 2;
        counter = 0;
        fifo_empty = 0;
        for (i = 0; i < GLITCH_COUNT; i = i + 1)
          begin
            test_type_array[i] = 1 << (i + 1);
            test_duration_array[i] = 1;
          end
        # 2
        flags = (1 << DATA_WIDTH - 1) | (1 << 4);
        external_trigger = 1;
        
        for (i = 0; i < EXPECTED_GLITCH_DELAY
            + EXPECTED_EXTERNAL_TRIGGER_DELAY + OFFSET_DELAY_2; i = i + 1)
          begin
            # 1
            if (glitch != 0)
              begin
                $display("glitch was inserted too early");
                $finish;
              end
            # 1;
          end
        
        for (i = 0; i < GLITCH_COUNT; i = i + 1)
          begin
            for (j = 0; j < test_duration_array[i]; j = j + 1)
              begin
                # 1
                if (glitch != (test_type_array[i] >> 1))
                  begin
                    $display("unexpected glitch value at %d %d", i, j);
                    $finish;
                  end
                # 1;
              end
          end
        
        # 1
        if (glitch != 0)
          begin
            $display("unexpected glitch value");
            $finish;
          end
        if (status != 'h1e)
          begin
            $display("unexpected status value");
            $finish;
          end
        # 3
        
        // Test reset_trigger and offset skip
        flags = 1 << DATA_WIDTH - 2;
        counter = 0;
        fifo_empty = 0;
        # 2
        flags = (1 << DATA_WIDTH - 1) | (1 << 3) | 1;
        
        for (i = 0; i < EXPECTED_GLITCH_DELAY + RESET_DELAY_3; i = i + 1)
          begin
            # 1
            if (glitch != 0)
              begin
                $display("glitch was inserted too early");
                $finish;
              end
            # 1;
          end
        
        for (i = 0; i < GLITCH_COUNT; i = i + 1)
          begin
            for (j = 0; j < test_duration_array[i]; j = j + 1)
              begin
                # 1
                if (glitch != (test_type_array[i] >> 1))
                  begin
                    $display("unexpected glitch value at %d %d", i, j);
                    $finish;
                  end
                # 1;
              end
          end
        
        # 1
        if (glitch != 0)
          begin
            $display("unexpected glitch value");
            $finish;
          end
        if (status != 'h1a)
          begin
            $display("unexpected status value");
            $finish;
          end
        # 3
        
        # 4
        $finish;
      end
    
    always # 1 clk = ~clk;

endmodule
