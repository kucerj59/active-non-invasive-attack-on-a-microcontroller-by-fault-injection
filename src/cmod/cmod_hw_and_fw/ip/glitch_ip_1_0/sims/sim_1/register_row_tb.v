// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module register_row_tb;

    localparam integer DATA_WIDTH = 2;
    localparam integer DEPTH = 2;

    reg clk;
    reg [DATA_WIDTH-1 : 0] data_in;
    wire [DATA_WIDTH-1 : 0] data_out;

    register_row #(
        .DATA_WIDTH(DATA_WIDTH),
        .DEPTH(DEPTH)
    ) register_row_inst (
        .clk(clk),
        .data_in(data_in),
        .data_out(data_out)
    );
    
    integer i;
    initial
      begin
        clk = 0;
        
        for (i = 0; i < 4; i = i + 1)
          begin
            data_in = i + 1;
            # 2;
          end
      
        # 8
        $finish;
      end
    
    always # 1 clk = ~clk;

endmodule
