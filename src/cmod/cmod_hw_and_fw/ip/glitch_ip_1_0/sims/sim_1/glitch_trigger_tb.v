// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


`define ASSERT_TRIGGER_HIGH() if (trigger !== 1) \
    begin \
      $display("trigger is not set high"); \
      $finish; \
    end

`define ASSERT_TRIGGER_LOW() if (trigger !== 0) \
    begin \
      $display("trigger is not set low"); \
      $finish; \
    end

module glitch_trigger_tb;

    localparam integer EXTERNAL_TRIGGER_WIDTH = 2;

    reg clk;
    reg enable;
    reg flag_trig_man;
    reg flag_trig_rst;
    reg trig_rst_enable;
    reg [EXTERNAL_TRIGGER_WIDTH-1 : 0] flag_trig_ext;
    reg [EXTERNAL_TRIGGER_WIDTH-1 : 0] trig_ext_enable;

    glitch_trigger #(
        .EXTERNAL_TRIGGER_WIDTH(EXTERNAL_TRIGGER_WIDTH)
    ) glitch_trigger_inst (
        .clk(clk),
        .enable(enable),
        .flag_trig_man(flag_trig_man),
        .flag_trig_rst(flag_trig_rst),
        .trig_rst_enable(trig_rst_enable),
        .flag_trig_ext(flag_trig_ext),
        .trig_ext_enable(trig_ext_enable),
        .trigger(trigger)
    );
    
    initial
      begin
        clk = 1;
        enable = 0;
        # 2
        enable = 1;
        flag_trig_man = 0;
        flag_trig_rst = 0;
        trig_rst_enable = 0;
        flag_trig_ext = 0;
        trig_ext_enable = 0;
        
        # 2
        flag_trig_man = 1;
        # 2
        
        flag_trig_man = 0;
        flag_trig_rst = 1;
        # 2
        `ASSERT_TRIGGER_LOW
        trig_rst_enable = 1;
        # 2
        `ASSERT_TRIGGER_HIGH
        flag_trig_rst = 0;
        flag_trig_ext = 2;
        # 2
        `ASSERT_TRIGGER_LOW
        trig_ext_enable = 1;
        # 2
        `ASSERT_TRIGGER_LOW
        trig_ext_enable = 2;
        # 2
        `ASSERT_TRIGGER_HIGH
        enable = 0;
        # 2
        `ASSERT_TRIGGER_LOW
                  
        # 2
        $finish;
      end
    
    always #1 clk = ~clk;

endmodule
