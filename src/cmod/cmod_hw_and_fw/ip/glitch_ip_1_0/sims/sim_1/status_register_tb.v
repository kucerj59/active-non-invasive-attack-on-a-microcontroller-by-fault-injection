// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module status_register_tb;

    localparam integer DATA_WIDTH = 16;
    localparam integer EXTERNAL_TRIGGER_WIDTH = 8;

    reg clk;
    reg reset;
    reg [DATA_WIDTH-1 : 0] input_reg;
    
    wire [DATA_WIDTH-1 : 0] data;
    wire [EXTERNAL_TRIGGER_WIDTH-1 : 0] external_trigger;

    assign {external_trigger, mgic_done, offset_done, glitch_trigger, target_reset} =
        input_reg[EXTERNAL_TRIGGER_WIDTH + 3 : 0];

    status_register #(
        .DATA_WIDTH(DATA_WIDTH),
        .EXTERNAL_TRIGGER_WIDTH(EXTERNAL_TRIGGER_WIDTH)
    ) status_register_inst (
        .clk(clk),
        .reset(reset),
        .target_reset(target_reset),
        .glitch_trigger(glitch_trigger),
        .offset_done(offset_done),
        .mgic_done(mgic_done),
        .external_trigger(external_trigger),
        .data(data)
    );
    
    integer i;
    initial
      begin
        clk = 1;
        reset = 1;
        input_reg = 0;
        # 2
        reset = 0;
        # 2
        
        // Test set
        for (i = 0; i < DATA_WIDTH; i = i + 1)
          begin
            input_reg <= (1 << i);
            # 2
            if (data & input_reg === 0)
              begin
                $display("status output did not change");
                $finish;
              end
          end
        
        // Test reset
        input_reg = 0;
        reset = 1;
        # 2
        if (data !== 0)
          begin
            $display("status output was not cleared");
            $finish;
          end
        
        # 4
        $finish;
      end
      
    always # 1 clk = ~clk;

endmodule
