// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module multiple_glitch_insertion_controller_tb;

    localparam integer GLITCH_DURATION_WIDTH = 24;
    localparam integer GLITCH_TYPE_WIDTH = 8;
    
    reg clk;
    reg trigger;
    wire [GLITCH_DURATION_WIDTH-1 : 0] glitch_duration;
    wire [GLITCH_TYPE_WIDTH-1 : 0] glitch_type_in;
    reg fifo_empty;
    
    wire [GLITCH_TYPE_WIDTH-1 : 0] glitch_type_out;
    
    multiple_glitch_insertion_controller #(
        .GLITCH_DURATION_WIDTH(GLITCH_DURATION_WIDTH),
        .GLITCH_TYPE_WIDTH(GLITCH_TYPE_WIDTH)
    ) multiple_glitch_insertion_controller_inst (
        .clk(clk),
        .trigger(trigger),
        .glitch_duration(glitch_duration),
        .glitch_type_in(glitch_type_in),
        .fifo_empty(fifo_empty),
        .glitch_type_out(glitch_type_out),
        .fifo_next(fifo_next),
        .fifo_reset(fifo_reset),
        .done(done)
    );
    
    reg [GLITCH_DURATION_WIDTH : 0] durations [7 : 0];
    reg [GLITCH_TYPE_WIDTH : 0] types [7 : 0];
    
    reg [2 : 0] counter; 
    
    always @(posedge clk)
      begin
        if (fifo_next)
          begin
            counter <= counter + 1;
            if (counter == 6)
              begin
                fifo_empty <= 1;
              end
          end
      end
    
    assign glitch_duration = durations[counter];
    assign glitch_type_in = types[counter];
    
    integer i;
    integer j;
    initial
      begin
        counter = 0;
        clk = 1;
        trigger = 0;
        for (i = 0; i < 8; i = i + 1)
          begin
            durations[i] = i;
            types[i] = 1 << i;
          end
        durations[0] = 1;
        # 3
        
         // Test empty
         fifo_empty = 1;
         trigger = 1;
         # 4
         if (done !== 1)
            begin
              $display("insertion did not finish on time");
              $finish;
            end

         fifo_empty = 0;
         trigger = 0;
         # 1
         
         // Test glitch insertion
         counter = 0;
         # 1
         trigger = 1;
         # 3
         
         // For every glitch_type pair
         for (i = 0; i < 7; i = i + 1)
           begin
             // Wait the given duration
             for (j = 0; j + 1 < durations[i]; j = j + 1)
               begin
                 # 1
                 if (glitch_type_out !== types[i])
                   begin
                     $display("glitch type is incorrect at glitch %d and cycle %d", i, j);
                     $finish;
                   end
                 
                 // fifo_next should be asserted only in the last cycle
                 if (fifo_next !== 0)
                   begin
                     $display("glitch duration was incorrect");
                     $finish;
                   end
                 # 1;
               end
             
             // Last cycle
             # 1
             if (glitch_type_out !== types[i])
               begin
                 $display("glitch type is incorrect at glitch %d and cycle %d", i, j);
                 $finish;
               end
             
             // fifo_next should be asserted only in the last cycle
             if (fifo_next !== 1)
               begin
                 $display("glitch duration was incorrect");
                 $finish;
               end
             # 1;
           end
         
         # 1
         if (done !== 1)
           begin
             $display("insertion did not finish on time");
             $finish;
           end
         # 1
         $finish;
      end
    
    always # 1 clk = ~clk;
    
endmodule
