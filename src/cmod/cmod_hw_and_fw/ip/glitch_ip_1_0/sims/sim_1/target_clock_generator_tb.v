// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module target_clock_generator_tb;

    reg clk;
    reg [32-1 : 0] divisor;

    target_clock_generator #(
        .DIVISOR_WIDTH(32)
    ) target_clock_generator_inst (
        .clk(clk),
        .divisor(divisor),
        .target_clk(target_clk),
        .target_clk_posedge(target_clk_posedge)
    );
    
    initial
      begin
        clk = 1;
        divisor = 0;
        # 8
        divisor = 1;
        # 12
        divisor = 2;
        # 16
        divisor = 3;
        # 22
        divisor = 4;
        # 32
        $finish;
      end
    
    always # 1 clk = ~clk;

endmodule
