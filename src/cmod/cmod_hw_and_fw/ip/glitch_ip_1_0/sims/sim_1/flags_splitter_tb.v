// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


`define ASSERT_VALUES(skip, man, rst, ext, target_rst, enable) \
    if (skip !== flag_skip_offset_block || man !== flag_trig_man_out \
        || rst !== flag_trig_rst_out || ext != flag_trig_ext_out \
        || target_rst !== target_rst_out || enable !== glitch_enable_out) \
      begin \
        $display("expected %d %d %d %d %d %d", skip, man, rst, ext, target_rst, enable); \
        $display("actual %d %d %d %d %d %d", flag_skip_offset_block, flag_trig_man_out, \
            flag_trig_rst_out, flag_trig_ext_out, target_rst_out, glitch_enable_out); \
        $display("flags do not match"); \
        $finish; \
      end

module flags_splitter_tb;

    reg [7:0] flags;
    wire [1 : 0] flag_trig_ext_out;

    flags_splitter #(
        .FLAGS_WIDTH(8),
        .EXTERNAL_TRIGGER_WIDTH(2)
    ) flags_splitter_inst (
        .flags(flags),
        .flag_skip_offset_block(flag_skip_offset_block),
        .flag_trig_man(flag_trig_man_out),
        .flag_trig_rst(flag_trig_rst_out),
        .flag_trig_ext(flag_trig_ext_out),
        .target_rst(target_rst_out),
        .glitch_enable(glitch_enable_out)
    );
    
    integer i;
    initial
      begin
        flags = 0;
        # 2
        `ASSERT_VALUES(0, 0, 0, 0, 0, 0)
        flags = 1;
        # 2
        `ASSERT_VALUES(1, 0, 0, 0, 0, 0)
        flags = 2;
        # 2
        `ASSERT_VALUES(0, 0, 0, 0, 0, 0)
        flags = 4;
        # 2
        `ASSERT_VALUES(0, 1, 0, 0, 0, 0)
        flags = 8;
        # 2
        `ASSERT_VALUES(0, 0, 1, 0, 0, 0)
        flags = 16;
        # 2
        `ASSERT_VALUES(0, 0, 0, 1, 0, 0)
        flags = 32;
        # 2
        `ASSERT_VALUES(0, 0, 0, 2, 0, 0)
        flags = 64;
        # 2
        `ASSERT_VALUES(0, 0, 0, 0, 1, 0)
        flags = 128;
        # 2
        `ASSERT_VALUES(0, 0, 0, 0, 0, 1)
        flags = 0;
        # 2
        `ASSERT_VALUES(0, 0, 0, 0, 0, 0)
        
        #2 $finish;
      end

endmodule
