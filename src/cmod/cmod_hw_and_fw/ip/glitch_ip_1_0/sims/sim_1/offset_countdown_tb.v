// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module offset_countdown_tb;

    localparam integer DATA_WIDTH = 32;

    reg clk;
    reg enable;
    reg target_clk_posedge;
    reg [DATA_WIDTH-1 : 0] offset;

    offset_countdown #(
        .DATA_WIDTH(DATA_WIDTH)
    ) offset_countdown_inst (
        .clk(clk),
        .enable(enable),
        .target_clk_posedge(target_clk_posedge),
        .offset(offset),
        .done(done)
    );
    
    initial
      begin
        clk = 1;
        target_clk_posedge = 0;
        offset = 0;
        enable = 0;
        # 4
        
        // Test offset value of 0
        enable = 1;
        # 3
        if (done !== 1)
          begin
            $display("done was not set high");
            $finish;
          end
        # 1
        
        // Test disable
        enable = 0;
        # 3
        if (done !== 0)
          begin
            $display("done stayed high");
            $finish;
          end
        # 1
        
        // Test offset value greater than 0
        offset = 3;
        # 2
        enable = 1;
        # 11
        if (done !== 0)
          begin
            $display("done was set high too early");
            $finish;
          end
        # 2
        if (done !== 1)
          begin
            $display("done was not set high");
            $finish;
          end
        # 7
        
        $finish;
      end
    
    always # 1 clk = ~clk;
    always@ (posedge clk)
      begin
        target_clk_posedge <= ~target_clk_posedge;
      end

endmodule
