// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module target_reset_module_tb;

    reg clk;
    reg reset_enable;
    reg target_clk_posedge;

    target_reset_module target_reset_module_inst (
        .clk(clk),
        .reset_enable(reset_enable),
        .target_clk_posedge(target_clk_posedge),
        .reset_n(reset_n),
        .reset(reset)
    );
    
    initial
      begin
        clk = 1;
        reset_enable = 0;
        target_clk_posedge = 0;
        # 2
        
        // reset_n should stay high
        target_clk_posedge = 1;
        # 2
        target_clk_posedge = 0;
        # 2
        if (reset_n !== 1)
          begin
            $display("reset_n should be high");
            $finish;
          end
        
        // reset_n should go low
        reset_enable = 1;
        # 2
        if (reset_n !== 0)
          begin
            $display("reset_n should be low");
            $finish;
          end
        
        // reset_n should stay low
        target_clk_posedge = 1;
        # 2
        target_clk_posedge = 0;
        # 2
        if (reset_n !== 0)
          begin
            $display("reset_n should be low");
            $finish;
          end
        
        // reset_n should return to high
        reset_enable = 0;
        target_clk_posedge = 1;
        # 2
        if (reset_n !== 1)
          begin
            $display("reset_n should be high");
            $finish;
          end
        
        $finish;
      end
      
    always # 1 clk = ~clk;

endmodule
