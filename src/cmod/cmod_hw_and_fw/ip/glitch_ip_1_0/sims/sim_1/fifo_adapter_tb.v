// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module fifo_adapter_tb;

    localparam integer DATA_WIDTH = 32;
    localparam integer ADDR_WIDTH = 4;
    
    reg clk;
    reg [DATA_WIDTH-1 : 0] array_data;
    reg next;
    reg reset;
    reg [ADDR_WIDTH-1 : 0] count;
    wire [ADDR_WIDTH-1 : 0] array_addr;
    wire [DATA_WIDTH-1 : 0] data;

    fifo_adapter #(
        .COUNT_WIDTH(ADDR_WIDTH),
        .ADDR_WIDTH(ADDR_WIDTH),
        .DATA_WIDTH(DATA_WIDTH)
    ) fifo_adaper_inst(
        .clk(clk),
        .array_data(array_data),
        .next(next),
        .reset(reset),
        .count(count),
        .array_addr(array_addr),
        .data(data),
        .empty(empty)   
    );
    
    reg [DATA_WIDTH-1 : 0] test_array [2 ** ADDR_WIDTH-1 : 0];
    always@ (posedge clk)
      begin
        array_data <= test_array[array_addr];
      end
    //assign array_data = test_array[array_addr];
    
    integer i;
    initial
      begin
        clk <= 1;
        count <= 7;
        reset <= 1;
        next <= 0;
        for (i = 0; i < 2 ** ADDR_WIDTH; i = i + 1)
          begin
            test_array[i] <= i + 1;
          end
        # 4
        reset <= 0;
        # 2
        
        // Test 7 reads
        for (i = 0; i < 7; i = i + 1)
          begin
            # 2
            if (data !== test_array[i])
              begin
                $display("unexpected data value");
                $finish;
              end
              
            // Test a cycle without next
            if (i == 4)
              begin
                next <= 0;
                # 2
                if (data !== test_array[i])
                  begin
                    $display("unexpected data value");
                    $finish;
                  end
              end
            
            next <= 1;
          end
        
        # 1
        if (empty !== 1)
          begin
            $display("empty was not asserted");
            $finish;
          end
        # 5
        
        // Test fifo with 1 element
        reset = 1;
        count = 1;
        next = 0;
        # 2
        reset = 0;
        # 1
        if (data !== test_array[0])
          begin
            $display("unexpected data value");
            $finish;
          end
        # 2
        if (empty !== 0)
          begin
            $display("empty was asserted too early");
            $finish;
          end
        # 1
        next = 1;
        # 1
        if (empty !== 1)
          begin
            $display("empty was not asserted");
            $finish;
          end
        # 1
        
        # 4 $finish;
      end
    
    always # 1 clk <= ~clk;

endmodule
