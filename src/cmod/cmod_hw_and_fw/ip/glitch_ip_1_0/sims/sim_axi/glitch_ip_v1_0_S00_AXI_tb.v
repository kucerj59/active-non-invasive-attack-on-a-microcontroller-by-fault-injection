// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


// Run in Tcl Console to FIX error:
// set_property library xil_defaultlib [get_files]

module glitch_ip_v1_0_S00_AXI_tb;
    
    // Change to test different widths
    localparam integer ARRAY_TEST_VALUE = 32'h12345678;
    localparam integer TEST_VALUE_MASK = 32'h0000FFFF;
    localparam integer ARRAY_DATA_WIDTH = 16;
    
    localparam integer REG_TEST_VALUE = 32'h90abcdef;
    
    localparam integer TEST_AXI_ARRAY_OFFSET = 4;
    localparam integer TEST_GLITCH_ARRAY_OFFSET = 2;
    
    localparam integer ARRAY_ADDR_WIDTH = 5;
    localparam integer C_S_AXI_DATA_WIDTH = 32;
    localparam integer C_S_AXI_ADDR_WIDTH = 7;
    
    // Second half of address space
    localparam integer ARRAY_OFFSET = 2 ** (C_S_AXI_ADDR_WIDTH - 1);
    
    reg [C_S_AXI_DATA_WIDTH-1 : 0] in0;
    reg [C_S_AXI_DATA_WIDTH-1 : 0] in1;
    
    reg [ARRAY_ADDR_WIDTH-1 : 0] array_addr;
    reg s_axi_aclk;
    reg s_axi_aresetn;
    reg [C_S_AXI_ADDR_WIDTH-1 : 0] s_axi_awaddr;
    // Unused
    reg [2 : 0] s_axiawprot;
    reg s_axi_awvalid;
    reg [C_S_AXI_DATA_WIDTH-1 : 0] s_axi_wdata;
    reg [(C_S_AXI_DATA_WIDTH/8)-1 : 0] s_axi_wstrb;
    reg s_axi_wvalid;
    reg s_axi_bready;
    reg [C_S_AXI_ADDR_WIDTH-1 : 0] s_axi_araddr;
    // Unused
    reg [2 : 0] s_axi_arprot;
    reg [2 : 0] s_axi_awprot;
    reg s_axi_arvalid;
    reg s_axi_rready;
    
    // Make the buses the right size
    wire [C_S_AXI_DATA_WIDTH-1 : 0] s_axi_rdata;
    wire [ARRAY_DATA_WIDTH-1 : 0] array_out;
    wire [1 : 0] s_axi_rresp;
    
    wire [C_S_AXI_DATA_WIDTH-1 : 0] out0;
    wire [C_S_AXI_DATA_WIDTH-1 : 0] out1;
    wire [C_S_AXI_DATA_WIDTH-1 : 0] out2;
    wire [C_S_AXI_DATA_WIDTH-1 : 0] out3;
    wire [C_S_AXI_DATA_WIDTH-1 : 0] out4;
    wire [C_S_AXI_DATA_WIDTH-1 : 0] out5;
    
    glitch_ip_v1_0_S00_AXI # ( 
	    .ARRAY_ADDR_WIDTH(ARRAY_ADDR_WIDTH),
	    .ARRAY_DATA_WIDTH(ARRAY_DATA_WIDTH),
        .C_S_AXI_DATA_WIDTH(C_S_AXI_DATA_WIDTH),
        .C_S_AXI_ADDR_WIDTH(C_S_AXI_ADDR_WIDTH)
    ) axi_glitch_ip_v1_0_S00_AXI_inst (
        .out0(out0),
        .out1(out1),
        .out2(out2),
        .out3(out3),
        .out4(out4),
        .out5(out5),
        .in0(in0),
        .in1(in1),
        .array_addr(array_addr),
        .array_out(array_out),
        .S_AXI_ACLK(s_axi_aclk),
        .S_AXI_ARESETN(s_axi_aresetn),
        .S_AXI_AWADDR(s_axi_awaddr),
        .S_AXI_AWPROT(s_axi_awprot),
        .S_AXI_AWVALID(s_axi_awvalid),
        .S_AXI_AWREADY(s_axi_awready),
        .S_AXI_WDATA(s_axi_wdata),
        .S_AXI_WSTRB(s_axi_wstrb),
        .S_AXI_WVALID(s_axi_wvalid),
        .S_AXI_WREADY(s_axi_wready),
        .S_AXI_BRESP(s_axi_bresp),
        .S_AXI_BVALID(s_axi_bvalid),
        .S_AXI_BREADY(s_axi_bready),
        .S_AXI_ARADDR(s_axi_araddr),
        .S_AXI_ARPROT(s_axi_arprot),
        .S_AXI_ARVALID(s_axi_arvalid),
        .S_AXI_ARREADY(s_axi_arready),
        .S_AXI_RDATA(s_axi_rdata),
        .S_AXI_RRESP(s_axi_rresp),
        .S_AXI_RVALID(s_axi_rvalid),
        .S_AXI_RREADY(s_axi_rready)
    );
    
    initial
      begin
        in0 = 100;
        in1 = 101;
        
        array_addr = 0;
        s_axi_aclk = 1;
        s_axi_aresetn = 0;
        s_axi_awaddr = 0;
        s_axi_awvalid = 0;
        s_axi_wdata = 0;
        s_axi_wstrb = 4'b1111;
        s_axi_wvalid = 0;
        s_axi_bready = 0;
        s_axi_araddr = 0;
        s_axi_arvalid = 0;
        s_axi_rready = 0;
        # 2
        s_axi_aresetn = 1;
        # 2
        
        // Write
        s_axi_awaddr = 0;
        s_axi_wdata = REG_TEST_VALUE;
        s_axi_awvalid = 1;
        s_axi_wvalid = 1;
        s_axi_bready = 1;
        # 2
        if (~s_axi_awready | ~s_axi_wready)
          begin
            $display("awready or wready was not asserted");
            $finish;
          end
        # 2
        // Delayed for simulation to work
        s_axi_awvalid = 0;
        s_axi_wvalid = 0;
        if (s_axi_bvalid !== 1)
          begin
            $display("bvalid was not asserted");
            $finish;
          end
        # 6
        
        // Write array
        // ARRAY_DATA_WIDTH
        s_axi_awaddr = ARRAY_OFFSET + TEST_AXI_ARRAY_OFFSET;
        s_axi_wdata = ARRAY_TEST_VALUE;
        s_axi_awvalid = 1;
        s_axi_wvalid = 1;
        s_axi_bready = 1;
        # 2
        if (s_axi_awready !== 1 || s_axi_wready !== 1)
          begin
            $display("awready or wready was not asserted");
            $finish;
          end
        # 2
        // Delayed for simulation to work
        s_axi_awvalid = 0;
        s_axi_wvalid = 0;
        if (s_axi_bvalid !== 1)
          begin
            $display("bvalid was not asserted");
            $finish;
          end
        # 6
    
        // Read test
        s_axi_araddr = 0;
        s_axi_arvalid = 1;
        s_axi_rready = 1;
        # 2
        if (s_axi_arready !== 1)
          begin
            $display("arready was not asserted");
            $finish;
          end
        # 2
        // Delayed for simulation to work
        s_axi_arvalid = 0;
        # 2
        if (s_axi_rvalid !== 1 || s_axi_rdata !== REG_TEST_VALUE)
          begin
            $display("rvalid was not asserted or received data is incorrect");
            $finish;
          end
        s_axi_rready = 0;
        # 6
        
        // Read array test
        s_axi_araddr = ARRAY_OFFSET + TEST_AXI_ARRAY_OFFSET;
        s_axi_arvalid = 1;
        s_axi_rready = 1;
        # 2
        if (s_axi_arready !== 1)
          begin
            $display("arready was not asserted");
            $finish;
          end
        # 2
        // Delayed for simulation to work
        s_axi_arvalid = 0;
        # 2
        if (s_axi_rvalid !== 1 || s_axi_rdata !== ARRAY_TEST_VALUE)
          begin
            $display("rvalid was not asserted or received data is incorrect");
            $finish;
          end
        s_axi_rready = 0;
        # 6
        
        // Read from array
        array_addr = TEST_GLITCH_ARRAY_OFFSET;
        # 2
        if ({{(32 - ARRAY_DATA_WIDTH){1'b0}}, array_out} != (ARRAY_TEST_VALUE & TEST_VALUE_MASK))
          begin
            # 2
            $display("array data is incorrect");
            $finish;
          end
        #4
        
        $finish;
      end
    
    always #1 s_axi_aclk = ~s_axi_aclk;
    
endmodule
