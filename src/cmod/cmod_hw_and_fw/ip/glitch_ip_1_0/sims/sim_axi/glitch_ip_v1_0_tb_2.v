// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


`define WRITE_DATA(addr, value) \
    s00_axi_awaddr = addr; \
    s00_axi_wdata = value; \
    s00_axi_awvalid = 1; \
    s00_axi_wvalid = 1; \
    s00_axi_bready = 1; \
    # 4 \
    s00_axi_awvalid = 0; \
    s00_axi_wvalid = 0;

module glitch_ip_v1_0_tb_2;

    localparam integer GLITCH_TYPE_WIDTH = 32;
    localparam integer GLITCH_DURATION_WIDTH = 32;
    localparam integer EXTERNAL_TRIGGER_WIDTH = 2;
    localparam integer C_S00_AXI_DATA_WIDTH = 32;
    localparam integer C_S00_AXI_ADDR_WIDTH = 7;

    localparam integer GLITCH_ARRAY_OFFSET = 2 ** (C_S00_AXI_ADDR_WIDTH - 1);

    reg s00_axi_aclk;
    reg s00_axi_aresetn;
    reg [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr;
    reg [2 : 0] s00_axi_awprot;
    reg s00_axi_awvalid;
    reg [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata;
    reg [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb;
    reg s00_axi_wvalid;
    reg s00_axi_bready;
    reg s00_axi_rready;
    reg [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr;
    reg [2 : 0] s00_axi_arprot;
    reg s00_axi_arvalid;
    
    reg [EXTERNAL_TRIGGER_WIDTH-1 : 0] external_trigger;
    
    wire [1 : 0] s00_axi_bresp;
    wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata;
    wire [1 : 0] s00_axi_rresp;
    
    wire [GLITCH_TYPE_WIDTH-2: 0] glitch;

    glitch_ip_v1_0 #(
        .GLITCH_TYPE_WIDTH(GLITCH_TYPE_WIDTH),
        .GLITCH_DURATION_WIDTH(GLITCH_DURATION_WIDTH),
        .EXTERNAL_TRIGGER_WIDTH(EXTERNAL_TRIGGER_WIDTH),
        .C_S00_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
        .C_S00_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
    ) glitch_ip_v1_0_inst (
        .target_clk(target_clk),
        .glitch(glitch),
        .target_reset_n(target_reset_n),
        .external_trigger(external_trigger),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awprot(s00_axi_awprot),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_bresp(s00_axi_bresp),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_arprot(s00_axi_arprot),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rresp(s00_axi_rresp),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_rready(s00_axi_rready)
    );
    
    initial
      begin
        s00_axi_aclk = 1;
        s00_axi_aresetn = 0;
        s00_axi_awaddr = 0;
        s00_axi_awprot = 0;
        s00_axi_awvalid = 0;
        s00_axi_wdata = 0;
        s00_axi_wstrb = 4'b1111;
        s00_axi_wvalid = 0;
        s00_axi_bready = 0;
        s00_axi_rready = 0;
        s00_axi_araddr = 0;
        s00_axi_arprot = 0;
        s00_axi_arvalid = 0;
        external_trigger = 0;
        # 2
        s00_axi_aresetn = 1;
        # 2
      
        // offset = 0;
        `WRITE_DATA(0, 0)
        # 4
        // flags = 0;
        `WRITE_DATA(4, 0)
        # 4
        // divisor = 3
        `WRITE_DATA(8, 3)
        # 4
        // count = 2
        `WRITE_DATA(12, 2)
        # 4
        // set first glitch's duration to 1 and type to 0xFFFFFFFF
        `WRITE_DATA(GLITCH_ARRAY_OFFSET, 1)
        # 4
        `WRITE_DATA(GLITCH_ARRAY_OFFSET + 4, 'hFFFFFFFF)
        # 4
        // set second glitch's duration to 1 and type to 17
        `WRITE_DATA(GLITCH_ARRAY_OFFSET + 8, 1)
        # 4
        `WRITE_DATA(GLITCH_ARRAY_OFFSET + 12, 17)
        # 4
        // flags = enable glitch, enable reset trigger, skip offset
        `WRITE_DATA(4, (3 << (C_S00_AXI_DATA_WIDTH - 2)) | (1 << 3) | 1);
        # 4
        
        // Trigger by ending the reset
        `WRITE_DATA(4, (1 << (C_S00_AXI_DATA_WIDTH - 1)) | (1 << 3) | 1);
        
        // Expected delay in cycles: 0 at glitch insertion
        // possibly some to wait for target's clock posedge at reset block and glitch block
        // there is some delay from previous write operation
        # 7
        if (glitch !== 0)
          begin
            $display("glitch was inserted too early");
            $finish;
          end
        # 2
        if (glitch !== 'h7FFFFFFF)
          begin
            $display("glitch was inserted too late");
            $finish;
          end
        # 2
        if (glitch !== 'h8)
          begin
            $display("second glitch was not inserted");
            $finish;
          end
        # 2
        if (glitch !== 0)
          begin
            $display("glitch was too long");
            $finish;
          end
        # 1
        
        # 10
        $finish;
      end
    
    always # 1 s00_axi_aclk = ~s00_axi_aclk;

endmodule
