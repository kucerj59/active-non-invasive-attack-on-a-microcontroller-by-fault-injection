// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module asymmetric_memory_tb;

    localparam integer WRITE_ADDR_WIDTH = 2;
    localparam integer WRITE_DATA_WIDTH = 32;
    localparam integer READ_ADDR_WIDTH = 4;
    localparam integer READ_DATA_WIDTH = 8;

    reg clk;
    reg write_enable;
    reg [WRITE_ADDR_WIDTH-1 : 0] write_addr;
    reg [WRITE_DATA_WIDTH-1 : 0] write_data;
    reg [READ_ADDR_WIDTH-1 : 0] read_addr;
    wire [READ_DATA_WIDTH-1 : 0] read_data;

    asymmetric_memory #(
        .WRITE_ADDR_WIDTH(WRITE_ADDR_WIDTH),
        .WRITE_DATA_WIDTH(WRITE_DATA_WIDTH),
        .READ_ADDR_WIDTH(READ_ADDR_WIDTH),
        .READ_DATA_WIDTH(READ_DATA_WIDTH)
    ) asymmetric_memory_inst (
        .clk(clk),
        .write_enable(write_enable),
        .write_addr(write_addr),
        .write_data(write_data),
        .read_addr(read_addr),
        .read_data(read_data)
    );
    
    integer i;
    initial
      begin
        clk = 1;
        write_enable = 0;
        write_addr = 0;
        write_data = 0;
        read_addr = 0;
        # 2
        
        // Write some data
        write_data = 'h90abcdef;
        write_enable = 1;
        # 2
        write_addr = 2 ** WRITE_ADDR_WIDTH - 1;
        write_data = 'h12345678;
        # 2
        
        // Test write enable
        write_data = 'h21436587;
        write_enable = 0;
        # 2
        
        // Test read
        for (i = 0; i < WRITE_DATA_WIDTH / READ_DATA_WIDTH; i = i + 1)
          begin
            read_addr = 2 ** READ_ADDR_WIDTH - i - 1;
            # 2;
          end
        
        # 2
        $finish;
      end
    
    always #1 clk = ~clk;

endmodule
