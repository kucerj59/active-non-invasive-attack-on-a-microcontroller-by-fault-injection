// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module three_port_memory_tb_wide;

    localparam integer WRITE_READ1_ADDR_WIDTH = 4;
    localparam integer WRITE_READ1_DATA_WIDTH = 32;
    localparam integer WRITE_ENABLE_WIDTH = 4;
    localparam integer READ2_ADDR_WIDTH = 2;
    localparam integer READ2_DATA_WIDTH = 128;

    reg clk;
    reg [WRITE_ENABLE_WIDTH-1 : 0] byte_write_enable;
    reg [WRITE_READ1_ADDR_WIDTH-1 : 0] write_addr;
    reg [WRITE_READ1_DATA_WIDTH-1 : 0] write_data;
    reg [WRITE_READ1_ADDR_WIDTH-1 : 0] read1_addr;
    wire [WRITE_READ1_DATA_WIDTH-1 : 0] read1_data;
    reg [READ2_ADDR_WIDTH-1 : 0] read2_addr;
    wire [READ2_DATA_WIDTH-1 : 0] read2_data;

    three_port_memory #(
        .WRITE_READ1_ADDR_WIDTH(WRITE_READ1_ADDR_WIDTH),
        .WRITE_READ1_DATA_WIDTH(WRITE_READ1_DATA_WIDTH),
        .WRITE_ENABLE_WIDTH(WRITE_ENABLE_WIDTH),
        .READ2_ADDR_WIDTH(READ2_ADDR_WIDTH),
        .READ2_DATA_WIDTH(READ2_DATA_WIDTH)
    ) three_port_memory_inst (
        .clk(clk),
        .byte_write_enable(byte_write_enable),
        .write_addr(write_addr),
        .write_data(write_data),
        .read1_addr(read1_addr),
        .read1_data(read1_data),
        .read2_addr(read2_addr),
        .read2_data(read2_data)
    );
    
    reg [WRITE_READ1_DATA_WIDTH-1 : 0] test_value;
    
    integer i;
    initial
      begin
        clk = 1;
        byte_write_enable = 0;
        write_addr = 0;
        write_data = 0;
        read1_addr = 0;
        read2_addr = 0;
        # 2
        
        // Test write and read1
        write_data = 'h90abcdef;
        byte_write_enable = 'b1111;
        # 4
        if (read1_data !== 'h90abcdef)
          begin
            $display("invalid read1 data");
            $finish;
          end
         
        // Write to all addresses
        for (i = 0; i < 2 ** WRITE_READ1_ADDR_WIDTH; i = i + 1)
          begin
            test_value = ((4 * i) << 24) | ((4 * i + 1) << 16)
                | ((4 * i + 2) << 8) | ((4 * i + 3) << 0);
            // Set new address
            write_addr = i;
            byte_write_enable = 'b0000;
            # 2
            // Write data
            write_data = test_value;
            byte_write_enable = 'b1111;
            read1_addr = i;
            # 2
            // Disable write and wait for data to be read
            byte_write_enable = 'b0000;
            # 2
            // Read data
            if (read1_data !== test_value)
              begin
                $display("invalid read1 data %d %d", read1_data,
                    ((4 * i) << 24) | ((4 * i + 1) << 16)
                        | ((4 * i + 2) << 8) | ((4 * i + 3) << 0));
                $finish;
              end
          end
        
        // Test write enable
        write_data = 'h21436587;
        byte_write_enable = 'b1001;
        # 2
        write_data = 'h09badcfe;
        byte_write_enable = 0;
        # 2
        
        // Test read
        read2_addr = 2 ** READ2_ADDR_WIDTH - 1;
        # 1
        if (read2_data !== 'h213d3e8738393a3b3435363730313233)
          begin
            $display("invalid read2 data. It might by caused by not working byte write enable");
            $finish;
          end
        # 1;
        
        # 2
        $finish;
      end
    
    always #1 clk = ~clk;

endmodule
