// Jakub Kucera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

`timescale 1ns / 1ps


module tdp_memory_tb;

    localparam integer ADDR_WIDTH = 3;
    localparam integer DATA_WIDTH = 32;

    reg clk;
    reg write_enable;
    reg [ADDR_WIDTH-1 : 0] write_addr;
    reg [DATA_WIDTH-1 : 0] write_data;
    wire [DATA_WIDTH-1 : 0] readw_data;
    reg [ADDR_WIDTH-1 : 0] read_addr;
    wire [DATA_WIDTH-1 : 0] read_data;

    tdp_memory #(
        .ADDR_WIDTH(ADDR_WIDTH),
        .DATA_WIDTH(DATA_WIDTH)
    ) tdp_memory_inst (
        .clk(clk),
        .write_enable(write_enable),
        .write_addr(write_addr),
        .write_data(write_data),
        .readw_data(readw_data),
        .read_addr(read_addr),
        .read_data(read_data)
    );
    
    initial
      begin
        clk = 1;
        write_enable = 0;
        write_addr = 0;
        write_data = 0;
        read_addr = 0;
        # 2
        
        // Write some data
        write_data = 'h90abcdef;
        write_enable = 1;
        # 2
        write_addr = 2 ** ADDR_WIDTH - 1;
        write_data = 'h12345678;
        # 2
        
        // Test write enable
        write_data = 'h21436587;
        write_enable = 0;
        # 2
        
        // Test read
        read_addr = 2 ** ADDR_WIDTH - 1;
        # 2
        
        # 2
        $finish;
      end
    
    always #1 clk = ~clk;

endmodule
