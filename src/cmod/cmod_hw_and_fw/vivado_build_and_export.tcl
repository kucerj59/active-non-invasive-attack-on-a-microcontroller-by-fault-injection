# Jakub Kučera
# kucerj59@fit.cvut.cz
# Created as part of my bachelor's thesis:
# Active non-invasive attack on a microcontroller by fault injection
# Czech Technical University - Faculty of Information Technology
# 2024

if {![info exists name]} {
    set name "[file dirname [info script]]/hardware.xsa"
}

if {[catch {current_project}]} {
    open_project "[file dirname [info script]]/project_glitch/project_glitch.xpr"
}

update_compile_order -fileset sources_1

if {[catch {get_property NEEDS_REFRESH [get_runs impl_1]}] || [get_property NEEDS_REFRESH [get_runs impl_1]]
    || [get_property STATUS [get_runs impl_1]] ne "write_bitstream Complete!"} {
    reset_run synth_1
    launch_runs impl_1 -to_step write_bitstream -jobs 10
    wait_on_run impl_1
}

write_hw_platform -fixed -include_bit -force -file "$name"