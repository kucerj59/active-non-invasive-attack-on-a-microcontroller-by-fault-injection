// Jakub Kučera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

#ifndef UART_HEADER
#define UART_HEADER

#include <xil_types.h>
#include <xuartns550.h>

int InitUart(XUartNs550 *UartPtr, UINTPTR BaseAddress, u32 UartClock, u32 BaudRate, int SelfTest);
void SetBaudRate(UINTPTR BaseAddress, u32 UartClock, u32 BaudRate);
int SetupInterrupt(XUartNs550 *UartPtr, UINTPTR BaseAddress, void *IntrHandler);
void DisconnectInterrupt(XUartNs550 *UartPtr, UINTPTR BaseAddress);

#endif