// Jakub Kučera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

#ifndef GLITCH_AXI_HEADER
#define GLITCH_AXI_HEADER

#include <xil_io.h>
#include <xil_types.h>

// Following parameters depend on the instiation parameters of the peripheral
#define MAX_GLITCH_COUNT 2048
#define GLITCH_TYPE_WIDTH 2
#define GLITCH_DURATION_WIDTH 14
#define EXTERNAL_TRIGGER_WIDTH 1
// Number of bytes of GLITCH_TYPE_WIDTH + GLITCH_DURATION_WIDTH rounded up to the nearest power of 2
#define GLITCH_DATA_BYTES 2

#define OFFSET_OFFSET 0
#define FLAGS_OFFSET 4
#define DIVISOR_OFFSET 8
#define GLITCH_COUNT_OFFSET 12
#define STATUS_OFFSET 24
#define GLITCH_ARRAY_OFFSET 4096
// Flags
// 0 - offset skip
// 1 - unused
// 2 - manual trigger
// 3 - reset trigger flag
// 4+ - external trigger flags
// -2 - reset
// -1 - glitch enable
// Status
// 0 - reset value
// 1 - glitch triggered
// 2 - offset countdown done
// 3 - glitch insertion done
// 4+ - external triggers

static INLINE void SetOffset(UINTPTR BaseAddress, u32 Offset) {
    Xil_Out32(BaseAddress + OFFSET_OFFSET, Offset);
}

static INLINE void SetFlags(UINTPTR BaseAddress, u32 Flags) {
    Xil_Out32(BaseAddress + FLAGS_OFFSET, Flags);
}

static INLINE void SetDivisor(UINTPTR BaseAddress, u32 Division) {
    Xil_Out32(BaseAddress + DIVISOR_OFFSET, Division);
}

static INLINE void SetGlitchCount(UINTPTR BaseAddress, u32 Count) {
    Xil_Out32(BaseAddress + GLITCH_COUNT_OFFSET, Count);
}

static INLINE u32 GetStatus(UINTPTR BaseAddress) {
    return Xil_In32(BaseAddress + STATUS_OFFSET);
}

static INLINE void SetGlitchArray8(UINTPTR BaseAddress, u32 Offset, u8 Value) {
    Xil_Out8(BaseAddress + GLITCH_ARRAY_OFFSET + Offset, Value);
}

static INLINE void SetGlitchArray16(UINTPTR BaseAddress, u32 Offset, u16 Value) {
    Xil_Out16(BaseAddress + GLITCH_ARRAY_OFFSET + Offset, Value);
}

static INLINE void SetGlitchArray32(UINTPTR BaseAddress, u32 Offset, u32 Value) {
    Xil_Out32(BaseAddress + GLITCH_ARRAY_OFFSET + Offset, Value);
}

static INLINE u32 ReadReg(UINTPTR BaseAddress, u32 Offset) {
    return Xil_In32(BaseAddress + Offset);
}

static INLINE void WriteReg(UINTPTR BaseAddress, u32 Offset, u32 Value) {
    Xil_Out32(BaseAddress + Offset, Value);
}

#endif