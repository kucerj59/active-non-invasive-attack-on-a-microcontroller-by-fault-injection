// Jakub Kučera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

#include "uart.h"

#include <xinterrupt_wrap.h>
#include <xstatus.h>

int InitUart(XUartNs550 *UartPtr, UINTPTR BaseAddress, u32 UartClock, u32 BaudRate, int SelfTest) {
    int Status = XUartNs550_Initialize(UartPtr, BaseAddress);
    if (Status != XST_SUCCESS) {
        return XST_FAILURE;
    }

    // Self test takes a long time to perform
    if (SelfTest) {
        Status = XUartNs550_SelfTest(UartPtr);
        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }
    }

    XUartNs550_SetBaud(BaseAddress, UartClock, BaudRate);
    XUartNs550_SetLineControlReg(BaseAddress, XUN_LCR_8_DATA_BITS);
    
    return XST_SUCCESS;
}

void SetBaudRate(UINTPTR BaseAddress, u32 UartClock, u32 BaudRate) {
    XUartNs550_SetBaud(BaseAddress, UartClock, BaudRate);
}

int SetupInterrupt(XUartNs550 *UartPtr, UINTPTR BaseAddress, void *IntrHandler) {
	XUartNs550_Config *CfgPtr = XUartNs550_LookupConfig(BaseAddress);

	int Status = XSetupInterruptSystem(UartPtr, &XUartNs550_InterruptHandler, 
        CfgPtr->IntrId, CfgPtr->IntrParent, XINTERRUPT_DEFAULT_PRIORITY);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
    
	XUartNs550_SetHandler(UartPtr, IntrHandler, UartPtr);

	u16 Options = XUN_OPTION_DATA_INTR | XUN_OPTION_FIFOS_ENABLE;
	XUartNs550_SetOptions(UartPtr, Options);

	return XST_SUCCESS;
}

void DisconnectInterrupt(XUartNs550 *UartPtr, UINTPTR BaseAddress) {	
    XUartNs550_Config *CfgPtr = XUartNs550_LookupConfig(BaseAddress);

	XDisconnectInterruptCntrl(CfgPtr->IntrId, CfgPtr->IntrParent);

	u16 Options = XUartNs550_GetOptions(UartPtr);
	Options = Options & ~(XUN_OPTION_DATA_INTR);
	XUartNs550_SetOptions(UartPtr, Options);
}