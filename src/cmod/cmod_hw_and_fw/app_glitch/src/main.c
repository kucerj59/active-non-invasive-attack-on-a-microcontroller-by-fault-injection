// Jakub Kučera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

#include "xparameters.h"

#include <stdint.h>
#include <sys/_intsup.h>
#include <sys/_types.h>
#include <xil_exception.h>
#include <xil_io.h>
#include <xinterrupt_wrap.h>
#include <xil_printf.h>
#include <xil_types.h>
#include <xstatus.h>
#include <xgpio.h>
#include <xuartns550.h>

#include "circular_buffer.h"
#include "uart.h"
#include "glitch_axi.h"

extern inline unsigned int CiBuff_IsEmpty(CiBuff *Ro);
extern inline unsigned int CiBuff_IsFull(CiBuff *Ro);
extern inline void CiBuff_CommitRead(CiBuff *Ro, const unsigned int Count);
extern inline void CiBuff_CommitWrite(CiBuff *Ro, const unsigned int Count);

#define U32_BYTES 4
#define BAUD_RATE 9600

#define PC_RECV_BUFF_SIZE 128
#define PC_SEND_BUFF_SIZE 128
#define TG_RECV_BUFF_SIZE 128
#define TG_SEND_BUFF_SIZE 128

// Other lengths are not supported
#define COMMAND_TYPE_LENGTH 1
// Command identifiers
#define FORWARD_MESSAGE 'F'
#define OFFSET_MESSAGE 'o'
#define FLAGS_MESSAGE 'f'
#define DIVISOR_MESSAGE 'd'
#define GLITCH_COUNT_MESSAGE 'g'
#define GLITCH_ARRAY_MESSAGE 'a'
#define STATUS_MESSAGE 's'
#define READ_ANY_REG_MESSAGE 'R'
#define WRITE_ANY_REG_MESSAGE 'W'
#define INFO_MESSAGE 'i'
#define PROGRAMMING_MODE_MESSAGE 'p'
#define TG_UART_BAUD_RATE_MESSAGE 'b'
#define PC_UART_BAUD_RATE_MESSAGE 'B'
// Custom command status codes
#define ST_UNKNOWN_COMMAND 2000L
#define ST_INVALID_DATA 2002L

typedef enum TransferType {PC_SEND, PC_RECV, TG_SEND, TG_RECV} TransferType;
typedef struct TransferParams {
    XUartNs550 *Uart;
    CiBuff *CBuff;
    volatile unsigned int *Flag;
} TransferParams;

int Init();
void InitCiBuffs();
void StartSendingAndReceiving();
void StopSendingAndReceiving();
int InfiniteLoop();
u32 ParseU32FromBuff(u8 *Buff);
void SerializeU32ToBuff(u8 *Buff, u32 Value);

static TransferParams GetTransferParams(TransferType Type);
void SendBufferTo(TransferType To, u8 *Buff, const unsigned int Count, int Forward);
void ReceiveBufferFrom(TransferType From, u8 *Buff, const unsigned int Count, int Forward);
unsigned int ForwardData(TransferType From, TransferType To, const unsigned int Count, int Forward, int Blocking);

int ForwardDataCommand();
int SetOffsetCommand();
int SetFlagsCommand();
int SetDivisorCommand();
int SetGlitchCountCommand();
int SetGlitchArrayCommand();
int GetStatusCommand();

int ReadAnyRegCommand();
int WriteAnyRegCommand();
int GetInfoCommand();

int EnterProgrammingModeCommand();
int SetTargetUartBaudRateCommand();
int SetPCUartBaudRateCommand();

void ForwardDataFromTgToPC();

void PCUartIntrHandler(void *CallBackRef, u32 Event, unsigned int EventData);
void TgUartIntrHandler(void *CallBackRef, u32 Event, unsigned int EventData);

static void EnableInterrupt();
static void DisableInterrupt();

XUartNs550 PCUart;
XUartNs550 TgUart;

XGpio ProgPinGpio;
#define PROG_PIN 1
#define PROG_PIN_CHANNEL 1

u8 PCRecvDataBuff[PC_RECV_BUFF_SIZE];
u8 PCSendDataBuff[PC_SEND_BUFF_SIZE];
u8 TgRecvDataBuff[TG_RECV_BUFF_SIZE];
u8 TgSendDataBuff[TG_SEND_BUFF_SIZE];

static volatile unsigned int SendingToTg;
static volatile unsigned int ReceivingFromPC;
static volatile unsigned int SendingToPC;
static volatile unsigned int ReceivingFromTg;

static CiBuff PCRecvCiBuff;
static CiBuff PCSendCiBuff;
static CiBuff TgRecvCiBuff;
static CiBuff TgSendCiBuff;

int main(void) {
    InitCiBuffs();

    if (Init() != XST_SUCCESS) {
        return XST_FAILURE;
    }

    // Offset, duration, width, division, flags
    SetOffset(XPAR_GLITCH_IP_0_BASEADDR, 1000);
    SetDivisor(XPAR_GLITCH_IP_0_BASEADDR, 50);
    SetGlitchCount(XPAR_GLITCH_IP_0_BASEADDR, 0);
    SetFlags(XPAR_GLITCH_IP_0_BASEADDR, 1 << 31);
    SetFlags(XPAR_GLITCH_IP_0_BASEADDR, 0);

    // Set programming pin to high
    XGpio_DiscreteWrite(&ProgPinGpio, PROG_PIN_CHANNEL, PROG_PIN);

    // Infinite read and write
    int Status = InfiniteLoop();
    if (Status != XST_SUCCESS) {
        xil_printf("Infinite loop exited due to a failure.\r\n");
        return XST_FAILURE;
    }

    return XST_SUCCESS;
}

int Init() {
    int Status = InitUart(&PCUart, XPAR_AXI_UART16550_PC_BASEADDR, 
        XPAR_CPU_CORE_CLOCK_FREQ_HZ, BAUD_RATE, 0);
    if (Status != XST_SUCCESS) {
        xil_printf("PCUart init failed\r\n");
        return XST_FAILURE;
    }

    Status = InitUart(&TgUart, XPAR_AXI_UART16550_TARGET_BASEADDR, 
        XPAR_CPU_CORE_CLOCK_FREQ_HZ, BAUD_RATE, 0);
    if (Status != XST_SUCCESS) {
        xil_printf("TargetUart init failed\r\n");
        return XST_FAILURE;
    }

    Status = SetupInterrupt(&PCUart, XPAR_AXI_UART16550_PC_BASEADDR, &PCUartIntrHandler);
    if (Status != XST_SUCCESS) {
        xil_printf("PC Uartns550 interrupt init failed\r\n");
        return XST_FAILURE;
    }

    Status = SetupInterrupt(&TgUart, XPAR_AXI_UART16550_TARGET_BASEADDR, &TgUartIntrHandler);
    if (Status != XST_SUCCESS) {
        xil_printf("Target Uartns550 interrupt init failed\r\n");
        return XST_FAILURE;
    }

    Status = XGpio_Initialize(&ProgPinGpio, XPAR_XGPIO_0_BASEADDR);
    if (Status != XST_SUCCESS) {
		xil_printf("Gpio init failed\r\n");
		return XST_FAILURE;
	}

    return XST_SUCCESS;
}

void InitCiBuffs() {
    CiBuff_Init(&PCSendCiBuff, PCSendDataBuff, PC_SEND_BUFF_SIZE);
    CiBuff_Init(&PCRecvCiBuff, PCRecvDataBuff, PC_RECV_BUFF_SIZE);
    CiBuff_Init(&TgSendCiBuff, TgSendDataBuff, TG_SEND_BUFF_SIZE);
    CiBuff_Init(&TgRecvCiBuff, TgRecvDataBuff, TG_RECV_BUFF_SIZE);
}

void StartSendingAndReceiving() {
    InitCiBuffs();

    // Initialize global variables
    SendingToPC = 0;
    SendingToTg = 0;
    ReceivingFromPC = PC_RECV_BUFF_SIZE;
    ReceivingFromTg = TG_RECV_BUFF_SIZE;

    // Start receiving
    XUartNs550_Recv(&PCUart, PCRecvDataBuff, PC_RECV_BUFF_SIZE);
    XUartNs550_Recv(&TgUart, TgRecvDataBuff, TG_RECV_BUFF_SIZE);    
}

void StopSendingAndReceiving() {
    ReceivingFromPC = 0;
    ReceivingFromTg = 0;
    // Wait for buffers to empty themselves
    while (SendingToPC || SendingToTg) {
        volatile int X = 10;
    }
}

int InfiniteLoop() {
    xil_printf("Infinite loop entered.\r\n");

    StartSendingAndReceiving();

    //#define VERBOSE_TEST // print info about received messages

    u8 CommandType [COMMAND_TYPE_LENGTH];

    while (1) {
        // Always forward data from target to PC
        ForwardDataFromTgToPC();
        // Read command received from PC, if it is in the PC buffer
        if (!CiBuff_IsEmpty(&PCRecvCiBuff)) {
            // Read command in PC buffer
            // No need to forward data to PC because it shouldn't take long
            ReceiveBufferFrom(PC_RECV, CommandType, COMMAND_TYPE_LENGTH, TRUE);

            #ifdef VERBOSE_TEST
            xil_printf("Rec com %c\r\n", CommandType[0]);
            #endif

            int Status;
            switch (CommandType[0]) {
                case FORWARD_MESSAGE:
                    Status = ForwardDataCommand();
                    break;
                case OFFSET_MESSAGE:
                    Status = SetOffsetCommand();
                    break;
                case FLAGS_MESSAGE:
                    Status = SetFlagsCommand();
                    break;
                case DIVISOR_MESSAGE:
                    Status = SetDivisorCommand();
                    break;
                case GLITCH_COUNT_MESSAGE:
                    Status = SetGlitchCountCommand();
                    break;
                case GLITCH_ARRAY_MESSAGE:
                    Status = SetGlitchArrayCommand();
                    break;
                case STATUS_MESSAGE:
                    Status = GetStatusCommand();
                    break;
                case READ_ANY_REG_MESSAGE:
                    Status = ReadAnyRegCommand();
                    break;
                case WRITE_ANY_REG_MESSAGE:
                    Status = WriteAnyRegCommand();
                    break;
                case INFO_MESSAGE:
                    Status = GetInfoCommand();
                    break;
                case PROGRAMMING_MODE_MESSAGE:
                    Status = EnterProgrammingModeCommand();
                    break;
                case TG_UART_BAUD_RATE_MESSAGE:
                    Status = SetTargetUartBaudRateCommand();
                    break;
                case PC_UART_BAUD_RATE_MESSAGE:
                    Status = SetPCUartBaudRateCommand();
                    break;
                default:
                    Status = ST_UNKNOWN_COMMAND;
            }

            if (Status != XST_SUCCESS) {
                xil_printf("Command %u failed with status code %d.\r\n", CommandType[0], Status);
                return Status;
            }
        }
    }
    
	return XST_FAILURE;
}

static INLINE unsigned int Max(const unsigned int A, const unsigned int B) {
    return A >= B ? A : B;
}

// Little endian
u32 ParseU32FromBuff(u8 *Buff) {
    u32 res = 0;
    for (int i = 0; i < U32_BYTES; ++i) {
        res += (Buff[i]) << (i * 8);
    }
    return res;
}

// Little endian
void SerializeU32ToBuff(u8 *Buff, u32 Value) {
    for (int i = 0; i < U32_BYTES; ++i) {
        Buff[i] = (u8) ((Value >> (i * 8)) & 0xFF);
    }
}

static TransferParams GetTransferParams(TransferType Type) {
    TransferParams t;
    switch(Type) {
        case PC_SEND:
            t.Uart = &PCUart;
            t.CBuff = &PCSendCiBuff;
            t.Flag = &SendingToPC;
            break;
        case PC_RECV:
            t.Uart = &PCUart;
            t.CBuff = &PCRecvCiBuff;
            t.Flag = &ReceivingFromPC;
            break;
        case TG_SEND:
            t.Uart = &TgUart;
            t.CBuff = &TgSendCiBuff;
            t.Flag = &SendingToTg;
            break;
        case TG_RECV:
            t.Uart = &TgUart;
            t.CBuff = &TgRecvCiBuff;
            t.Flag = &ReceivingFromTg;
            break;
        default:
            t.Uart = NULL;
            t.CBuff = NULL;
            t.Flag = NULL;
    }
    return t;
}

void SendBufferTo(TransferType To, u8 *Buff, const unsigned int Count, int Forward) {
    TransferParams TP = GetTransferParams(To);
    unsigned int TotalSent = 0;

    while (TotalSent < Count) {
        // Forward data from target to PC
        while (CiBuff_IsFull(TP.CBuff)) {
            if (Forward) {
                ForwardDataFromTgToPC();
            }
        }

        u8 *SendBuff;
        volatile const unsigned int Sent = CiBuff_GetWriteBuff(TP.CBuff, &SendBuff, Count - TotalSent);
        // Copy to circular buffer
        for (unsigned int i = 0; i < Sent; ++i) {
            SendBuff[i] = Buff[TotalSent + i];
        }
        TotalSent += Sent;
        // Commit write and send
        DisableInterrupt();
        CiBuff_CommitWrite(TP.CBuff, Sent);
        if (!(*(TP.Flag))) {
            u8 *ReadBuff;
            *(TP.Flag) = CiBuff_GetReadBuff(TP.CBuff, &ReadBuff, Sent);
            XUartNs550_Send(TP.Uart, ReadBuff, *(TP.Flag));
        }
        EnableInterrupt();
    }
}

void ReceiveBufferFrom(TransferType From, u8 *Buff, const unsigned int Count, int Forward) {
    TransferParams TP = GetTransferParams(From);
    unsigned int TotalRead = 0;

    while (TotalRead < Count) {
        // Forward data from target to PC
        while (CiBuff_IsEmpty(TP.CBuff)) {
            if (Forward) {
                ForwardDataFromTgToPC();
            }
        }

        u8 *ReadBuff;
        volatile const unsigned int Read = CiBuff_GetReadBuff(TP.CBuff, &ReadBuff, Count - TotalRead);
        // Copy from the circular buffer
        for (unsigned int i = 0; i < Read; ++i) {
            Buff[TotalRead + i] = ReadBuff[i];
        }
        TotalRead += Read;
        // Commit read and receive
        DisableInterrupt();
        CiBuff_CommitRead(TP.CBuff, Read);
        if (!(*(TP.Flag))) {
            u8 *SendBuff;
            *(TP.Flag) = CiBuff_GetWriteBuff(TP.CBuff, &SendBuff, Read);
            XUartNs550_Recv(TP.Uart, SendBuff, *(TP.Flag));
        }
        EnableInterrupt();
    }
}

// Returns number of sent bytes
unsigned int ForwardData(TransferType From, TransferType To, const unsigned int Count, int Forward, int Blocking) {
    TransferParams TPFrom = GetTransferParams(From);
    TransferParams TPTo = GetTransferParams(To);
    unsigned int TotalTransferred = 0;

    while (TotalTransferred < Count) {
        // Forward data from target to PC
        while (CiBuff_IsEmpty(TPFrom.CBuff) || CiBuff_IsFull(TPTo.CBuff)) {
            if (!Blocking) {
                return TotalTransferred;
            }
            if (Forward) {
                ForwardDataFromTgToPC();
            }
        }
        u8 *SendBuff;
        u8 *ReadBuff;
        // Get upper limit
        volatile unsigned int Transferred = CiBuff_GetWriteBuff(TPTo.CBuff, &SendBuff, Count - TotalTransferred);
        // Get actual count
        Transferred = CiBuff_GetReadBuff(TPFrom.CBuff, &ReadBuff, Transferred);

        // Copy from one circular buffer to another
        for (unsigned int i = 0; i < Transferred; ++i) {
            SendBuff[i] = ReadBuff[i];
        }
        TotalTransferred += Transferred;
        // Commit read and write
        DisableInterrupt();
        CiBuff_CommitRead(TPFrom.CBuff, Transferred);
        CiBuff_CommitWrite(TPTo.CBuff, Transferred);
        if (!(*(TPTo.Flag))) {
            u8 *ReadBuff;
            *(TPTo.Flag) = CiBuff_GetReadBuff(TPTo.CBuff, &ReadBuff, Transferred);
            XUartNs550_Send(TPTo.Uart, ReadBuff, *(TPTo.Flag));
        }
        if (!(*(TPFrom.Flag))) {
            u8 *SendBuff;
            *(TPFrom.Flag) = CiBuff_GetWriteBuff(TPFrom.CBuff, &SendBuff, Transferred);
            XUartNs550_Recv(TPFrom.Uart, SendBuff, *(TPFrom.Flag));
        }
        EnableInterrupt();
    }

    return TotalTransferred;
}

int ForwardDataCommand() {
    u8 Buff[U32_BYTES];
    ReceiveBufferFrom(PC_RECV, Buff, U32_BYTES, TRUE);
    u32 Length = ParseU32FromBuff(Buff);
    ForwardData(PC_RECV, TG_SEND, Length, TRUE, TRUE);
    return XST_SUCCESS;
}

int SetOffsetCommand() {
    u8 Buff[U32_BYTES];
    ReceiveBufferFrom(PC_RECV, Buff, U32_BYTES, TRUE);
    SetOffset(XPAR_GLITCH_IP_0_BASEADDR, ParseU32FromBuff(Buff));
    return XST_SUCCESS;
}

int SetFlagsCommand() {
    u8 Buff[U32_BYTES];
    ReceiveBufferFrom(PC_RECV, Buff, U32_BYTES, TRUE);
    SetFlags(XPAR_GLITCH_IP_0_BASEADDR, ParseU32FromBuff(Buff));
    return XST_SUCCESS;
}

int SetDivisorCommand() {
    u8 Buff[U32_BYTES];
    ReceiveBufferFrom(PC_RECV, Buff, U32_BYTES, TRUE);
    SetDivisor(XPAR_GLITCH_IP_0_BASEADDR, ParseU32FromBuff(Buff));
    return XST_SUCCESS;
}

int SetGlitchCountCommand() {
    u8 Buff[U32_BYTES];
    ReceiveBufferFrom(PC_RECV, Buff, U32_BYTES, TRUE);
    SetGlitchCount(XPAR_GLITCH_IP_0_BASEADDR, ParseU32FromBuff(Buff));
    return XST_SUCCESS;
}

int SetGlitchArrayCommand() {
    // Index of first glitch
    // Data length in bytes
    // Data
    const unsigned int DataLen = Max(U32_BYTES * 2, GLITCH_DATA_BYTES);
    u8 Buff[DataLen];
    ReceiveBufferFrom(PC_RECV, Buff, 2 * U32_BYTES, TRUE);
    u32 StartIndex = ParseU32FromBuff(Buff);
    u32 Length = ParseU32FromBuff(Buff + U32_BYTES);
    
    if ((StartIndex >= MAX_GLITCH_COUNT) || (Length / GLITCH_DATA_BYTES > MAX_GLITCH_COUNT)
        || (StartIndex + (Length / GLITCH_DATA_BYTES) > MAX_GLITCH_COUNT)) {
        // Throw away data and return failure
        for (u32 Index = 0; Index < Length; ++Index) {
            ReceiveBufferFrom(PC_RECV, Buff, Index, TRUE);
        }
        return ST_INVALID_DATA;
    }

    StartIndex = StartIndex * GLITCH_DATA_BYTES;

    for (u32 Offset = 0; Offset < Length; Offset += GLITCH_DATA_BYTES) {
        ReceiveBufferFrom(PC_RECV, Buff, GLITCH_DATA_BYTES, TRUE);
        // Byte by byte - Little endian
        for (u32 OffsetByte = 0; OffsetByte < GLITCH_DATA_BYTES; ++OffsetByte) {
            SetGlitchArray8(XPAR_GLITCH_IP_0_BASEADDR, StartIndex + Offset + OffsetByte, Buff[OffsetByte]);
        }
    }
    return XST_SUCCESS;
}

int GetStatusCommand() {
    u32 Status = GetStatus(XPAR_GLITCH_IP_0_BASEADDR);
    u8 Buff[U32_BYTES];
    SerializeU32ToBuff(Buff, Status);
    SendBufferTo(PC_SEND, Buff, U32_BYTES, TRUE);
    return XST_SUCCESS;
}

int ReadAnyRegCommand() {
    u8 Buff[U32_BYTES];
    ReceiveBufferFrom(PC_RECV, Buff, U32_BYTES, TRUE);
    u32 Offset = ParseU32FromBuff(Buff);        

    u32 Value = ReadReg(XPAR_GLITCH_IP_0_BASEADDR, Offset);
    SerializeU32ToBuff(Buff, Value);
    SendBufferTo(PC_SEND, Buff, U32_BYTES, TRUE);
    return XST_SUCCESS;
}

int WriteAnyRegCommand() {
    u8 Buff[2 * U32_BYTES];
    ReceiveBufferFrom(PC_RECV, Buff, 2 * U32_BYTES, TRUE);
    u32 Offset = ParseU32FromBuff(Buff);
    u32 Value = ParseU32FromBuff(Buff + U32_BYTES);
    WriteReg(XPAR_GLITCH_IP_0_BASEADDR, Offset, Value);
    return XST_SUCCESS;
}

int GetInfoCommand() {
    u8 Buff[4 * U32_BYTES];
    SerializeU32ToBuff(Buff, MAX_GLITCH_COUNT);
    SerializeU32ToBuff(Buff + U32_BYTES, GLITCH_TYPE_WIDTH);
    SerializeU32ToBuff(Buff + 2 * U32_BYTES, GLITCH_DURATION_WIDTH);
    SerializeU32ToBuff(Buff + 3 * U32_BYTES, EXTERNAL_TRIGGER_WIDTH);
    SendBufferTo(PC_SEND, Buff, 4 * U32_BYTES, TRUE);
    return XST_SUCCESS;    
}

int EnterProgrammingModeCommand() {
    // Set prog pin to 0
    XGpio_DiscreteClear(&ProgPinGpio, PROG_PIN_CHANNEL, PROG_PIN);

    while (1) {
        ForwardData(PC_RECV, TG_SEND, PC_RECV_BUFF_SIZE, TRUE, TRUE);
    }
    // This should be unreachable
    return XST_FAILURE;
}

int SetTargetUartBaudRateCommand() {
    u8 Buff[U32_BYTES];
    ReceiveBufferFrom(PC_RECV, Buff, U32_BYTES, TRUE);
    // Ensure that Uart is not sending or receiving data
    StopSendingAndReceiving();
    // Set PC Uart baud rate
    SetBaudRate(XPAR_AXI_UART16550_TARGET_BASEADDR, 
        XPAR_CPU_CORE_CLOCK_FREQ_HZ, ParseU32FromBuff(Buff));
    StartSendingAndReceiving();
    return XST_SUCCESS;
}

int SetPCUartBaudRateCommand() {
    u8 Buff[U32_BYTES];
    ReceiveBufferFrom(PC_RECV, Buff, U32_BYTES, TRUE);
    // Ensure that Uart is not sending or receiving data
    StopSendingAndReceiving();
    // Set PC Uart baud rate
    SetBaudRate(XPAR_AXI_UART16550_PC_BASEADDR, 
        XPAR_CPU_CORE_CLOCK_FREQ_HZ, ParseU32FromBuff(Buff));
    StartSendingAndReceiving();
    return XST_SUCCESS;
}

// Non-blocking forwarding of data between the target and the PC
void ForwardDataFromTgToPC() {
    ForwardData(TG_RECV, PC_SEND, TG_RECV_BUFF_SIZE, FALSE, FALSE);
}

void PCUartIntrHandler(void *CallBackRef, u32 Event, unsigned int EventData)
{
	XUartNs550 *UartPtr = (XUartNs550 *)CallBackRef;
    
	if (Event == XUN_EVENT_RECV_DATA || Event == XUN_EVENT_RECV_TIMEOUT) {
        if (ReceivingFromPC > 0) {
            // Move tail pointer of the circular buffer
            // and receive more data
            CiBuff_CommitWrite(&PCRecvCiBuff, EventData);
            u8 *Buff;
            ReceivingFromPC = CiBuff_GetWriteBuff(&PCRecvCiBuff, &Buff, PC_RECV_BUFF_SIZE);
            XUartNs550_Recv(UartPtr, Buff, ReceivingFromPC);
        } else {
            // If not receiving, discard bytes in RX FIFO
            u16 Options = XUartNs550_GetOptions(UartPtr);
	        XUartNs550_SetOptions(UartPtr, Options | XUN_OPTION_RESET_RX_FIFO);
        }
	}

    if (Event == XUN_EVENT_SENT_DATA) {
        CiBuff_CommitRead(&PCSendCiBuff, SendingToPC);
        // If buffer is not empty, send more data
        if (!CiBuff_IsEmpty(&PCSendCiBuff)) {
            u8 *Buff;
            SendingToPC = CiBuff_GetReadBuff(&PCSendCiBuff, &Buff, PC_SEND_BUFF_SIZE);
            XUartNs550_Send(UartPtr, Buff, SendingToPC);
        } else {
            SendingToPC = 0;
        }
    }

	if (Event == XUN_EVENT_RECV_ERROR) {
		u8 Errors = XUartNs550_GetLastErrors(UartPtr);
        xil_printf("PC Uart error: %hhu\r\n", Errors);
	}
}

void TgUartIntrHandler(void *CallBackRef, u32 Event, unsigned int EventData)
{
	XUartNs550 *UartPtr = (XUartNs550 *)CallBackRef;
    
	if (Event == XUN_EVENT_RECV_DATA || Event == XUN_EVENT_RECV_TIMEOUT) {        
        if (ReceivingFromTg > 0) {
            // Move tail pointer of the circular buffer
            // and receive more data
            CiBuff_CommitWrite(&TgRecvCiBuff, EventData);
            u8 *Buff;
            ReceivingFromTg = CiBuff_GetWriteBuff(&TgRecvCiBuff, &Buff, TG_RECV_BUFF_SIZE);
            XUartNs550_Recv(UartPtr, Buff, ReceivingFromTg);
        } else {
            // If not receiving, discard bytes in RX FIFO
            u16 Options = XUartNs550_GetOptions(UartPtr);
	        XUartNs550_SetOptions(UartPtr, Options | XUN_OPTION_RESET_RX_FIFO);
        }
	}

    if (Event == XUN_EVENT_SENT_DATA) {
        // If buffer is not empty, send more data
        CiBuff_CommitRead(&TgSendCiBuff, SendingToTg);
        if (!CiBuff_IsEmpty(&TgSendCiBuff)) {
            u8 *Buff;
            SendingToTg = CiBuff_GetReadBuff(&TgSendCiBuff, &Buff, TG_SEND_BUFF_SIZE);
            XUartNs550_Send(UartPtr, Buff, SendingToTg);
        } else {
            SendingToTg = 0;
        }
    }

	if (Event == XUN_EVENT_RECV_ERROR) {
		u8 Errors = XUartNs550_GetLastErrors(UartPtr);
        xil_printf("Target Uart error: %hhu\r\n", Errors);
	}
}

// Fixed typo in one of the interrupt functions. Source:
// https://github.com/Xilinx/embeddedsw/blob/master/lib/bsp/standalone/src/microblaze/xil_exception.c
static void EnableInterrupt() {
    UINTPTR val = mfmsr();
    val = val | (XIL_INTERRUPTS_MASK);
    mtmsr(val);
}

static void DisableInterrupt() {
    UINTPTR val = mfmsr();
    val = val & (~XIL_INTERRUPTS_MASK);
    mtmsr(val);
}