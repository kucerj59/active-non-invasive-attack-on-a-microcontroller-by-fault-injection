// Jakub Kučera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

#include "circular_buffer.h"

static INLINE unsigned int Min(const unsigned int A, const unsigned int B) {
    return A <= B ? A : B;
}

void CiBuff_Init(CiBuff *Ro, u8 *Buff, const unsigned int Size) {
    Ro->Size = Size;
    Ro->Count = 0;
    Ro->Head = 0;
    Ro->Tail = 0;
    Ro->Buff = Buff;
}

// To get new data call CommitRead after using data.
unsigned int CiBuff_GetReadBuff(CiBuff *Ro, u8 ** const Buff, const unsigned int MaxBytes) {
    *Buff = Ro->Buff + Ro->Head;

    if (Ro->Count == 0)
        return 0;

    const unsigned int tail = Ro->Tail;
    if (tail > Ro->Head) {
        return Min(MaxBytes, tail - Ro->Head);
    }

    return Min(MaxBytes, Ro->Size - Ro->Head);
}

unsigned int CiBuff_GetWriteBuff(CiBuff *Ro, u8 ** const Buff, const unsigned int MaxBytes) {
    *Buff = Ro->Buff + Ro->Tail;

    if (Ro->Count == Ro->Size)
        return 0;

    const unsigned int head = Ro->Head;
    if (head > Ro->Tail) {
        return Min(MaxBytes, head - Ro->Tail);
    }

    return Min(MaxBytes, Ro->Size - Ro->Tail);
}