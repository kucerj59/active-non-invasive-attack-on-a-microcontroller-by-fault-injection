// Jakub Kučera
// kucerj59@fit.cvut.cz
// Created as part of my bachelor's thesis:
// Active non-invasive attack on a microcontroller by fault injection
// Czech Technical University - Faculty of Information Technology
// 2024

#ifndef CiBuff_HEADER
#define ROBUF_HEADER

#include <sys/_types.h>
#include <xil_io.h>
#include <xil_types.h>

typedef struct CiBuff {
    unsigned int Size;
    volatile unsigned int Head;
    volatile unsigned int Tail;
    volatile unsigned int Count;
    u8 *Buff;
} CiBuff;

void CiBuff_Init(CiBuff *Cb, u8 *Buff, const unsigned int Size);

INLINE unsigned int CiBuff_IsEmpty(CiBuff *Cb) {
    return Cb->Count == 0;
}

INLINE unsigned int CiBuff_IsFull(CiBuff *Cb) {
    return Cb->Count == Cb->Size;
}

// To get new data call CommitRead after using data.
unsigned int CiBuff_GetReadBuff(CiBuff *Cb, u8 ** const Buff, const unsigned int MaxBytes);

// Must be called after GetData with Count equal to or less than the value returned by GetData.
INLINE void CiBuff_CommitRead(CiBuff *Cb, const unsigned int Count) {
    Cb->Head = (Cb->Head + Count) % Cb->Size;
    Cb->Count -= Count;
}

unsigned int CiBuff_GetWriteBuff(CiBuff *Cb, u8 ** const Buff, const unsigned int MaxBytes);

INLINE void CiBuff_CommitWrite(CiBuff *Cb, const unsigned int Count) {
    Cb->Tail = (Cb->Tail + Count) % Cb->Size;
    Cb->Count += Count;
}

#endif