# Files

Hardware description in Verilog is in ip/glitch_ip_1_0/hdl
Verilog testbenches is in ip/glitch_ip_1_0/sims
Firmware is in app_glitch

# Build

Use build_all.py script to build the hrdware and firmware.
Use build_all.py script with "vitis" argument or use vitis_build.py script to build only firmware from hardware.xsa.

# Device programming

1. Open Vitis.
2. Open the workspace with app_glitch_vitis, i.e. the parent directory of app_glitch_vitis.
3. Connect the Digilent Cmod S7 to the PC.
4. The built application can be run by pressing Run in the FLOW menu under the Build button.

# Flash programming

1. Open Vitis and the workspace with app_glitch_vitis, i.e. the parent directory of app_glitch_vitis.
2. Connect the Digilent Cmod S7 to the PC.
3. In the top navigation bar, select Vitis, Program Device.
4. Choose app glitch vitis in the Project dropdown menu.
5. Select hardware.bit in the Bitstream/PDI field.
6. Select hardware.mmi as the BMM/MMI File.
7. Select app glitch vitis.elf in the microblaze 0 field.
9. Click on Generate.
10. Wait for Vitis to generate download.bit file, and close the Program Device window.
11. Then, select Vitis, Program Flash in the top navigation bar.
12. Select app glitch vitis in the Project field.
13. Select download.bit file as Image File.
14. Choose mx25l3273f-spi-x1_x2_x4 as Flash Type.
15. Select app glitch vitis.elf file as the Init File.
16. Check Blank check after erase and Verify after flash checkboxes.
18. Click on Program to program the flash.
19. Disconnect the Digilent Cmod S7 from to PC after the flash programming is done.