# Jakub Kučera
# kucerj59@fit.cvut.cz
# Created as part of my bachelor's thesis:
# Active non-invasive attack on a microcontroller by fault injection
# Czech Technical University - Faculty of Information Technology
# 2024

# To run: vitis -s ./vitis_build.py

import os
import sys
import shutil
import vitis

project_dir = os.path.dirname(os.path.realpath(sys.argv[0]))

last_arg = sys.argv[len(sys.argv) - 1]

if len(sys.argv) > 1 and (last_arg == "-help" or last_arg == "-h" or last_arg == "--help"):
    print(f"Usage: {sys.argv[0]} [.xsa file name]")
    sys.exit()

if (len(sys.argv) < 2):
    xsa_file = "hardware.xsa"
else:
    xsa_file = sys.argv[1]

client = vitis.create_client()
client.set_workspace(path=project_dir)

platform = client.create_platform_component(name = "platform_glitch",hw = os.path.join(project_dir, "hardware.xsa") ,os = "standalone",cpu = "microblaze_0")

platform = client.get_platform_component(name="platform_glitch")
status = platform.build()

comp = client.create_app_component(name="app_glitch_vitis",platform = os.path.join(project_dir, "platform_glitch/export/platform_glitch/platform_glitch.xpfm"),domain = "standalone_microblaze_0")

# Copy source file into application's src folder
src_folder = os.path.join(project_dir, "app_glitch/src")
target_folder = os.path.join(project_dir, "app_glitch_vitis/src")
for filename in os.listdir(src_folder):
    shutil.copyfile(os.path.join(src_folder, filename), os.path.join(target_folder, filename))

comp = client.get_component(name="app_glitch_vitis")
comp.build()