# Jakub Kučera
# kucerj59@fit.cvut.cz
# Created as part of my bachelor's thesis:
# Active non-invasive attack on a microcontroller by fault injection
# Czech Technical University - Faculty of Information Technology
# 2024

import serial
import time
import warnings
import math

class SerialWriteEception(IOError):
    pass

class GlitchDevice:
    # Message codes
    FORWARD_MESSAGE = b'F'
    OFFSET_MESSAGE = b'o'
    FLAGS_MESSAGE = b'f'
    DIVISOR_MESSAGE = b'd'
    GLITCH_COUNT_MESSAGE = b'g'
    GLITCH_ARRAY_MESSAGE = b'a'
    STATUS_MESSAGE = b's'
    READ_ANY_REG_MESSAGE = b'R'
    WRITE_ANY_REG_MESSAGE = b'W'
    INFO_MESSAGE = b'i'
    PROGRAMMING_MODE_MESSAGE = b'p'
    TARGET_BAUD_RATE_MESSAGE = b'b'
    PC_BAUD_RATE_MESSAGE = b'B'

    # Status codes
    ST_UNKNOWN_COMMAND = 2000
    ST_INVALID_DATA = 2002

    # Status register masks - status is returned by get_status function.
    STATUS_RESET_VALUE = 1
    STATUS_GLITCH_TRIGGERED = 2
    STATUS_OFFSET_COUNTDOWN_DONE = 4
    STATUS_GLITCH_INSERTION_DONE = 8
    STATUS_FIRST_EXTERNAL_TRIGGER = 16

    FLAG_NONE = 0
    """ No flags """
    FLAG_OFFSET_SKIP = 1
    """ Flags register masks - set flags with set_flags function. """

    FLAG_MANUAL_TRIGGER = 4
    """ Triggers glitch when set. FLAG_GLITCH_ENABLE has to be set too. """

    FLAG_RESET_TRIGGER = 8
    """ Triggers glitch when reset changes to low. FLAG_GLITCH_ENABLE must be also set to high. """

    FLAG_FIRST_EXTERNAL_TRIGGER = 16
    """
    Enables eternal trigger. The glitch is then triggered at rising edge of the external trigger.
    When there are multiple external triggers, the other triggers are at next (higher) bits.
    """
    
    FLAG_RESET = (1 << 30)
    """
    Directly controls the reset signal output of the FPGA.
    The reset signal out of FPGA is inverted.
    """

    FLAG_GLITCH_ENABLE = (1 << 31)
    """ Set high to enable glitching, reset triggers, and reset status register. """

    # Glitch types - glitch types are passed to set_glitch_array function.
    GLITCH_NONE = 0
    GLITCH_CLOCK = 1
    GLITCH_VOLTAGE = 2

    # Constants
    U32_LENGTH = 4

    # Can be set automatically via read_platform_info
    MAX_GLITCH_COUNT = 1
    GLITCH_TYPE_WIDTH = 2
    GLITCH_DURATION_WIDTH = 14
    EXTERNAL_TRIGGER_WIDTH = 1
    GLITCH_DATA_BYTES = 2

    def __init__(self, port: str, timeout = 0.5, use_message_queue = False, verbose = False, logfile = "./tmp", log = False):
        """
        Initialize connection to the glitch tool

        Args:
            port: Comm port.
            timeout: Serial timeout.
            use_message_queue: When use_message_queue is true, messages are queued
                and sent after calling send_all()
            verbose: Displays sent messages.
            logfile: Specifies log file location. Log file is overwritten.
            log: Enables logging to a log file.
        
        Raises:
            ValueError: If the parameters, such as baud rate, are out of range.
            SerialException: If the the device cannot be found or configured.
        """

        self.serial = serial.Serial(port, 9600, timeout=timeout, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, write_timeout=None)
        self.use_message_queue = use_message_queue
        self.send_queue = bytearray()
        self.verbose = verbose
        self.log = log
        self.logfile = logfile
    
    def set_use_message_queue(self, value: bool) -> None:
        """
        Enables or disables automatic message sending.

        Args:
            value: If set to false, all messages are sent immediately.
                Otherwise, they will be sent after calling send_all function.
        """
        self.use_message_queue = value
    
    def send_all(self) -> None:
        """
        Send all queued messages. It is useful only when use_message_queue is true.

        Raises:
            SerialWriteException: In case not all bytes are sent.
        """
        self.__serial_write(self.send_queue)
        self.send_queue = bytearray()
    
    def discard_all(self) -> None:
        """
        Discard all queued messages. It is useful only when use_message_queue is true.
        """
        self.send_queue = bytearray()

    def set_offset(self, offset: int) -> None:
        """
        Set glitch insertion offset. It starts counting down from the first target clock's rising edge.

        Args:
            offset: Glitch offset in target clock cycles.
        
        Raises:
            SerialWriteException: In case not all command's bytes are sent.
            ValueError: If offset is out of range.
        """
        command = bytearray(self.OFFSET_MESSAGE) + self.__u32_to_bytearray(offset)
        self.__send_command(command)

    def set_divisor(self, divisor: int) -> None:
        """
        Set target's clock cycle to last divisor FPGA's clock cycles
        Only even values are supported. Odd values are rounded down to the closest even number.

        Args:
            divisor: Sets targets clock frequency to a 100 / divisor MHz.
        
        Raises:
            SerialWriteException: In case not all command's bytes are sent.
            ValueError: If divisor is out of range.
        """
        command = bytearray(self.DIVISOR_MESSAGE) + self.__u32_to_bytearray(divisor // 2)
        self.__send_command(command)
    
    def set_flags(self, flags: int) -> None:
        """
        Flags manipulates register that controls glitch insertion and triggers.

        Args:
            flags: There are predefined constants to ease the usage.
                FLAG_OFFSET_SKIP - If set, the offset countdown is skipped,
                    and the glitch insertion doesn't doesn't wait for the first target's clock rising edge.
                FLAG_MANUAL_TRIGGER - Triggers the glitch insertion immediately.
                FLAG_RESET_TRIGGER - Triggers glitch on reset's rising edge.
                FLAG_FIRST_EXTERNAL_TRIGGER - Enables 1st external trigger
                    that trigger glitch insertion on a rising edge.
                FLAG_RESET - Sets value of a reset signal to the target. 
                FLAG_GLITCH_ENABLE - It must be enabled for a glitch insertion to happen.
                    After every glitch insertion it has to be set low.
        """
        command = bytearray(self.FLAGS_MESSAGE) + self.__u32_to_bytearray(flags)
        self.__send_command(command)
    
    def set_glitch_count(self, count: int) -> None:
        """
        Sets glitch count. I.e. the number of pairs in glitch array.
        Afer inserting all of the pairs, all inserted glitches are stopped.

        Args:
            count: Number of inserted glitches.
        
        Raises:
            SerialWriteException: In case command is not sent.
            ValueError: If count is out of range.
        """
        if (count < 0) or (count > self.MAX_GLITCH_COUNT):
            raise ValueError("Glitch count of {count} is out of range")
        
        command = bytearray(self.GLITCH_COUNT_MESSAGE) + self.__u32_to_bytearray(count)
        self.__send_command(command)
    
    def set_glitch_array(self, array: list, index: int) -> None:
        """
        Configure glitch insertion types and duration.

        Args:
            array: This argument should contain glitch type and duration pairs
                Ie. [(glitch_type1, duration1), (glitch_type2, duration2), ...]
                glitch_type: It can be set to any combination of the following values:
                    0 (no glitch), GLITCH_CLOCK, GLITCH_VOLTAGE.
                Duration specifies the duration of the inserted glitch_type.
                    Duration of 0 has the same effect as duration of 1.
            index: Index determines the first overwritten glitch_type duration pair in FPGA's memory.
                Indexing starts from 0.
        
        Raises:
            SerialWriteException: In case command is not sent.
            ValueError: If index, array's length or array's values are out of range.
        """
        if index < 0 or len(array) + index > self.MAX_GLITCH_COUNT:
            raise ValueError(f"Write at index {index} is out of glitch array's bounds")
        
        command = bytearray(self.GLITCH_ARRAY_MESSAGE)
        # Add index
        command += self.__u32_to_bytearray(index)
        # Add Length in bytes
        command += self.__u32_to_bytearray(len(array) * self.GLITCH_DATA_BYTES)
        # Add data
        for element in array:
            command += self.__glitch_type_duration_to_bytearray(element[0], element[1])
        self.__send_command(command)

    def get_status(self, return_dict: bool = True, skip_queue: bool = True) -> dict|int|None:
        """
        Read status register. If return_dict is set to true, status is returned as a dictionary.

        Args:
            return_dict: If true, a dictionary is returned.
            skip_queue: If true, the send queue is skipped. It has an effect only when use_message_queue is true.

        Returns:
            Value of status register. If return_dict is set to true, a dictionary is returned instead.
            If skip_queue is set to false, None is returned.
        
        Raises:
            SerialWriteException: In case command is not sent.
            ValueError: If received status is out of range.
        """
        self.__send_command(self.STATUS_MESSAGE, skip_queue)

        if self.use_message_queue and not skip_queue:
            return None

        res = self.__serial_read(self.U32_LENGTH)
        number = self.__bytearray_to_u32(res)

        if not return_dict:
            return number
        
        res_dict = {}
        res_dict["STATUS_RESET_VALUE"] = number & self.STATUS_RESET_VALUE
        res_dict["STATUS_GLITCH_TRIGGERED"] = (number & self.STATUS_GLITCH_TRIGGERED) > 0
        res_dict["STATUS_OFFSET_COUNTDOWN_DONE"] = (number & self.STATUS_OFFSET_COUNTDOWN_DONE) > 0
        res_dict["STATUS_GLITCH_INSERTION_DONE"] = (number & self.STATUS_GLITCH_INSERTION_DONE) > 0
        res_dict["STATUS_EXTERNAL_TRIGGERS"] = 0
        for i in range(self.EXTERNAL_TRIGGER_WIDTH):
            res_dict["STATUS_EXTERNAL_TRIGGERS"] |= (number & (self.STATUS_FIRST_EXTERNAL_TRIGGER << i))
        res_dict["STATUS_EXTERNAL_TRIGGERS"] >>= int(math.log2(self.STATUS_FIRST_EXTERNAL_TRIGGER))
        
        return res_dict
    
    def read_peripherals_memory(self, offset: int, skip_queue: bool = True) -> int|None:
        """
        Read U32 from glitch peripheral's memory.

        Args:
            offset: Glitch peripheral's memory offset.
            skip_queue: If true, the send queue is skipped. It has an effect only when use_message_queue is true.
        
        Returns:
            Read memory value at the specified offset, if skip_queue is set to true.
        
        Raises:
            SerialWriteException: In case command is not sent.
            ValueError: If offset or received value is out of range.
        """
        command = bytearray(self.READ_ANY_REG_MESSAGE) + self.__u32_to_bytearray(offset)
        self.__send_command(command, skip_queue)

        if self.use_message_queue and not skip_queue:
            return None

        res = self.__serial_read(self.U32_LENGTH)
        return self.__bytearray_to_u32(res)

    def write_peripherals_memory(self, offset: int, value: int, skip_queue: bool = True) -> None:
        """
        Write U32 to glitch peripheral's memory.

        Args:
            offset: Glitch peripheral's memory offset.
            value: Value to be written.
            skip_queue: If true, the send queue is skipped. It has an effect only when use_message_queue is true.
        
        Raises:
            SerialWriteException: In case command is not sent.
            ValueError: If value or offset is out of range.
        """
        command = bytearray(self.WRITE_ANY_REG_MESSAGE) + self.__u32_to_bytearray(offset) + self.__u32_to_bytearray(value)
        self.__send_command(command, skip_queue)
    
    def read_platform_info(self, autoconfigure: bool) -> dict:
        """
        Read platforms configuration:
        maximum glitch count, glitch type width, glitch duration width and external trigger width.
        If autoconfigure is set to true, platform sets configurations variables according to the received values.
        During autoconfiguration the serial input is flushed.

        Args:
            autoconfigure: Automatically configures variables based on FPGA's configuration.
        
        Returns:
            Dictionary of platform's configuration variables.
        
        Raises:
            SerialWriteException: In case command is not sent.
            ValueError: If received values are out of range.
        """
        if autoconfigure:
            self.serial.reset_input_buffer()

        self.__send_command(self.INFO_MESSAGE, autoconfigure)

        res = {}
        res["MAX_GLITCH_COUNT"] = self.__bytearray_to_u32(self.__serial_read(self.U32_LENGTH))
        res["GLITCH_TYPE_WIDTH"] = self.__bytearray_to_u32(self.__serial_read(self.U32_LENGTH))
        res["GLITCH_DURATION_WIDTH"] = self.__bytearray_to_u32(self.__serial_read(self.U32_LENGTH))
        res["EXTERNAL_TRIGGER_WIDTH"] = self.__bytearray_to_u32(self.__serial_read(self.U32_LENGTH))
        
        if autoconfigure:
            self.MAX_GLITCH_COUNT = res["MAX_GLITCH_COUNT"]
            self.GLITCH_TYPE_WIDTH = res["GLITCH_TYPE_WIDTH"]
            self.GLITCH_DURATION_WIDTH = res["GLITCH_DURATION_WIDTH"]
            self.EXTERNAL_TRIGGER_WIDTH = res["EXTERNAL_TRIGGER_WIDTH"]
            self.GLITCH_DATA_BYTES = 2 ** int(math.ceil(math.log2((self.GLITCH_TYPE_WIDTH + self.GLITCH_DURATION_WIDTH) / 8)))

        return res

    def enter_programming_mode(self) -> None:
        """
        Device forwards data from PC to target and from target to PC.
        The FPGA stays in this mode, until it is reset.
        """
        self.__send_command(self.PROGRAMMING_MODE_MESSAGE)
    
    def set_baud_rate(self, new_baud_rate: int, dev_baud_rate: int|None=None) -> None:
        """
        Set baud rate of FPGA's PC facing UART and the PC's UART.

        Args:
            new_baud_rate: Baud rate to set.
            dev_baud_rate: If it is None, the PC's current baud rate is used.
                Otherwise, the PC baud rate is changed to dev_baud_rate before sending the command.
        """
        if dev_baud_rate is not None:
            # Wait before reconfiguring the port
            time.sleep(1/10)
            self.serial.baudrate = dev_baud_rate
            # Wait after reconfiguring the port
            time.sleep(1/10)
        command = bytearray(self.PC_BAUD_RATE_MESSAGE) + self.__u32_to_bytearray(new_baud_rate)
        self.__send_command(command, skip_queue=True)
        # Wait before reconfiguring the port
        time.sleep(1/10)
        self.serial.baudrate = new_baud_rate
        # Wait after reconfiguring the port
        time.sleep(1/10)
    
    def set_target_baud_rate(self, baud_rate: int) -> None:
        """
        Set baud rate of FPGA's target facing UART.

        Args:
            baud_rate: Target UARTS's baud rate.
        """
        command = bytearray(self.TARGET_BAUD_RATE_MESSAGE) + self.__u32_to_bytearray(baud_rate)
        self.__send_command(command, skip_queue=True)
    
    def read(self, size, timeout: None|float=None) -> bytes:
        """
        Read data from serial. When the timeout is not specified,
        the timeout set during the class instantion is used.
        
        Args:
            size: Maximum number of received bytes. If size is -1, read all.

        Returns:
            Up to size bytes that were received before the timeout.
        """
        old_timeout = self.serial.timeout
        if (timeout != None):
            warnings.warn("Timeout in proximity of writes might corrupt sent data")
            self.serial.timeout = timeout

        res = self.__serial_read(size)
        if (timeout != None):
            self.serial.timeout = old_timeout

        return res

    def send_to_target(self, data: bytearray|str, skip_queue: bool = False) -> None:
        """
        Send data to target device

        Args:
            data: Data to be sent to the target device.
            skip_queue: If true, the send queue is skipped. It has an effect only when use_message_queue is true.
        
        Raises:
            SerialWriteException: In case not all bytes are sent.
        """
        if isinstance(data, str):
            data = bytearray(data, "ascii")

        message = self.__add_header(self.FORWARD_MESSAGE, data)

        self.__serial_write(message, skip_queue)
    
    def send_raw_data(self, data: bytearray|str, skip_queue: bool = True) -> None:
        """
        Send data without a header.

        Args:
            data: Data to be sent.
            skip_queue: If true, the send queue is skipped. It has an effect only when use_message_queue is true.
        
        Raises:
            SerialWriteException: In case not all bytes are sent.
        """
        if isinstance(data, str):
            data = bytearray(data, "ascii")

        self.__serial_write(data, skip_queue)

    def __serial_read(self, size: int) -> bytes:
        """
        Read data from serial.
        If verbose is set to true, data are printed out.
        If log is true, data are written to the log file.

        Args:
            size: Maximum number of bytes read. If size is -1, read all.
        
        Returns:
            Up to size bytes that were received before the timeout.
        """
        if (size == -1):
            res = self.serial.read_all()
            # Change None to 0 bytes
            if (res == None):
                res = bytes(0)
        else:
            res = self.serial.read(size)

        if self.log:
            with open(self.logfile, "a") as log_file:
                log_file.write(f"R: {res}\n")

        if self.verbose:
            print("R:", res)
        
        return res

    def __serial_write(self, data: bytearray, skip_queue: bool = True) -> None:
        """
        Write data to serial, if use_message_queue is set to false or skip_queue is set to true.
        Or else the data are appended to the send queue.
        If verbose is set to true, data are printed out.
        If log is true, data are written to the log file.

        Args:
            data: Data to be sent.
            skip_queue: If true, the send queue is skipped. It has an effect only when use_message_queue is true.
        
        Raises:
            SerialWriteException: In case not all bytes are sent.
        """
        if self.use_message_queue and not skip_queue:
            self.send_queue += data
            return

        if self.log:
            with open(self.logfile, "a") as log_file:
                log_file.write(f"S: {data}\n")

        if self.verbose:
            print("S:", data)

        n = self.serial.write(data)
        if n != len(data):
            raise SerialWriteEception("Only ", n, " out of ", len(data), " bytes were sent")

    def __send_command(self, command: bytearray, skip_queue: bool = False) -> None:
        """
        Send command to FPGA.

        Args:
            command: Command to be sent.
            skip_queue: If true, the send queue is skipped. It has an effect only when use_message_queue is true.
        
        Raises:
            SerialWriteException: In case not all bytes are sent.
        """
        self.__serial_write(command, skip_queue)

    def __add_header(self, byte: bytes, content: bytearray) -> bytearray:
        """
        Add header with message type indicator and length.

        Args:
            byte: Specifies the message type.
            content: Content of the message.
        
        Returns:
            Returns content with prepended header that is composed of byte and content length.
        """
        header = bytearray(byte) + self.__u32_to_bytearray(len(content))
        return header + content

    def __u32_to_bytearray(self, number: int) -> bytearray:
        """
        Convert u32 to bytearray in little-endian format.

        Args:
            number: Number to be converted to bytearray.
        
        Returns:
            number encoded to bytearray.
        
        Raises:
            ValueError: If the number doesn't fit into U32_LENGTH bytes.
        """
        if number >> (self.U32_LENGTH * 8) != 0:
            raise ValueError(f"Number {number} is out of range")

        arr = bytearray(self.U32_LENGTH)
        for i in range(self.U32_LENGTH):
            arr[i] = (number >> (i * 8)) & 0xFF
        return arr
    
    def __bytearray_to_u32(self, arr: bytearray) -> int:
        """
        Convert bytes in little-endian format to number.

        Args:
            arr: Bytearray that contains the number.
        
        Returns:
            Number decoded from arr's bytes.
        
        Raises:
            ValueError: If arr's size is not U32_LENGTH.
        """
        if len(arr) != self.U32_LENGTH:
            raise ValueError(f"Invalid bytearray length of {len(arr)}")

        number = 0
        for i in range(self.U32_LENGTH - 1, -1, -1):
            number = (number << 8) + arr[i]
        return number

    def __glitch_type_duration_to_bytearray(self, glitch_type: int, duration: int) -> bytearray:
        """
        Converts glitch_type, duration pair to a representation that can be sent over serial to the FPGA.

        Args:
            glitch_type: Types of inserted glitch.
            duration: duration in FPGA clock cycles.
        
        Returns:
            glitch_type and duration pair encoded as a bytearray.

        Raises:
            ValueError: If glitch_type or duration is out of range specified by GLITCH_TYPE_WIDTH and GLITCH_DURATION_WIDTH.
        """
        if duration < 0 or duration >= (1 << self.GLITCH_DURATION_WIDTH):
            raise ValueError(f"Glitch duration of {duration} is out of range")
        if glitch_type < 0 or glitch_type >= (1 << self.GLITCH_TYPE_WIDTH):
            raise ValueError(f"Glitch type of {glitch_type} is out of range")

        value = duration | (glitch_type << self.GLITCH_DURATION_WIDTH)
        arr = bytearray(self.GLITCH_DATA_BYTES)
        for i in range(self.GLITCH_DATA_BYTES):
            arr[i] = (value >> (i * 8)) & 0xFF
        return arr
