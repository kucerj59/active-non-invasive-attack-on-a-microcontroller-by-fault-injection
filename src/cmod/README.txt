# Our fault injection platform implemented on Cmod S7

cmod_hw_and_fw  - hardware and firmware source code of our platform
cmod_sw         - interface in Python for sending commands to our platform
notebooks       - Files for controlling our platform from PC