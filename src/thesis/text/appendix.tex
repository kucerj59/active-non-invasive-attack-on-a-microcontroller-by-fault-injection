%---------------------------------------------------------------
\chapter{Attack on AES by Piret et al.}
\label{app:AttackAlgorithm}
%---------------------------------------------------------------

Here we give the algorithm for the attack on AES proposed by Piret et al. The algorithm~\ref{alg:AESKeyRecoveryPiret} describes how to recover the key, where $cts$ is the array of correct ciphertexts, $faulty\_cts$ is the array with corresponding erroneous ciphertexts, and \emph{Inv} suffix denotes the inverse transformation. Algorithm~\ref{alg:AESKeyRecoverySSR} shows how to decrease the search space.

\begin{algorithm}
    \caption[Recovery of $K_{10}$ with fault between the 7th and the 8th \emph{MixColumns}]{Recovery of $K_{10}$ with fault between the 7th and the 8th \emph{MixColumns}. Source: article by Piret et al.~\cite{DFAOnAESUsingSPN}, modified into pseudocode}
    \label{alg:AESKeyRecoveryPiret}
    \begin{algorithmic}[1]
        \Procedure{Recovery78Piret}{$cts$, $faulty\_cts$}
\State $D \gets \{\}$
\For{every 16-byte array $a$ with one non-zero byte}
    \Statex \Comment{Differences between ciphertexts at the output of $\theta_{R-1}$ layer}
    \State $D \gets D \cup$ MixColumns(ShiftRows($a$))
\EndFor

\State $key\_candidates \gets$ \textsc{ReduceSearchSpace}($cts[0]$, $faulty\_cts[0]$, $cts[1]$, $faulty\_cts[1]$)
\For{$c$ \textbf{in} $\{0, 4, 8, 12\}$}
    \Comment{For every quartet of $K_{10}$ key's bytes}
    \State $idx \gets 0$
    \While{$|key\_candidates| > 1$}
        \Comment{Until there is only one candidate left}
        \State $ct \gets$ ShiftRowsInv($cts[idx]$)
        \Comment{Reorder bytes by applying inverse of \emph{ShiftRows}}
        \State $faulty\_ct \gets ct$
        \State $faulty\_ct[c \dots c + 3] \gets $ ShiftRowsInv($faulty\_cts[idx]$)$[c \dots c + 3]$
        \Statex \Comment{Keep faults only in one column}

        
        \State $key\_candidates' \gets \{\}$
        \For{$key\_guess$ \textbf{in} $key\_candidates$}
            \State $d \gets$ SubBytesInv($ct \oplus key\_guess$)) $ \oplus $ SubBytesInv($faulty\_ct \oplus key\_guess$))
            \Statex \Comment{$\gamma^{-1} \circ \sigma[key\_guess](ct) \oplus \gamma^{-1} \sigma[key\_guess](faulty\_ct)$}
            \If{$d \in D$}
                \State $key\_candidates' \gets key\_candidates' \cup key\_guess$
            \EndIf
        \EndFor
        \State $key\_candidates \gets key\_candidates'$
        \State $idx \gets idx + 1$
    \EndWhile
\EndFor
\State \textbf{return} ShiftRows($key\_candidates[0]$)
\Comment{Correctly arrange the bytes of $K_{10}$}
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

\begin{algorithm}
    \caption[Search space reduction]{Search space reduction. Source: article by Piret et al.~\cite{DFAOnAESUsingSPN}, modified into pseudocode}
    \label{alg:AESKeyRecoverySSR}
    \begin{algorithmic}[1]   
        \Procedure{ReduceSearchSpace}{$ct_1$, $faulty\_ct_1$, $ct_2$, $faulty\_ct_2$}
\State $D \gets \{\}$
\For{every 16-byte array $a$ with one non-zero byte}
    \Statex \Comment{Differences between ciphertexts at the output of $\theta_{R-1}$ layer}
    \State $D \gets D \cup$ MixColumns(ShiftRows($a$))
\EndFor
\State $key\_candidates \gets \{\}$
\Comment{Set of key candidates}
\For{$c$ \textbf{in} $\{0, 4, 8, 12\}$}
    \Comment{For every column to obtain four times four bytes of $K_{10}$}

    \For{$i$ \textbf{in} $\{1, 2\}$}
        \Comment{Transform both ciphertext pairs}
        \State $ct'_i \gets$ ShiftRowsInv($ct_i$)
        \Comment{Reorder bytes by applying inverse of \emph{ShiftRows}}
        \State $faulty\_ct'_i \gets ct'_i$
        \State $faulty\_ct'_i[c \dots c + 3] \gets $ ShiftRowsInv($faulty\_ct_i$)$[c \dots c + 3]$
        \Statex \Comment{Keep faults only in one column}
    \EndFor
    \State $D' \gets \{d[c:c+1]: d \in D\}$
    \Comment{Set of values from $D$ restricted to only two bytes}
    \State $L \gets \{\}$
    \Comment{Possible bytes of $K_{10}$}
    \For{$k_c$ \textbf{in} $\{00, \dots \text{FF}\}$}
        \For{$k_{c+1}$ \textbf{in} $\{00, \dots \text{FF}\}$}
            \Comment{For every first two bytes of a key $K_{10}$ in column $c / 4$}
            \State $key\_guess \gets [00, \dots, 00, k_c, k_{c+1}, 00, \dots, 00]$
            \Comment{Pad with $c$ and $14 - c$ zeroes}
            \State $d_1 \gets$ SubBytesInv($ct'_1 \oplus key\_guess$)) $ \oplus $ SubBytesInv($faulty\_ct'_1 \oplus key\_guess$))
            \State $d_2 \gets$ SubBytesInv($ct'_2 \oplus key\_guess$)) $ \oplus $ SubBytesInv($faulty\_ct'_2 \oplus key\_guess$))
            \Statex \Comment{$\gamma^{-1} \circ \sigma[key\_guess](ct'_i) \oplus \gamma^{-1} \sigma[key\_guess](faulty\_ct'_i)$}
            \If{$d_1[c:c+1] \in D'$ and $d_2[c:c+1] \in D'$}
                \Statex \Comment{Compare differences with those in $D'$.}
                \State $L \gets L \cup key\_guess$
            \EndIf
        \EndFor
    \EndFor
    \For{$j$ \textbf{in} $\{c+2, c+3\}$}
        \Comment{Extend the key guesses to four bytes}
        \State $D' \gets \{d[c:j]: d \in D\}$
        \Comment{Restrict values in $D$ to $j - c + 1$ bytes}
        \State $L' \gets \{\}$
        \Comment{Contains keys from $L$ extended by one byte}
        \For{$guess \in L$}
            \For{$k_j$ \textbf{in} $\{00,\dots,\text{FF}\}$}
                \State $guess' \gets [00, \dots, 00, guess, k_j, 00, \dots, 00]$
                \Comment{Pad with $c$ and $15 - j$ zeroes}
                \State $d_1 \gets$ SubBytesInv($ct'_1 \oplus guess'$)) $ \oplus $ SubBytesInv($faulty\_ct'_1 \oplus guess'$))
                %\If{}
                \State $d_2 \gets$ SubBytesInv($ct'_2 \oplus guess'$)) $ \oplus $ SubBytesInv($faulty\_ct'_2 \oplus guess'$))
                %\Statex \Comment{$\gamma^{-1} \circ \sigma[guess'](ct'_i) \oplus \gamma^{-1} \sigma[guess'](faulty\_ct'_i)$}
                \If{$d_1[c:j] \in D'$ and $d_2[c:j] \in D'$}
                    \State $L' \gets L' \cup guess'$
                \EndIf
            \EndFor
        \EndFor
        \State $L \gets L'$
    \EndFor
    \State $key\_candidates \gets$ concatenate values from $L$ to candidates in $key\_candidates$
\EndFor
\State \textbf{return} $key\_candidates$
\Statex \Comment{Bytes of $key\_candidates$ are not in the correct order, but they are not reordered since they would be transformed back in the main function}
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

%---------------------------------------------------------------
\chapter[Tutorial on fault injection using our platform]{Tutorial on fault injection \\ using our platform}
%---------------------------------------------------------------

We also created a tutorial in Python in the form of a Jupyter Notebook. The tutorial offers step-by-step instructions for carrying out an attack on AES using the platform that was implemented in this work. The notebook contains only the fault injection part of the attack because its main purpose is to familiarize users with the tool.

The code is broken up into functions that the user has to fill in to perform the attack. By following the tutorial, the user can learn how to use both voltage glitching and clock glitching. The notebook also lets the user choose between two glitch trigger types. The glitches can be triggered on a reset of a target device or after receiving a trigger signal from the target. The latter allows the user to change the encryption key and the plaintext, which is encrypted by the target.

The first notebook is in a file \lstinline{FaultInjectionTemplate.ipynb}. There is also a second notebook \lstinline{FaultInjectionFull.ipynb} with all the missing code filled in.

% TODO
We were also able to get feedback on the tutorial from a student. We edited task assignments based on the observation that they did not contain enough information.

%---------------------------------------------------------------
\chapter{Build manual}
%---------------------------------------------------------------

The build process consists of three parts. First, the hardware platform has to be built in Vivado. During the second part, the application for the MicroBlaze is compiled. Then, the compiled application can be loaded onto the FPGA. Optionally, the FPGA's flash is programmed.

The build can be done manually by following the steps in sections \ref{sec:BuildHW} and \ref{sec:BuildApp}, or by running the provided \lstinline{build_all.py} script. The two programming sections are not automated.

The build process was tested with version 2023.2 of Vivado and the same version of Vitis.

\section{Building hardware}
\label{sec:BuildHW}

To generate the platform, follow these steps:

\begin{enumerate}
    \item Open Vivado (preferably version 2023.2)
    \item Click on \emph{Tools} in the top navigation bar.
    \item Select \emph{Run Tcl Script\dots}
    \item Select \emph{project\textunderscore{}glitch.tcl} and proceed with \emph{OK}.
    \item After the project is created, select in \emph{Flow Navigator}, which is on the left side of the screen, \emph{Generate Bitstream} under \emph{PROGRAM AND DEBUG}.
    \item \emph{Synthesis is Out-of-date} dialog window might pop up. Press \emph{Yes}.
    \item Then a \emph{Launch Runs} window appears.
    \item Optionally, change in the window \emph{Number of jobs} to speed up the synthesis process. Then click \emph{OK}.
    \item When the synthesis and implementation finish, another dialog window with header \emph{Device Image Generation Completed} will appear, and press \emph{Cancel}.
\end{enumerate}

Now, the platform is ready to be exported for use in Vitis:

\begin{enumerate}
    \item In the top navigation bar of Vivado, select \emph{File}, \emph{Export}, \emph{Export Hardware\dots}.
    \item Click \emph{Next} in the \emph{Export Hardware Platform} window.
    \item As output select option \emph{Include bitstream}.
    \item Continue by clicking \emph{Next}.
    \item In the \emph{Files} window, the file can be named and it's location can be changed.
    \item Then click on \emph{Finish} button.
\end{enumerate}

The hardware platform is exported, and Vivado can be closed.

\section{Building application}
\label{sec:BuildApp}

First, the hardware platform has to be added:

\begin{enumerate}
    \item Open Vitis.
    \item In the top navigation, select \emph{Open Workspace}.
    \item Select the parent directory of app\textunderscore{}glitch.
    \item In the top navigation bar, select \emph{File}, \emph{New Component}, \emph{Platform}.
    \item Name the platform and click on \emph{Next}.
    \item In the \emph{Hardware Desing (XSA)} field, choose the .xsa file created in the previous section.
    \item Click \emph{Next} twice and then click on \emph{Finish}.
    \item Locate \emph{FLOW} in the left third of the screen.
    \item Click on \emph{Build} in the \emph{FLOW} window.
\end{enumerate}

Then, the application has to be created:

\begin{enumerate}
    \item In the top navigation bar, select \emph{File}, \emph{New Component}, \emph{Application}.
    \item Name the application and click on \emph{Next}.
    \item Select the platform from the previous step and proceed by clicking \emph{Next}.
    \item Click \emph{Next} again and then \emph{Finish}.
    \item Copy files from \emph{app\_glitch\textbackslash{}src} into \emph{name of the application\textbackslash{}src}.
    \item Locate \emph{FLOW} in the left third of the screen.
    \item Click on \emph{Build} in the \emph{FLOW} window.
\end{enumerate}

\section{Device programming}

\begin{enumerate}
    \item Open Vitis.
    \item Open the workspace with app\textunderscore{}glitch\textunderscore{}vitis.
    \item Connect the Digilent Cmod S7 to the PC.
    \item The built application can be run by pressing \emph{Run} in the \emph{FLOW} menu under the \emph{Build} button.
\end{enumerate}

\section{Flash programming}

The flash can be programmed to permanently store the hardware configuration and the program in a non-volatile memory of the FPGA. Follow these steps to store the program in the FPGA's flash memory:

\begin{enumerate}
    \item Open Vitis and the same workspace as in the previous sections.
    \item Connect the Digilent Cmod S7 to the PC.
    \item In the top navigation bar, select \emph{Vitis}, \emph{Program Device}.
    \item Choose app\textunderscore{}glitch\textunderscore{}vitis in the \emph{Project} dropdown menu.
    \item Select \emph{hardware.bit} in the \emph{Bitstream/PDI} field.
    \item Select \emph{hardware.mmi} as the \emph{BMM/MMI File}.
    \item Select \emph{app\textunderscore{}glitch\textunderscore{}vitis.elf} in the \emph{microblaze\textunderscore{}0} field.
    \item The window should look like the one in the figure~\ref{fig:ProgDev}. The content of the \emph{Project} field should contain the name of the application from section \ref{sec:BuildApp}, and might differ from the one in the picture.
    
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{images/BP_Prog_edit.png}
        \caption{Program Device window}
        \label{fig:ProgDev}.
    \end{figure}
    
    \item Click on \emph{Generate}.
    \item Wait for Vitis to generate \emph{download.bit} file, and close the \emph{Program Device} window.
    \item Then, select \emph{Vitis}, \emph{Program Flash} in the top navigation bar.
    \item Select app\textunderscore{}glitch\textunderscore{}vitis in the \emph{Project} field.
    \item Select \emph{download.bit} file as \emph{Image File}.
    \item Choose \lstinline{mx25l3273f-spi-x1_x2_x4} as \emph{Flash Type}.
    \item Select app\textunderscore{}glitch\textunderscore{}vitis.elf file as the \emph{Init File}.
    \item Check \emph{Blank check after erase} and \emph{Verify after flash} checkboxes.
    \item The window should look like the one in the figure~\ref{fig:ProgFlash}. Again, the \emph{Project} field's content might be different.
    \begin{figure}
    \centering
        \includegraphics[width=\textwidth]{images/BP_Flash_edit.png}
        \caption{Program Flash window}
        \label{fig:ProgFlash}
    \end{figure}
    \item Click on \emph{Program} to program the flash.
    \item Disconnect the Digilent Cmod S7 from to PC after the flash programming is done.
\end{enumerate}
