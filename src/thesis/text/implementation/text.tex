%---------------------------------------------------------------
\chapter{Implementation}
\label{chap:Implementation}
%---------------------------------------------------------------

In this chapter, we implement the designed hardware, firmware, and software. We also write about the technologies we used during the implementation.

%---------------------------------------------------------------
\section{Hardware implementation}
%---------------------------------------------------------------

The hardware description is written in the Verilog hardware description language. We chose Verilog because of our prior experience with it and because it is supported by Vivado~\cite{Vivado} design software for FPGAs, which we used to implement the hardware.

\subsection{Hardware overview}

The majority of the hardware blocks at the highest level of the design are connected via AXI4-Lite, which is a subset of AXI4. In this work, we will refer to the AXI4-Lite as the AXI for brevity.

The hardware consists of a MicroBlaze processor, glitch peripheral, GPIO AXI peripheral, two UART interfaces, Clocking Wizard, and other components that are required by the ones mentioned above, such as memory, AXI interrupt controller, and AXI interconnect.

We use a 32-bit MicroBlaze processor. MicroBlaze~\cite{MicroBlaze} is a RISC (Reduced Instruction Set Computer) processor, is synthesizable on an FPGA, and supports AXI. We chose MicroBlaze because it is supported by Vivado design tool, and it supports AXI, which can be used to connect it to other components. The processor has 64 KB of memory at its disposal. Originally it was 32 KB large, but the complete firmware couldn't fit in it.

A Clocking Wizard block generates a 100 MHz clock that drives every block in the design. The frequency of this generated clock also determines the glitch resolution.

There are two UART 16650 interfaces with configurable baud rates. One of them is used for communication with a computer, and the other for communication with a target device. The design also contains one GPIO block that controls a programming signal from the FPGA to the target. All three of these components are connected to the Microblaze via an AXI interconnect.

Last, a glitch IP (Intellectual Property)\footnote{Intellectual property is a term used for reusable units.} is connected to MicroBlaze via an AXI. The peripheral is described in detail in the following section.

The top level diagram of the hardware is shown in figure~\ref{fig:BlockDiagram}. The glitch IP is highlighted in green.

\begin{sidewaysfigure}
    \caption{Top level diagram of the FPGA's hardware}
    \label{fig:BlockDiagram}
    \includesvg[inkscapelatex=false, width=\textwidth]{images/bd2.svg}
\end{sidewaysfigure}

\subsection{Glitch IP}
% glitch\_ip\_v1\_0.v

The glitch IP is a standalone unit that can be connected to existing designs via AXI. This peripheral is responsible for glitch insertion, target clock generation, and trigger event processing. It is divided into three modules:

\begin{itemize}
    \item AXI module that also contains configuration registers.
    \item Glitch module that is responsible for glitching.
    \item FIFO adapter module that removes the need for the glitch module to know the exact addresses of glitch durations and glitch types.
\end{itemize}

The glitch parameters are transferred over the AXI to the configuration registers, and the values from the status registers are sent back. In addition to the AXI, the glitch IP has other inputs and outputs:

\begin{itemize}
    \item \emph{External} trigger input signal.
    \item \emph{Target clock} output signal.
    \item \emph{Glitch} output signal that controls the crowbar circuit.
    \item Inverted target reset output signal.
\end{itemize}

The modules of the glitch IP are parameterized to provide a good foundation for possible future modifications of the glitch module. The maximum glitch duration, the count of glitch signals, the number of trigger inputs, and the maximum glitch count are all configurable using  parameters of the Verilog module.

\subsection{AXI module}
% glitch\_ip\_v1\_0\_S00\_AXI.v

The AXI logic of this module was generated with a Vivado IP creator. Then, we modified the AXI module to output glitch parameters to the glitch module and to the FIFO adapter. The glitch parameters are the following:

\begin{itemize}
    \item \emph{Offset}.
    \item \emph{Flags}.
    \item \emph{Divisor}.
    \item \emph{Glitch count}.
    \item \emph{Glitch array data} that contain \emph{glitch duration} and \emph{glitch type} pairs.
\end{itemize}

We also added a three port memory to the configuration registers for storing thousands of \emph{glitch duration} and \emph{glitch type} pairs, which are used to insert multiple glitches. The three port memory was implemented to use FPGA's memory resources efficiently.

\subsection{Three port memory}
% three\_port\_memory.v

We implemented the three port memory so that it might be instantiated as BRAM, or in other words BRAM can be inferred from the code. An option to infer BRAM from the code is better than forced BRAM because the synthesis tools can choose the optimal way to generate it in the hardware. Also, the code is platform independent since it does not contain anything platform specific.

However, BRAM has some limitations. BRAM on the FPGA of CMod S7 supports up to three ports, but we needed three ports. We solved this by combining two memories together. Since we needed only one write port and two read ports, we write the same data to both memories and use the second port of every memory for reading.

BRAM inferring did not work, when we added byte write enables that are required by the AXI protocol. We had to simulate a byte write enable by reading the current value from one of the memories before writing. Another problem that we had to deal with was a mismatch between data sizes that are transferred over AXI and data sizes that are read by the glitch module through the FIFO adapter. We solved it by changing one of the memories to an asymmetric type. That means that the widths of the ports are different.

The inputs and outputs of the three port memory are shown in figure~\ref{fig:ThreePortMemory}.

\begin{figure}[h]
    \caption{Three port memory I/O}
    \label{fig:ThreePortMemory}
    \centering
    \includesvg[width=0.45\textwidth]{images/ThreePortMemory.drawio.svg}
\end{figure}

\subsubsection{True dual port memory}
% tdp\_memory.v

True dual port memory module is part of the three port memory and contains Verilog code for inferring true dual port BRAM. The memory is called true dual port because it contains two independent ports that can be used to read data from the memory and to write data to the memory.

One port of true dual port memory is used for getting the data requested via AXI and the other port is used for simulating byte write enable for this memory and the asymmetric memory since they contain the same data, and additionally, asymmetric memory does not support two read ports.

\subsubsection{Asymmetric memory}
% asymmetric\_memory.v

Asymmetric memory is the second submodule of the three port memory. It contains a code for inferring asymmetric BRAM. It has one write port and one read port, which might be of a different width than the write port. The different read port widths are needed because the \emph{glitch duration} and \emph{glitch type} pair may not have the same width as the width of the data on the AXI bus, which are 32 bits wide.

\subsection{FIFO adapter}
% fifo\_adapter.v

We opted for inserting an adapter between the three port memory and the glitch module to abstract the location of data in the memory from the glitch module. The implementation of the FIFO adapter does not add any additional delay to the path between the memory and the glitch module because an increase in the read latency of the glitch parameters would not allow the MGIC (Multiple Glitch Insertion Controller), which is part of the glitch module, to function properly.

The input and output signals of these modules are shown in figure~\ref{fig:FIFOAdapter}.When the \emph{next} is set high, FIFO retrieves new data from the asymmetric port of the three port memory, increments the internally stored memory address, and sends the fetched data. When no new data are available for reading, the FIFO asserts the \emph{empty} signal. The availability of data is recognized by comparing an address with the \emph{glitch count} value. The \emph{glitch count} indicates how many records should be read from the memory. Therefore, it also determines the number of inserted glitches. Lastly, the \emph{reset} signal sets the internally stored address to zero.

\begin{figure}
    \caption{FIFO adapter IO}
    \label{fig:FIFOAdapter}
    \centering
    \includesvg[width=0.38\textwidth]{images/FIFOAdapter.drawio.svg}
\end{figure}

\subsection{Glitch module}
% glitch\_module.v

The glitch module is a wrapper for a group of modules that together perform all operations related to glitching, including trigger signal processing and clock generation. The connection of the modules is shown in figure \ref{fig:GlitchModule}.

Here we list the most important modules contained in the glitch module:

\begin{sidewaysfigure}
    \caption{Glitch module}
    \label{fig:GlitchModule}
    \includesvg[width=\textwidth]{images/GlitchModule.drawio.svg}
\end{sidewaysfigure}

\begin{itemize}
    \item Target clock generator --- Generates the \emph{target clock} signal.
    \item Glitch trigger module --- Controls, when the glitch insertion starts.
    \item Offset countdown module --- Delays the glitch insertion by a fixed number of the target clock cycles.
    \item Multiple glitch insertion controller (MGIC) --- Controls durations and types of the injected glitches.
\end{itemize}

\subsubsection{Flags splitting module}
% flags\_splitter.v

The flags splitting module splits data from a flags configuration register into individual signals. Therefore, this module defines what bits of the flags mean. The meaning of the individual bits is shown in table~\ref{tab:Flags}.

\begin{table}
    \caption{Meaning of flags bits}
    \label{tab:Flags}
    \centering
    \begin{tabular}{r|l|l}
         Bit & Flag & Meaning\\
         \hline
         0 &  skip offset & When set to high, glitch insertion skips offset countdown. \\
         2 & manual trigger & Triggers glitch immediately. \\
         3 & reset trigger & Starts glitching after reset is set to low. \\
         4 & external trigger & Allows an external signal to trigger glitching. \\
         30 & target reset & Controls the output of the reset signal to the target. \\
         31 & glitch enable & Enables glitch injection. It must be set low after an insertion. \\
    \end{tabular}
\end{table}

\subsubsection{Target clock generator module}
% target\_clock\_generator.v

This module generates a \emph{target clock} signal by dividing the main 100 MHz clock that is generated by the Clocking Wizard. \emph{Divisor} input determines how many clock cycles of the main clock a half-period of the target clock lasts. This module also generates a signal one clock cycle before the rising edge of the target clock to allow other modules to synchronize the glitch insertion with the \emph{target clock}.

The inputs and outputs are shown in figure~\ref{fig:TargetClockGeneratorIO}, and the clock generator state diagram is shown in figure~\ref{fig:TargetClockGeneratorSM}.

\begin{figure}[h]
    \centering
    \begin{minipage}{0.45\textwidth}
        \caption{Target clock generator I/O}
        \label{fig:TargetClockGeneratorIO}
        \centering
        \includesvg[width=\textwidth]{images/TargetClockGenerator.drawio.svg}
    \end{minipage}\hfill
    \begin{minipage}{0.45\textwidth}
        \caption[Target clock generator state diagram]{Target clock generator state \\ machine}
        \label{fig:TargetClockGeneratorSM}
        \centering
        \includesvg[width=\textwidth]{images/TargetClockGeneratorSM.drawio.svg}
    \end{minipage}
\end{figure}

\subsubsection{Target reset module}
% target\_reset\_module.v

The value of \emph{target reset} is controlled by a single bit from the flags register, which is denoted as the target reset flag. When the reset flag transitions to low, the reset signal does too, but the transition of the reset signal from high to low only occurs on the rising edges of the target clock to ensure that the trigger signal, to which the \emph{target reset} is connected, always occurs at the same time relative to the target clock.

\subsubsection{Glitch trigger module}
% glitch\_trigger.v

The glitch trigger component evaluates the state of the flags register and trigger signals, and outputs a single \emph{trigger} signal enables offset countdown module or MGIC, if the offset countdown is skipped. The \emph{trigger} is set high when an input \emph{trigger} signal is set high, a corresponding flag is asserted, and the glitching is enabled. Only one of the trigger signals with its enable signal has to be set high to assert the \emph{trigger} signal.

\subsubsection{Offset countdown module}
% offset\_countdown.v

The offset countdown module delays the glitch insertion by a number of target clock cycles, which is stored in a glitch offset configuration register. The countdown starts when the \emph{trigger} signal from the glitch trigger module is asserted. This module can be skipped during a glitch insertion by setting a flag in a configuration register. After the countdown has finished, the \emph{offset countdown done} signal is set high.

The module's I/O and its state diagram are shown in figures~\ref{fig:OffsetCountdownIO} and~\ref{fig:OffsetCountdownSM} respectively.

\begin{figure}[h]
    \centering
    \begin{minipage}{0.45\textwidth}
        \caption{Offset countdown I/O}
        \label{fig:OffsetCountdownIO}
        \centering
        \includesvg[width=0.85\textwidth]{images/OffsetCountdown.drawio.svg}
    \end{minipage}\hfill
    \begin{minipage}{0.45\textwidth}
        \caption{Offset countdown state diagram}
        \label{fig:OffsetCountdownSM}
        \centering
        \includesvg[width=\textwidth]{images/OffsetCountdownSM.drawio.svg}
    \end{minipage}
\end{figure}

\subsubsection{Multiple glitch insertion controller}
% multiple\_glitch\_insertion\_controller.v

Multiple glitch insertion controller manages when glitches are inserted and what types of glitches are injected. This module does not handle how glitches are inserted, but it generates enable signals for the modules that do the actual glitching.

The glitching starts if one of two conditions is satisfied. Either the \emph{offset countdown done} is set high, or a trigger event is received, and the skip offset is asserted via its flag bit.

First, we describe the general principle behind the MGIC. The controller receives \emph{glitch duration} and \emph{glitch type} pairs from the FIFO adapter, and outputs the received \emph{glitch type} for the number of clock cycles specified by the \emph{glitch duration}. After the duration has passed, another \emph{glitch type} is output for a new \emph{glitch duration} that is again received from the FIFO. This continues until the FIFO asserts the \emph{empty} signal. After that, a \emph{MGIC done} signal is set high to signal that the glitching is completed.

Now we write about the MGIC implementation in greater detail. This module supports glitch durations of one clock cycle and longer. The upper limit is given by the width of the glitch duration input bus. Since the memory behind the FIFO adapter has a one clock-cycle read latency, we fetch the glitch parameter pairs before the previous glitch duration passes.

When the glitching starts, the second \emph{glitch type} and \emph{glitch duration} pair is requested from the FIFO by raising the \emph{next} signal. The second pair is requested before a glitch is inserted because there would not be enough time to insert the second glitch on time if the first \emph{glitch duration} was a single clock cycle.

After the second glitch pair is requested, a first glitch is inserted for the number of FPGA's clock cycles specified by the \emph{glitch duration}. In the last cycle of a glitch, another pair of parameters is requested. This continues until the FIFO adapter asserts \emph{empty} signal, which means that all glitches were inserted. After that, all glitch signals are cleared and a \emph{MGIC done} signal is set high, and written into a status register in a separate module.

The MGIC introduces a two-cycle delay on the \emph{glitch type} bus. Due to this, the \emph{target clock} signal has to be also delayed by two clock cycles to preserve their relative offset. The \emph{target clock} is delayed even when the glitching is not active to keep the period of the target clock stable.

To better vizualize the function of this module, we provide an I/O diagram in figure~\ref{fig:MGICIO}, and the state diagram in figure~\ref{fig:MGICSM}

\begin{figure}
    \caption{Multiple glitch insertion controller I/O}
    \label{fig:MGICIO}
    \centering
    \includesvg[width=0.58\textwidth]{images/MGIC.drawio.svg}
\end{figure}

\begin{figure}
    \caption{Multiple glitch insertion controller state diagram}
    \label{fig:MGICSM}
    \centering
    \includesvg[width=\textwidth]{images/MGICSM.drawio.svg}
\end{figure}

\subsubsection{Register row module}
% register\_row.v

We used this module to introduce signal delay for synchronization purposes of a \emph{target clock} and \emph{glitch} signals and to prevent clock domain crossing issues when reading trigger signals from the target.

\subsubsection{Clock glitch module}
% clock\_glitch.v

The clock glitch module performs the XOR operation of a \emph{target clock} signal and a \emph{glitch} signal, which is generated by the MGIC. The clock glitch is controlled by the lowest bit of the \emph{glitch} signal.

\subsubsection{Status register module}
% status\_register.v

The status register gives feedback on the progress of the glitch insertion process to the user. It tracks the following events and signal values:

\begin{itemize}
    \item External trigger values.
    \item Glitch triggered event.
    \item End of the offset countdown.
    \item End of the whole glitch insertion process, i.e. the moment after all glitches are inserted.
    \item \emph{Target reset} signal value.
\end{itemize}

The \emph{external trigger} values are cleared after the glitch insertion is disabled because otherwise short signals could easily be missed when reading the value of the status register.

%---------------------------------------------------------------
\section{Firmware implementation}
%---------------------------------------------------------------

The role of the firmware is to provide an interface through which the platform can be configured. The firmware receives commands from a computer, and then it either configures the glitch insertion parameters or interacts with the target.

We wrote the firmware in C programming language because it is supported by the Vitis~\cite{Vitis} integrated development environment. We used Vitis for firmware development and for FPGA programming.

\subsection{Commands}
\label{subsec:Commands}

Communication between the computer and the FPGA is command-based. The PC sends a command, and the FPGA performs an operation defined by the command and optionally sends some data back. Commands are identified by the first byte of a transfer. The length of the command's content depends on the command. The available commands are:

\begin{itemize}
    \item Forward data that forwards the received data to the target.
    \item Set offset that writes the value to the glitch offset register in the glitch IP.
    \item Set flags command that sets glitch flags.
    \item Set divisor command sets the ratio between the target clock and the FPGA's 100 MHz clock. The sent value defines how many clock cycles a target's clock half-period lasts.
    \item Set glitch count that determines how many elements are valid in the glitch array.
    \item Set glitch array that overwrites the glitch type and the glitch duration pairs from a specified offset.
    \item Get glitch status that returns the content of the status register in the glitch IP.
    \item Get info command returns the platform's hardware capabilities such as maximum glitch count.
    \item Read register reads any register in the glitch IP.
    \item Write register writes to any register in the glitch IP.
    \item Enter programming mode command starts infinite forwarding of data between the PC and the target without the need for the send data command, and asserts the \emph{programming} signal.
    \item Set PC UART baud rate changes the baud rate of the UART that is connected to the computer.
    \item Set target UART baud rate changes the baud rate of the target facing UART.
\end{itemize}

The data that are received from the target, are immediately forwarded to the PC.

Upon receiving an invalid message, the program execution stops in order not to cause any harm to the target by executing invalid commands. The platform has to be reset by pressing \emph{BTN0} on Cmod S7.

\subsection{AXI glitch}

Constants such as glitch type width, glitch delay width, and maximum glitch count are defined as macros. When the hardware configuration is changed, these values have to be manually updated to correspond with the hardware's capabilities.

\subsection{Circular buffer}

We implemented a circular buffer to store data received over UART in an interrupt service routine. A circular buffer is a fixed-size structure that maintains a pointer to the start and the end of the stored data. Retrieving data moves the pointer pointing at the start of the data and writing moves the second pointer.

Data from our implementation of the circular buffer are obtained by requesting a pointer to the start of the data. After the required operation is performed on the data, the data are removed from the buffer by calling a function that moves the start of the buffer. The two-step process is used to avoid creating unnecessary copies of the data that could be used directly from the buffer.

\subsection{UART}

Both UART interfaces are configured to eight bits of data length with one stop bit and without parity. Also, they feature 16 character transmit and receive FIFOs. The default baud rate of both UART interfaces is 9600 bauds. The baud rate can be changed by sending a designated command to the FPGA.

The two UART interfaces are interrupt-driven. We set up interrupt service routines to store the received data in circular buffers. Each UART has its own buffer. The UARTs can transmit data from the circular buffers, or a different buffer can be specified.

%---------------------------------------------------------------
\section{Software implementation}
%---------------------------------------------------------------

We created an interface for communication with the FPGA in the Python programming language. We implemented the interface in the form of a Python class that stores the hardware configuration and provides abstraction from the communication protocol between the computer and the FPGA. The software includes functions that send all of the commands mentioned in the firmware implementation section~\ref{subsec:Commands}. In addition, there is a function for reading data from the UART interface, and there are also some other functions that improve the code readability and user experience.

We implemented the software in such a way that it does not abstract from the inner workings of the fault injection platform. Listing~\ref{lst:CmodSW} shows an example of how to insert a clock glitch with the implemented interface. In the example, the clock glitch is inserted with offset of one and duration of two and is triggered by an external trigger.

\begin{lstlisting}[caption={Clock glitch insertion example using our platform},label=lst:CmodSW,captionpos=t,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=Python]
    dev.set_offset(1)
    dev.set_glitch_count(1)
    dev.set_glitch_array([(dev.GLITCH_CLOCK, 2)], 0)
    dev.set_flags(dev.FLAG_GLITCH_ENABLE |
        dev.FLAG_FIRST_EXTERNAL_TRIGGER)
\end{lstlisting}

We used the pySerial module to handle communication over the serial line. pySerial~\cite{PySerial} is a Python module for accessing serial ports. The software also provides the option to log the communication and the option to send all data in one burst.

We also created a file with examples that show how to use the platform. The examples are also in Python. The examples include clock glitching, voltage glitching, various trigger possibilities, and multiple fault injection. The examples are implemented in file \lstinline{Examples.ipynb}.