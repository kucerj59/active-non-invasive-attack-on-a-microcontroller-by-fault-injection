%---------------------------------------------------------------
\chapter{Design}
\label{chap:Design}
%---------------------------------------------------------------

In this chapter, we design the fault injection platform based on Cmod S7. We took into consideration the features of the Cmod S7 during the design process of the fault injection platform.

Cmod S7 is a board with an FPGA designed for use with breadboards because it has 32 digital I/O pins at the bottom of the board. The FPGA on Cmod S7 is Xilinx Spartan-7. It is also equipped with a PMOD (Peripheral Module) connector, which has 8 additional I/O pins and 2 pairs of pins dedicated to ground and power delivery. Another feature of the CMod S7 is a USB-UART bridge, which can be used for communication with a computer.~\cite{CMODS7}

This chapter is divided into three parts. First, we focus on the hardware design that is implemented using the flexible logic inside of the FPGA. Then, we design the firmware that controls the hardware. Lastly, we design an interface that runs on the computer. The interface allows users of the platform to control the glitch parameters and retrieve the results in a simple way.

%---------------------------------------------------------------
\section{Hardware design}
%---------------------------------------------------------------

The foundation of every platform is a well-designed hardware. The functional requirements for the hardware are the following:

\begin{itemize}
    \item Capability to insert voltage glitches.
    \item Ability to insert clock glitches.
    \item Controllable glitch offset and delay.
    \item Controllable glitch width.
    \item Capability to trigger glitch insertion.
    \item Generation of a clock for a target.
    \item Interface for communication with a target device.
    \item Adjustable glitch parameters from a computer.
\end{itemize}

\subsection{Voltage glitch insertion}

One of the requirements is the voltage glitching capability. The voltage glitch is inserted via a crowbar circuit. The crowbar circuit can be driven by a single signal. Therefore, we need to design a mechanism to control the glitch signal. The signal can be set to high to activate a glitch by driving a transistor in the crowbar circuit, or set to low to not insert a glitch.

The glitch insertion is also controlled by offset, delay, and width of the glitch. These parameters are described in the following subsections.

\subsection{Clock glitch insertion}

The clock has its own glitch insertion signal that can be enabled independently of the voltage glitch insertion signal, but it is controlled by the same offset, delay, and width parameters. When the clock glitch is enabled, it flips the value of the target clock signal by using the XOR operation on the target clock signal.

\subsection{Glitch offset}

To be able to insert glitches precisely at the desired time, we decided to use two parameters: glitch offset and glitch delay. The two parameters are used to enable long delays between a trigger and an injected glitch and also to ease the synchronization of the glitch insertion with the target's clock for the user of the platform.

The glitch offset parameter determines how many target clock cycles it takes to insert a glitch. The glitch offset is also synchronized with the target's clock. That means that a fault injection without using a glitch delay always activates at the rising edge of the target's clock. This is especially necessary for clock glitching, where it is desirable to have control over the glitch's position relative to a target clock signal.

The delay parameter is implemented in the same way as the glitch width, and for that reason, it is described in the subsection~\ref{subsec:GlitchWidthDelay} together with the glitch width.

\subsection{Glitch width and glitch delay}
\label{subsec:GlitchWidthDelay}

We designed the hardware so that the glitch width and the glitch delay share the same hardware. What differs between a glitch delay and a glitch width is another parameter named glitch type. The glitch width and glitch delay parameters are stored as an array of integers. An additional array of glitch-type variables specifies whether a glitch should be inserted and eventually what type of glitch should be inserted. To unify the naming of the glitch widths and delays in the first array, we call the array glitch duration array and the values in it glitch duration.

Storing both width and delay as a single variable together with a glitch type in a glitch duration array has also an added benefit. By increasing the array size to more than two, our design is capable of inserting multiple glitches in succession. For example, a multiple glitch configuration would contain glitch durations in one array and alternating glitch-on and glitch-off values in the second array.

The glitch type can also specify that both clock glitching and voltage glitching should be active at the same time.

Figure~\ref{fig:DelayOffset} shows the difference between the glitch offset and the glitch duration. The resolution of the offset is in target clock cycles and the resolution of the durations is in the FPGA's clock cycles. Alternatively, duration 1 and duration 2 could be denoted as the glitch delay and the glitch width, respectively.

\begin{figure}
    \caption{Difference between glitch offset and glitch duration}
    \label{fig:DelayOffset}
    \centering
    \includesvg[width=\textwidth]{images/OffsetDelay.drawio.svg}
\end{figure}

\subsection{Target clock generation}

The target clock is generated from the FPGA's clock by dividing it. Doing it this way ensures that its rising edges are synchronized with the FPGA's clock. The downside of this design is that the target clock frequency generator does not support fractional divisors. If we allow the generation of an arbitrary clock frequency of the target clock signal, the glitches would not always be inserted at the same position in the target clock cycle.

\subsection{Glitch trigger}

The glitch is generally inserted relative to some reference point. The reference point can be an event in the target device, such as a rising edge of an output signal.

We designed three trigger options:

\begin{itemize}
    \item Falling edge of the target reset signal.
    \item Rising edge of the external trigger signal.
    \item Manual trigger.
\end{itemize}

The falling edge of the target reset signal is useful in situations when the target runs an algorithm after it is reset. The rising edge of an external trigger can be used in instances that do not involve resetting the target device. Finally, the primary purpose of the manual trigger is for testing.

The external trigger goes through two registers in a series before its input is used in further logic. This is because of a clock domain crossing that exists between the FPGA's clock and the microcontroller's clock.

\subsection{Communication interfaces}

We chose the UART interface as the communication interface between the FPGA and the PC since the CMod S7 comes with a USB-UART bridge.

For data transfer between the FPGA and the target, we also used a UART interface. Hence, the FPGA design contains two UART interfaces.

\subsection{Adjustable glitch parameters}

To make the glitch parameters configurable, the glitch settings must be stored in configuration registers, which are accessible from a PC. In our design, a microprocessor receives commands from the USB-UART connection to the PC and then configures the configuration registers via AXI.

The AXI is inserted between the microprocessor and the configuration registers together with the fault injection hardware to make the glitch insertion circuitry independent from other components of the fault injection platform, i.e. the microprocessor and the communication interfaces. We selected the AXI because of the support provided by the tools, which we name in the implementation section.

%---------------------------------------------------------------
\section{Firmware design}
%---------------------------------------------------------------

Because our design uses a microprocessor to transfer data between the UART interfaces and the fault injection peripheral, we need to design firmware for the microprocessor as well. The functional firmware requirements are as follows:

\begin{itemize}
    \item Capability to forward data from the target device to the PC.
    \item Ability to forward data from the PC to the target device.
    \item Capability to receive commands from the computer.
\end{itemize}

We use interrupts to copy the input from both UART interfaces into two circular buffers. The interrupts are used to avoid missing data transmissions while the processor is executing other code. The main program routine then performs the necessary operations on the received data. Sending data is also handled via interrupts, and there are two circular buffers for sending data as well.

The commands can vary in length and content. Some of them might set glitch parameters, and others might have data that need to be forwarded to the target. Every command starts with a one-byte header, which contains a command identifier. The command identifier determines how many bytes of data are there to read, what the data represent, and what action should be performed on the received data.

No response is sent back from the FPGA after receiving the data or executing a command unless an error occurs. The data received from the target device are sent immediately, as well as any error messages.

%---------------------------------------------------------------
\section{Software design}
%---------------------------------------------------------------

Another part of the fault injection platform is the software, which runs on a computer. The software offers an easy-to-use programming interface, and servers as an abstraction from data encoding. The software requirements include the following:

\begin{itemize}
    \item Capability to set control the glitch parameters.
    \item Capability to receive data from the target.
\end{itemize}

We designed the software to be a counterpart of the firmware. The software includes control over the glitch parameters and the configuration of both UART interfaces. The software converts function calls into commands that are then encoded for transfer and sent over the USB-UART connection.