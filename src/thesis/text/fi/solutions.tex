%---------------------------------------------------------------
\section{Existing fault injection solutions}
%---------------------------------------------------------------

Here is given a brief overview of how a typical voltage and clock fault injection tool functions. Some fault injection techniques, such as clock glitching or voltage glitching, require that the glitch is inserted at a specific time in the execution of a program. One possible way to insert a glitch at a specific time in the code execution is to wait for the reception of a trigger event. For example, a logic level change of the target device's output or a specific character received via a communication interface. Then a given number of target's clock cycles and an optional finer offset later a glitch is inserted.

Another important factor is the duration of the glitch. Sometimes multiple glitches have to be inserted for a successful fault injection attack. In that case, it is desirable to be able to control each delay between inserted glitches and the length of each glitch individually.

A comparison of fault injection capabilities of various solutions can be done based on the variety and flexibility of trigger mechanisms, glitch offset range and resolution, glitch width range and resolution, a maximum count of inserted glitches and a voltage fault injection mechanism. Then there are features for controlling a target device. These can include, but are not limited to, target clock frequency generation range, communication with the target device, and ability to program the target device.

%---------------------------------------------------------------
\subsection{NewAE ChipWhisperer}
%---------------------------------------------------------------

O'Flynn created an open source side-channel analysis platform that is capable of clock glitching and power trace capture. The original ChipWhisperer's reference implementation runs on a ZTEX Spartan 6 LX25 FPGA Module, but it can be programmed into the control FPGA on SAKURA-G or SASEBO-W platform. The ChipWhisperer's FPGA design is modular. Modularity is achieved by connecting all blocks, such as a glitch generator or a trigger unit, to a central register controller.~\cite{ChipWhispererPaper}

The original ChipWhisperer generates clock glitches by performing XOR or OR logical operation of the clock signal with a glitch signal. The glitch width is controlled by shifting the phase of the glitch signal and changing its delay. The original ChipWhisperer tool cannot insert voltage glitches.~\cite{ChipWhispererPaper}

O'Flynn founded in 2013 a NewAE Technology Inc. company~\cite{NewAEFounder} that manufactures a product line of side-channel power analysis and fault injection devices named ChipWhisperer. The company designed four ChipWhisperer devices: ChipWhisperer-Nano, ChipWhisperer-Lite, ChipWhisperer-Pro, and ChipWhisperer-Husky. These tools are capable of voltage glitching and power trace capture, and all of them, except for the ChipWhisperer-Nano, are also capable of clock glitching. The voltage glitches are inserted with a crowbar circuit on all of the platforms.~\cite{ChipWhispererWeb}

ChipWhisperer-Nano is a low-cost platform that is mainly intended for side-channel power analysis. The platform is based on a microcontroller that handles communication with a target device and voltage glitch insertion. The microcontroller is used instead of an FPGA to reduce the cost of the platform. The resolution of glitch width and glitch offset is about 8.3~ns, but it suffers with high jitter. The ChipWhisperer-Nano is not capable of measuring the offset in terms of the target's clock cycles.~\cite{ChipWhispererWeb, CWNanoDatasheet}

Both ChipWhisperer-Lite and ChipWhisperer-Pro upgraded their platform from a microcontroller to an FPGA. That allows them to generate almost any target clock frequency, measure glitch offset in the target's clock cycles, and perform voltage and clock glitching with sub 1~ns resolution.~\cite{ChipWhispererWeb, CWLite, CWPro}

ChipWhisperer-Husky is the successor to ChipWhisperer-Lite. The improvements include a higher resolution of the glitch offset and the glitch width. The achievable glitch resolution is 833~ps. ChipWhisperer-Husky also introduces an option to insert multiple independent glitches.~\cite{CWHuskyNotebook}

% Does not fit the header
% NewAE Technology Inc. also manufactures a device named ChipSHOUTER. Unlike the ChipWhisperer product line, the ChipSHOUTER is an electromagnetic fault injection tool.~\cite{ChipShouterWeb}

%---------------------------------------------------------------
\subsection{Tools by Riscure}
%---------------------------------------------------------------

Riscure is another company that provides voltage glitching tools. Their FPGA-based tool named Spider is capable of generating arbitrary waveforms with 4~ns resolution. The spider supports SPI, JTAG, I\textsuperscript{2}C, and UART protocols, and has an SDK that supports Python, Java, and C.~\cite{RiscureSpiderDatasheet}

Another tool offered by Riscure is a VC glitcher. The VC glitcher is built on an FPGA as well. The key feature of the VC glitcher is glitch insertion with a programmable amplitude and duration. Their tool is capable of generating voltage patterns with 2~ns resolution and 500 samples in length.~\cite{RiscureVCGlitcherDatasheet}

%---------------------------------------------------------------
\subsection{Generic Implementation Analysis Toolkit}
%---------------------------------------------------------------

This section is about GIAnT (Generic Implementation Analysis Toolkit) presented in an article~\cite{GIAnTArticle} and later improved as a part of a Ph.D. thesis~\cite{GIAnT}. GIAnT is a low-cost platform for performing non-invasive fault injection. GIAnT is an open source platform that is built around Spartan6 FPGA. The platform is capable of manipulating the target's supply voltage with a 16-bit DAC (Digital-to-Analog Converter) with a 10~ns resolution, measuring the target's power consumption with an ADC (Analog-to-Digital Converter) with the same maximum sample rate of 100~MHz and triggering the glitch by detecting patterns in the target's power consumption.

GIAnT can communicate with smartcards via an ISO 14443 reader, an ISO 7816-compliant smartcard interface, and with other targets via GPIO (General Purpose I/O) pins. GIAnT communicates with the PC via USB with the help of a microcontroller, which is part of the platform. GIAnT can be controlled with an API (Application Programming Interface) that is implemented in C++.

GIAnT is capable of injecting faults with various fault injection techniques. It can perform optical fault injection with a modified electronic flash of a photo-camera. The flash can be replaced with a coil to inject faults by generating an electromagnetic field.

Another type of attack that GIAnT is capable of is clock glitching. The platform inserts clock glitches by generating two clock signals with the second one being slightly offset from the first one. Then a logical AND or a logical OR operation is performed on both clock signals to shorten or lengthen the output clock signal. The operations on the clock signal are performed with external circuitry outside of the FPGA. The resolution of a clock glitch is $1/256$th of a clock period, which is 10~ns.

%---------------------------------------------------------------
\subsection{\textmu-Glitch hardware framework}
%---------------------------------------------------------------

Saß et al.~\cite{OopsIGlitchedItAgainAndMiGlitch} present a hardware framework named \textmu-Glitch capable of injecting multiple consecutive voltage faults. Their framework consists of \enquote{the Clock Generation Unit, the Host Communication Unit, the I/O Buffer Unit, internal Configuration Registers, the Multiple Voltage Fault Unit and the Serial Target I/O Unit}~\cite{OopsIGlitchedItAgainAndMiGlitch}.

Their Multiple Fault Injection Unit consists of multiple Single Fault Units. Single Fault Units take as an input clock, trigger, offset and width, and they have two outputs: Fault Done and Fault Out. The former is asserted for one clock cycle after executing a fault attempt. The latter indicates that a glitch should be inserted.~\cite{OopsIGlitchedItAgainAndMiGlitch}

In the Multiple Fault Injection Unit, Single Fault Units are chained together to provide the capability of inserting multiple faults triggered by a single trigger event. The chaining is done in such a way that the Fault Done signal of the previous Single Fault Unit is connected to the trigger input of the next Single Fault Unit in the chain via a demultiplexer. Demultiplexers are used to control the count of inserted faults. The demultiplexers pass the Fault Done signal to another Single Fault Unit in the chain or interrupt the chain by not passing the Fault Done to the next Single Fault Unit. If a demultiplexer interrupts the chain, it signals the end of all fault injections instead. Fault Out signals are connected with OR logic to provide one output that activates the crowbar circuit.~\cite{OopsIGlitchedItAgainAndMiGlitch}

%---------------------------------------------------------------
\subsection{Summary}
%---------------------------------------------------------------

The capabilities of each platform are summarized in table~\ref{tab:ToolGlitchSummary}

\begin{table}
    \caption{Capabilities of fault injection tools}
    \label{tab:ToolGlitchSummary}
    \centering
    \begin{tabular}{l}
    \begin{tabular}{l|l|l|l}
        Platform name & Offset resolution & Glitch resolution & Glitch types \\
        \hline
        CW-Nano~\cite{CWOverview} & 8.3~ns (high jitter) & 8.3~ns (high jitter) & Crowbar \\
        %\hline
        CW-Lite~\cite{CWOverview} & 0.4\% of clock cycle & 0.4\% of clock cycle & Crowbar, clock \\
        %\hline
        CW-Pro~\cite{CWOverview} & 0.4\% of clock cycle & 0.4\% of clock cycle & Crowbar, clock \\
        %\hline
        CW-Husky~\cite{CWOverview} & 0.833--1.666~ns & 0.833--1.666~ns & Crowbar, clock \\
        Spider~\cite{RiscureSpiderWeb} & N/A & 4~ns & Voltage waveform \\
        %\hline
        VC glitcher~\cite{RiscureVCGlitcherDatasheet} & N/A & 2~ns & Voltage waveform, clock \\
        %\hline
        \textmu-Glitch~\cite{OopsIGlitchedItAgainAndMiGlitch} & N/A & N/A & Crowbar \\
        %\hline
        GIAnT~\cite{GIAnT, GIAnTArticle} & 10~ns & 10~ns (voltage) & Voltage waveform, clock, \\
         & & \quad 0.4\% (clock) & \quad optical, electromagnetic \\
    \end{tabular} \\
    \\
    \begin{tabular}{l|l|l}
        Platform name & Multiple fault injection & Target clock frequency \\
        \hline
        CW-Nano~\cite{CWOverview} & No & 3.75, 7.5, 15, 30 or 60~MHz \\
        %\hline
        CW-Lite~\cite{CWOverview} & Consecutive clock cycles & 5--200~MHz \\
        %\hline
        CW-Pro~\cite{CWOverview} & Consecutive clock cycles & 5--200~MHz \\
        %\hline
        CW-Husky~\cite{CWDocs} & Yes, same glitch type & 10--350~MHz \\
        %\hline
        Spider~\cite{RiscureSpiderDatasheet} & N/A & N/A \\
        %\hline
        VC glitcher~\cite{RiscureVCGlitcherDatasheet} & N/A & N/A \\
        %\hline
        \textmu-Glitch~\cite{OopsIGlitchedItAgainAndMiGlitch} & Yes & N/A \\
        %\hline
        GIAnT~\cite{GIAnTArticle} & Yes, (different glitch types --- N/A) & N/A \\
    \end{tabular} \\
    \\
    \begin{tabular}{l|l}
        Platform name & Trigger types \\
        \hline
        CW-Nano~\cite{CWOverview} & Rising edge \\
        %\hline
        CW-Lite~\cite{CWOverview} & Rising edge \\
        %\hline
        CW-Pro~\cite{CWOverview} & Rising edge, analog pattern, UART, SPI \\
        %\hline
        CW-Husky~\cite{CWOverview} & Edge/level, analog pattern and threshold, UART, ARM trace \\
        %\hline
        Spider~\cite{RiscureSpiderDatasheet} & N/A \\
        %\hline
        VC glitcher~\cite{RiscureVCGlitcherDatasheet} & N/A \\
        %\hline
        \textmu-Glitch~\cite{OopsIGlitchedItAgainAndMiGlitch} & N/A \\
        %\hline
        GIAnT~\cite{GIAnTArticle} & Rising edge \\
    \end{tabular} \\
    \\

    \begin{tabular}{l|l|l}
        Platform name & Programming API & Communication with target \\
        \hline
        CW-Nano~\cite{CWPython, CWOverview} & Python & UART \\
        %\hline
        CW-Lite~\cite{CWPython, CWOverview} & Python & UART \\
        %\hline
        CW-Pro~\cite{CWPython, CWOverview} & Python & UART \\
        %\hline
        CW-Husky~\cite{CWPython, CWOverview} & Python & UART \\
        %\hline
        Spider~\cite{RiscureSpiderDatasheet} & Python, Java, C & SPI, JTAG, I\textsuperscript{2}C, UART \\
        %\hline
        VC glitcher~\cite{RiscureVCGlitcherDatasheet} & N/A & Smart card connector \\
        %\hline
        \textmu-Glitch~\cite{OopsIGlitchedItAgainAndMiGlitch} & N/A & Serial 
        \\
        %\hline
        GIAnT~\cite{GIAnTArticle} & C++ & Serial, ISO 7816, ISO 14443 \\
    \end{tabular} \\
    \end{tabular}
\end{table}