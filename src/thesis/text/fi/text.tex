%---------------------------------------------------------------
\chapter{Fault injection}
\label{chap:FaultInjection}
%---------------------------------------------------------------

A side channel is an unintended exchange of information between a cryptographic device and its environment. Side channel attacks allow to bypass theoretical safety of cryptographic algorithms by exploiting additional information gained via side channels. Side-channel attacks are implementation-specific and device-specific due to the physical nature of side-channel attacks.~\cite{IntroductionToSideChannelAttacks}

There are many side channels. They include:
\begin{itemize}
    \item Timing side channel --- Non-constant execution time of algorithms can reveal information about handled data or secrets.~\cite{TimingSC, IntroductionToSideChannelAttacks}
    \item Power side channel --- Power consumption of a target device can be measured to gain information about processed input or secret.~\cite{DPA, IntroductionToSideChannelAttacks}
    \item Fault side channel --- Consists of injection of faults into a target device and observation of outputs returned by the targeted device.~\cite{DFA, FaultSideChannel}
    \item Electromagnetic side channel --- Electrical devices often generate electromagnetic radiation that can be measured.~\cite{EMSC}
    \item Acoustic side channel --- Acoustic emanations, such as keystrokes, can be used by an attacker to obtain passwords or other secrets.~\cite{KeyboardAcousticEmanations}
\end{itemize}

Hardware attacks can be categorized based on two criteria. Firstly, attacks are divided into categories based on their influence on a target device~\cite{PassiveAndActiveCombinedAttacks}:

\begin{itemize}
    \item Passive attacks -- Such attacks don’t interfere with the target device, and only collect data emitted by the target device.
    \item Active attacks -- Active attacks influence the behavior of the target device.
    \item Passive and active combined attacks -- This is a type of attack that combines both approaches. A disturbance of a target device can leave it vulnerable to a passive attack. For instance, one of the proposed passive and active combined attacks combines fault injection with power analysis.
\end{itemize}

The other possible classification of hardware attacks is based on the required access to the target device~\cite{OpticalFaultInjection}:
\begin{itemize}
    \item Invasive attacks require chip depackaging and passivation layer removal.
    \item Semi-invasive attacks require chip depackaging, but do not involve passivation layer removal. An example of such an attack is an optical fault induction attack.
    \item Non-invasive attacks do not involve chip depackaging, but exploit externally observable information such as electromagnetic emissions or power consumption.
\end{itemize}

Fault injection attacks (FIAs) are active hardware attacks. FIAs introduce faults into running code to reveal secret information by observing the target device's behavior and collecting erroneous outputs. However, some FIAs do not require erroneous outputs to successfully perform the attack. For example, ineffective fault analysis~\cite{IFA} obtains information from fault injections that leave the output unchanged, and safe-error analysis~\cite{SEA} only needs to distinguish between faulted and correct outputs.

Faults can be introduced by various techniques. The techniques range from non-invasive to invasive. Fault injection techniques can be further divided into low-cost and high-cost categories~\cite{FIATheoryPracticeCountermeasures}. Low-cost fault injection techniques include underpowering, voltage glitching, clock glitching, device heating, light radiation, etc. Examples of high-cost techniques are the focused light beam, the laser beam, and the focused ion beam.

\input{text/fi/techniques}

\input{text/fi/attacks}

\input{text/fi/hardware}

\input{text/fi/solutions}