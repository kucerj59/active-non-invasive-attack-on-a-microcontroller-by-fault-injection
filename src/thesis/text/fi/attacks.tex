%---------------------------------------------------------------
\subsection{Advanced Encryption Standard}
%---------------------------------------------------------------

This chapter is based on the Advanced Encryption Standard (AES) publication FIPS 197~\cite{AES}.

AES (Advanced Encryption Standard) is a block cipher. There are three AES ciphers: AES-128, AES-192, and AES-256. The main difference between them is in the key length and the number of rounds, $Nr$, that are performed. In this work, only AES-128, with a key length of 128 bits and 10 rounds, is discussed and is denoted in the rest of the work as AES.

The input of the AES is a 128-bit key and 128 bits of input data. The output of the algorithm is 128 bits of encrypted input data with the specified key. \enquote{Internally, the algorithms for the AES block ciphers are performed on a two-dimensional (four-by-four) array of bytes called the \emph{state}.}\cite{AES}. The arrangement of bytes in the array is shown in figure~\ref{fig:AESState}.

\begin{figure}[h]
    \caption[AES \emph{state} array input and output]{AES \emph{state} array input and output. Source: FIPS 197~\cite{AES}}
    \label{fig:AESState}
    \includesvg[width=\linewidth]{images/AESstate.svg}
\end{figure}

Some transformations work with bytes of \emph{state} as if they were elements of Galois field $GF(2^8)$. Byte $b = {b_7 b_6 b_5 b_4 b_3 b_2 b_1 b_0}$ is represented in $GF(2^8)$ as a polynomial $b_7 x^7 + b_6 x^6 + b_5 x^5 + b_4 x^4 + b_3 x^3 + b_2 x^2 + b_1 x + b_0$ that is marked as $b(x)$. We also use hexadecimal notation with leading zeros to represent elements of $GF(2^8)$. For example, $x + 1$ is represented as $03$.

The addition of two bytes in $GF(2^8)$ is equivalent to performing a \emph{XOR} operation on them, denoted by $\oplus$. The multiplication of bytes $b$ and $c$ consists of their multiplication and modular reduction of the product. The multiplication in $GF(2^8)$ is defined by equation~\ref{eq:GFModularReduction} and is denoted by the symbol $\bullet$.

\begin{equation}
    \label{eq:GFModularReduction}
    a(x) \bullet b(x) = a(x) b(x) \bmod x^8 + x^4 + x^3 + x + 1.
\end{equation}

The cipher is composed of rounds during which four byte-oriented transformations transform the \emph{state}: \emph{SubBytes}, \emph{ShiftRows}, \emph{MixColumns} and \emph{AddRoundKey}. All of the transformations are invertible, so the encrypted text can be decrypted. The order, in which the transformations are called, is displayed in the pseudocode~\ref{alg:AES} where $in$ denotes the input data, $Nr$ represents the number of rounds, and $w$ is the expanded round key. The key expansion is described in the algorithm~\ref{alg:AESKeyExpansion}

\begin{algorithm}
    \caption[AES encryption]{AES encryption. Source: FIPS 197~\cite{AES}, presentation slightly changed}
    \label{alg:AES}
    \begin{algorithmic}[1]
        \Procedure{Cipher}{$in,Nr,w$}
            \State $state \gets in$
            \State $state \gets$ AddRoundKey($state,w[0 \dots 3]$)
            \For{$round$ \textbf{from} $1$ \textbf{to} $Nr - 1$}
                \State $state \gets$ SubBytes($state$)
                \State $state \gets$ ShiftRows($state$)
                \State $state \gets$ MixColumns($state$)
                \State $state \gets$ AddRoundKey($state,w[4 \cdot round \dots 4 \cdot round + 3]$)
            \EndFor
            \State $state \gets$ SubBytes($state$)
            \State $state \gets$ ShiftRows($state$)
            \State $state \gets$ AddRoundeKey($state,w[4 \cdot Nr \dots 4 \cdot Nr + 3]$)
            \State \textbf{return} $state$
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

\emph{AddRoundKey} transformation is a \emph{XOR} operation of the \emph{state} and a round key. The $i$th round is denoted as $K_i$. The round keys are obtained by expanding the key that is equal to $K_0$. The key expansion generates $4 \cdot (Nr + 1)$ four byte words that are denoted as $w_i$ for $0 \leq i < 4 \cdot (Nr + 1)$. The \emph{AddRoundKey} transformation can be written down as equation~\ref{eq:AddRoundKey}.

\begin{equation}
    \label{eq:AddRoundKey}
    \begin{bmatrix}
        s'_{0, c}\\
        s'_{1, c}\\
        s'_{2, c}\\
        s'_{3, c}\\
    \end{bmatrix}
    =
    \begin{bmatrix}
        s_{0, c}\\
        s_{1, c}\\
        s_{2, c}\\
        s_{3, c}\\
    \end{bmatrix}
    \oplus
    \begin{bmatrix}
        \rule[-1ex]{0.5pt}{1.5em} \\
        w_{(4 \cdot round + c)}\\
        \rule[0.5ex]{0.5pt}{1.5em} \\
    \end{bmatrix}
\end{equation}

\emph{SubBytes} is a non-linear transformation that is applied to each byte of the \emph{state}, and that is composed of two transformations. The first transformation, described in equation~\ref{eq:SubBytesInverse}, transforms the byte into its inversion in $GF(2^8)$. The transformation additionally calculates the inverse of zero as zero. The second transformation is a modular reduction given by equation~\ref{eq:SubBytesReduction} in which lower index $i$ denotes the bit of the byte from the least significant bit to the most significant bit.

\begin{equation}
    \label{eq:SubBytesInverse}
    \Tilde{x} = 
    \begin{cases}
        00 & \text{if } x = 00\\
        x^{-1} & \text{if } x \neq 00
    \end{cases}
\end{equation}

\begin{equation}
    \label{eq:SubBytesReduction}
    x'_i = \Tilde{x}_i {\oplus} \Tilde{x}_{(i+4) \bmod 8} {\oplus} \Tilde{x}_{(i+5) \bmod 8} {\oplus} \Tilde{x}_{(i+6) \bmod 8} {\oplus} \Tilde{x}_{(i+7) \bmod 8} {\oplus} b_i
    \quad \text{ where } b = 63 \in GF(2^8)
\end{equation}

Equation~\ref{eq:SubBytesReduction} is written in matrix form in equation~\ref{eq:SubBytesMatrix}, where subscript $i$ denotes the $i$th bit starting from the least significant one. $\ast$ is the multiplication of matrices over GF(2). Later in the work, it is referred to the matrix as $a$ and to the vector as $b$. 

\begin{equation}
    \label{eq:SubBytesMatrix}
    \begin{bmatrix}
    x'_{0}\\
    x'_{1}\\
    x'_{2}\\
    x'_{3}\\
    x'_{4}\\
    x'_{5}\\
    x'_{6}\\
    x'_{7}\\
    \end{bmatrix}
    =
    \begin{bmatrix}
    1 & 0 & 0 & 0 & 1 & 1 & 1 & 1 \\
    1 & 1 & 0 & 0 & 0 & 1 & 1 & 1 \\
    1 & 1 & 1 & 0 & 0 & 0 & 1 & 1 \\
    1 & 1 & 1 & 1 & 0 & 0 & 0 & 1 \\
    1 & 1 & 1 & 1 & 1 & 0 & 0 & 0 \\
    0 & 1 & 1 & 1 & 1 & 1 & 0 & 0 \\
    0 & 0 & 1 & 1 & 1 & 1 & 1 & 0 \\
    0 & 0 & 0 & 1 & 1 & 1 & 1 & 1 \\
    \end{bmatrix}
    \ast
    \begin{bmatrix}
    \Tilde{x}_{0}\\
    \Tilde{x}_{1}\\
    \Tilde{x}_{2}\\
    \Tilde{x}_{3}\\
    \Tilde{x}_{4}\\
    \Tilde{x}_{5}\\
    \Tilde{x}_{6}\\
    \Tilde{x}_{7}\\
    \end{bmatrix}
    \oplus
    \begin{bmatrix}
    1 \\
    1 \\
    0 \\
    0 \\
    0 \\
    1 \\
    1 \\
    0 \\
    \end{bmatrix}
    =
    a
    \ast
    \begin{bmatrix}
        \Tilde{x}_{0}\\
        \Tilde{x}_{1}\\
        \Tilde{x}_{2}\\
        \Tilde{x}_{3}\\
        \Tilde{x}_{4}\\
        \Tilde{x}_{5}\\
        \Tilde{x}_{6}\\
        \Tilde{x}_{7}\\
    \end{bmatrix}
    \oplus
    b
\end{equation}

\emph{ShiftRows} is a transformation that cyclically shifts every row of the \emph{state} array. Bytes in row $r$ are cyclically shifted $r$ bytes to the left, where $r$ is the index of the row. The transformation is described in equation~\ref{eq:ShiftRows}.

\begin{equation}
    \label{eq:ShiftRows}
    s'_{r, c} = s_{r, (c+r) \bmod 4} \quad \text{ for } 0 \leq r < 4 \text{ and } 0 \leq c < 4
\end{equation}

\emph{MixColumns} transformation multiplies every column by a matrix. The equation for the transformation is given in equation~\ref{eq:MixColumns}.

\begin{equation}
    \label{eq:MixColumns}
    \begin{bmatrix}
        s'_{0, c}\\
        s'_{1, c}\\
        s'_{2, c}\\
        s'_{3, c}\\
    \end{bmatrix}
    =
    \begin{bmatrix}
        02 & 03 & 01 & 01\\
        01 & 02 & 03 & 01\\
        01 & 01 & 02 & 03\\
        03 & 01 & 01 & 02\\
    \end{bmatrix}
    \begin{bmatrix}
        s_{0, c}\\
        s_{1, c}\\
        s_{2, c}\\
        s_{3, c}\\
    \end{bmatrix}
    \quad \text{ for } 0 \leq c < 4
\end{equation}

The round keys of the AES are obtained by expanding the \emph{key} of the AES. The key expansion is described by the algorithm~\ref{alg:AESKeyExpansion} where the function \emph{RotWord} rotates a sequence of four bytes to the left by one position, i.e. RotWord($[x_0, x_1, x_2, x_3]$) $= [x_1, x_2, x_3, x_0]$. The operation \emph{SubWord} performs a byte-wise replacement defined by the same equation~\ref{eq:SubBytesMatrix} as in \emph{SubBytes}. Finally, \emph{Rcon} is an array of constants whose values are shown in table~\ref{tab:Rcon}.

\begin{table}[h]
    \caption[Rcon array]{Rcon array. Source: FIPS 197~\cite{AES}}
    \label{tab:Rcon}
    \centering
    \begin{tabular}{c|c|c|c}
        $j$ & $Rcon[j]$ & $j$ & $Rcon[j]$ \\
        \hline
        $1$ & $[01, 00, 00, 00]$ & $6$ & $[20, 00, 00, 00]$ \\
        $2$ & $[02, 00, 00, 00]$ & $7$ & $[40, 00, 00, 00]$ \\
        $3$ & $[04, 00, 00, 00]$ & $8$ & $[80, 00, 00, 00]$ \\
        $4$ & $[08, 00, 00, 00]$ & $9$ & $[1\text{b}, 00, 00, 00]$ \\
        $5$ & $[10, 00, 00, 00]$ & $10$ & $[36, 00, 00, 00]$ \\
    \end{tabular}
    
\end{table}

\begin{algorithm}
    \caption[AES key expansion algorithm]{AES key expansion algorithm. Source: FIPS 197~\cite{AES}, presentation slightly changed}
    \label{alg:AESKeyExpansion}
    \begin{algorithmic}[1]
        \Procedure{KeyExpansion}{$key$}
\State $i \gets 0$
\While{$i \leq Nk - 1$}
    \State $w[i] \gets key[4 \cdot i \dots 4 \cdot i + 3]$
\EndWhile
\While{$i \leq 4 \cdot Nr + 3$}
    \State $temp \gets w[i - 1]$
    \If{$i \bmod Nk = 0$ }
        \State $temp \gets$ SubWord(RotWord($temp$)) $\oplus Rcon[i / Nk]$
    \ElsIf{$Nk > 6 \text{ and } i \bmod Nk = 4$}
        \State $temp \gets$ SubWord($temp$)
    \EndIf
    \State $w[i] \gets w[i - Nk] \oplus temp$
    \State $i \gets i + 1$
\EndWhile
\State \textbf{return} $w$
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

%---------------------------------------------------------------
\subsection[Attack on AES after the 8th \emph{MixColumns}]{Attack on AES after the 8th \emph{MixColumns} \\ and before the 9th \emph{MixColumns}}
\label{subsec:AES89}
%---------------------------------------------------------------

This subsection describes the attack on AES that was proposed by Dussart et al.~\cite{DFAOnAES} The aim of the attack is to recover the last round key $K_{10}$, which is created by the key expansion algorithm. It is possible to perform key expansion in reverse order to recover the AES key from the knowledge of the key $K_{10}$. The fault model for this attack allows an attacker to insert a random fault into the \emph{state} during the execution of AES. This attack requires about 40 to 50 unique pairs of ciphertexts and faulty ciphertexts.

The attack starts with the injection of a fault between the 8th \emph{MixColumns} and the 9th \emph{MixColumns}. For example, let us assume that a fault is inserted into the first byte $s_{0, 0}$ of the AES \emph{state} between the 8th \emph{MixColumns} and the 9th \emph{MixColumns}. The fault caused a one-byte difference that is denoted as $\varepsilon$. After the \emph{ShiftRows} transformation the difference between the unaffected \emph{state} and the erroneous \emph{state} is equal to:

\begin{equation}
    \begin{bmatrix}
        \varepsilon & 00 & 00 & 00\\
        00 & 00 & 00 & 00\\
        00 & 00 & 00 & 00\\
        00 & 00 & 00 & 00\\
    \end{bmatrix}
\end{equation}

The 9th \emph{MixColumns} transformation spreads the error:

\begin{equation}
    \begin{bmatrix}
        02 \bullet \varepsilon & 00 & 00 & 00\\
        01 \bullet \varepsilon & 00 & 00 & 00\\
        01 \bullet \varepsilon & 00 & 00 & 00\\
        03 \bullet \varepsilon & 00 & 00 & 00\\
    \end{bmatrix}
\end{equation}

\emph{AddRoundKey} and \emph{SubBytes}, referenced as function $s(x)$, changes the difference between the original \emph{state} and the erroneous \emph{state} to the one in equation~\ref{eq:DifferentialFaults}. The $\varepsilon'_0, \varepsilon'_1, \varepsilon'_2,$ and $\varepsilon'_3$ represent the differences between the correct ciphertext and the faulty ciphertext at the end of the encryption process. The values of $x_0, x_1, x_2,$ and $x_3$ represent bytes of the AES state before the last \emph{SubBytes} transformation and are unknown to the attacker.

\begin{equation}
    \label{eq:DifferentialFaults}
    \begin{bmatrix}
        \varepsilon'_0 & 00 & 00 & 00\\
        \varepsilon'_1 & 00 & 00 & 00\\
        \varepsilon'_2 & 00 & 00 & 00\\
        \varepsilon'_3 & 00 & 00 & 00
    \end{bmatrix}
    \text{ where }
    \begin{cases}
        \varepsilon'_0 = s(x_0 \oplus 02 \bullet \varepsilon) \oplus s(x_0) \\
        \varepsilon'_1 = s(x_1 \oplus \varepsilon) \oplus s(x_1) \\
        \varepsilon'_2 = s(x_2 \oplus \varepsilon) \oplus s(x_2) \\
        \varepsilon'_3 = s(x_3 \oplus 03 \bullet \varepsilon) \oplus s(x_3) \\
    \end{cases}
\end{equation}

The \emph{ShiftRows} only rearranges the errors:

\begin{equation}
    \label{eq:89AttackShiftRows}
    \begin{bmatrix}
        \varepsilon'_0 & 00 & 00 & 00\\
        00 & 00 & 00 & \varepsilon'_1\\
        00 & 00 & \varepsilon'_2 & 00\\
        00 & \varepsilon'_3 & 00 & 00\\
    \end{bmatrix}
\end{equation}

Finally, the last transformation \emph{AddRoundKey} does not affect the fault matrix. Therefore, the difference between the original \emph{state} and the \emph{state} with an inserted fault is given by equation~\ref{eq:89AttackShiftRows}.

Information about the last round key $K_{10}$ can be gained from the last \emph{SubBytes} transformation by solving a system of equations~\ref{eq:89EqSystem}, where $x_0, x_1, x_2, x_3, \varepsilon$ are unknown variables that represent bytes of the AES \emph{state} before the last \emph{SubBytes} transformation.

\begin{equation}
    \label{eq:89EqSystem}
    \begin{cases}
        \varepsilon'_0 = s(x_0 \oplus 02 \bullet \varepsilon) \oplus s(x_0) \\
        \varepsilon'_1 = s(x_1 \oplus \varepsilon) \oplus s(x_1) \\
        \varepsilon'_2 = s(x_2 \oplus \varepsilon) \oplus s(x_2) \\
        \varepsilon'_3 = s(x_3 \oplus 03 \bullet \varepsilon) \oplus s(x_3) \\
    \end{cases}
\end{equation}

The system of equations~\ref{eq:89EqSystem} can be generalized as a single equation~\ref{eq:89EqSystemSingle}.

\begin{equation}
    \label{eq:89EqSystemSingle}
    \varepsilon' = s(x \oplus c \bullet \varepsilon) \oplus s(x)
    \text{ where } c \in \{01, 02, 03\}
\end{equation}

It is possible to calculate a set of possible values of the inserted fault and four bytes of the key $K_{10}$ from the equation system in equation~\ref{eq:89EqSystem}. Recovery of the key $K_{10}$ consists of three steps. First, a set of possible values of $\varepsilon$ is calculated. Then, a set of possible values of the AES \emph{state} is obtained. Finally, a set of possible values of a key $K_{10}$ is calculated.

A set of possible values of the injected fault $\varepsilon$ is calculated for every valid combination of the differential fault $\varepsilon'$ and the coefficient $c$. The set of possible values of the injected fault $\varepsilon$ for a specific $\varepsilon$ and $c$ is denoted as $E_{c, \varepsilon'}$. The formula for $E_{c, \varepsilon'}$ is written in equation~\ref{eq:Eceps}.

\begin{equation}
    \label{eq:Eceps}
    E_{c, \varepsilon'} = \{ \varepsilon \in GF(2^8): \exists x \in GF(2^8), \varepsilon' = s(x \oplus c \bullet \varepsilon) \oplus s(x) \}
\end{equation}

Then, a new set $E$ is calculated that is an intersection of four $E_{c, \varepsilon'}$ sets. For example, with the same fault location as in the fault propagation example, $E$ would be as shown in equation~\ref{eq:EcepsIntersection}.

\begin{equation}
    \label{eq:EcepsIntersection}
    E = E_{2, \varepsilon'_0} \cap E_{1, \varepsilon'_1} \cap E_{1, \varepsilon'_2} \cap E_{3, \varepsilon'_3}
\end{equation}

The values of the $E$ set are used to calculate the possible values of bytes of the AES \emph{state} before the last \emph{SubBytes} transformation. For every fault $e \in E$, equation~\ref{eq:StateBeforeSubBytes} is solved with the same combinations of $c$ and $\varepsilon'$ as in the last step. Equation~\ref{eq:StateBeforeSubBytes} can be obtained from equation~\ref{eq:89EqSystemSingle} by writing $s(x)$ as $a \ast x^{-1} \oplus b$, which is the definition of \emph{SubBytes} transformation, and performing a few operations so that the equality still hold true.

\begin{equation}
    \label{eq:StateBeforeSubBytes}
    x^2 \bullet (c \bullet \varepsilon)^{-2} \oplus x \bullet (c \bullet \varepsilon)^{-1} = (a^{-1} \ast \varepsilon')^{-1} \bullet (c \bullet \varepsilon)^{-1}
\end{equation}

By substituting $x \bullet (c \bullet \varepsilon)^{-1}$ for $t$ and the right-hand side for $\theta$, we obtain equation~\ref{eq:StateBeforeSubBytesSubst}.

\begin{equation}
    \label{eq:StateBeforeSubBytesSubst}
    t^2 \oplus t = \theta
\end{equation}

Equation~\ref{eq:StateBeforeSubBytesSubst} has two solutions: $\alpha$ and $\beta$. We get $y_1 = \alpha \bullet c \bullet \varepsilon$ and $y_2 = \beta \bullet c \bullet \varepsilon$ after undoing the substitution for $x \bullet (c \bullet \varepsilon)^{-1}$. There are two more solutions $y_3 = 0$ and $y_4 = c \bullet \varepsilon$, if $\theta = 01$.

The possible values of the last round key $K_{10}$ are obtained by calculating $K_{10}[i] = s(y_k) \oplus C'[i]$, where $C'$ is the erroneous ciphertext, $k \in \{1, 2\}$ or $k \in \{1, 2, 3, 4\}$ based on the value $\theta$, and $i$ is the index of a key byte that is being calculated. The XOR operation eliminates the value of the state before the \emph{AddRoundKey} operation and reveals a possible byte of key $K_{10}$.

 The calculations are repeated for other combinations of coefficients $c$, corresponding $\varepsilon'$ differential faults and injected fault values $\varepsilon$ from set $E$ and with different ciphertext pairs, until only one possible value of the four bytes of the key $K_{10}$ is left. This process can be replicated for the remaining twelve $K_{10}$'s bytes to recover the whole last round key $K_{10}$.

The algorithm~\ref{alg:AESKeyRecovery89} describes how to obtain four bytes of the AES' last round key $K_{10}$ using the steps described above. The algorithm takes as input an array of correct ciphertexts $cts$, an array of corresponding erroneous ciphertexts $faulty\_cts$, into which a fault has always been injected into the same column of \emph{state}, and an array of indices $fault\_idx$, which indicate the faulty bytes, for example $[ 0, 13, 10, 7 ]$.

% The main premise of the algorithm~\ref{alg:AESKeyRecovery89} is to find for each faulty ciphertext and a corresponding correct ciphertext all possible values of a fault $\varepsilon$ that could have caused the differences $\varepsilon'_0, \varepsilon'_1, \varepsilon'_2, \varepsilon'_3$ in four of the ciphertext's bytes. Sets of possible key $K_{10}$ values are calculated from the possible fault values, and then an intersection of the key sets is calculated to obtain four bytes of the key $K_{10}$. The algorithm has to be executed four times to recover all 16 bytes of the key $K_{10}$. Each time ciphertext pairs with a fault in a different column are used, and the indices $fault\_idx$ are also different.

\begin{algorithm}
    \caption[Recovery of $K_{10}$ with fault between the 8th and the 9th \emph{MixColumns}]{Recovery of $K_{10}$ with fault between the 8th and the 9th \emph{MixColumns}. Source: article by Dusart et al.~\cite{DFAOnAES}, edited into pseudocode}
    \label{alg:AESKeyRecovery89}
    \begin{algorithmic}[1]
        \Procedure{GetFaultValues}{$c$, $\varepsilon'$}
        \Statex \Comment{Faster way to calculate $\{ \varepsilon \in GF(2^8): \exists x \in GF(2^8), \varepsilon' = s(x \oplus c \bullet \varepsilon) \oplus s(x) \}$}
\State $\varepsilon\_set \gets \{\}$

\For{$t$ \textbf{in} $\{ 01, \dots, 1\text{F},40, \dots, 5\text{F},\text{A}0, \dots, \text{BF},\text{E}0, \dots, \text{FF} \}$}
    \State $\varepsilon\_set \gets \varepsilon\_set \cup (c \bullet (a^{-1} \bullet \varepsilon') \bullet t)^{-1}$
\EndFor
        \EndProcedure
        \item[]
    
        \Procedure{GetKeyValues}{$c$, $\varepsilon$, $\varepsilon'$, $fault$}
\State $\theta \gets ((a^{-1} \ast \varepsilon') \bullet c \bullet \varepsilon)^{-1}$
\Comment{$a$ is the matrix from \emph{SubBytes} definition}
\State $\alpha \gets$ solution of $t^2 \oplus t = \theta$
\State $\beta \gets \alpha \oplus 01$
\If{$\theta \neq 01$}
    \State \textbf{return} $\{
    s(c \bullet \varepsilon \bullet \alpha) \oplus fault,
    s(c \bullet \varepsilon \bullet \beta) \oplus fault \}$
\Else
    \State \textbf{return} $\{
    s(c \bullet \varepsilon \bullet \alpha) \oplus fault,
    s(c \bullet \varepsilon \bullet \beta) \oplus fault,
    b \oplus fault,
    s(c \bullet \varepsilon) \oplus fault \}$
\EndIf
\Comment{$b$ on the line above is the constant from \emph{SubBytes} definition}
        \EndProcedure
        \item[]
        
        \Procedure{Recover89}{$cts$,$faulty\_cts$,$fault\_idx$}
\For{$i$ \textbf{in} $fault\_idx$}
    \Comment{Possible $K_{10}$'s bytes that match all ciphertext pairs}
    \label{alg:line:InitKeys}
    \State $keys[i] \gets \{00, \dots, \text{FF}\}$
\EndFor

\For{$ct, faulty\_ct$ \textbf{in} $cts$, $faulty\_cts$}
    \Comment{For every pair of correct and faulty ciphertext}

    \For{$i$ \textbf{in} $fault\_idx$}
        \Comment{Possible $K_{10}$'s bytes from a single fault}
        \label{alg:line:ForCTStart}
        \State $keys\_single\_set[i] \gets \{\}$
    \EndFor

    \For{$coeffs$ \textbf{in}
        $[  [ 02, 01, 01, 03 ],
            [ 03, 02, 01, 01 ],
            [ 01, 03, 02, 01 ],
            [ 01, 01, 03, 02 ] ]$}
        \Statex \Comment{Coefficients depend on the fault's position in the \emph{state} column}

        \State $\varepsilon\_set \gets \{00, \dots, \text{FF}\}$
        \Comment{Set of possible values of the injected fault}
        \For{$c, i$ \textbf{in} $coeffs, fault\_idx$}
            \State $\varepsilon' \gets faulty\_ct[i] \oplus ct[i]$
            \Comment{Difference between the ciphertexts}
            \State $\varepsilon\_set \gets \varepsilon\_set \cap
            \textsc{GetFaultValues} (c, \varepsilon')$
        \EndFor
        
        \For{$c, i$ \textbf{in} $coeffs, fault\_idx$}
            \Comment{Possible key values based on $\varepsilon$}
            \State $\varepsilon' \gets faulty\_ct[i] \oplus ct[i]$
            \Comment{Difference between the ciphertexts}
            \State $fault \gets faulty\_ct[i]$
            \Comment{Value of the faulty byte in the output of AES}
            \For{$\varepsilon$ \textbf{in} $\varepsilon\_set$}
                \State $keys\_single\_set[i] \gets keys\_single\_set[i] \cup
                \textsc{GetKeyValues} (c, \varepsilon, \varepsilon', fault)$
            \EndFor
        \EndFor
    \EndFor

    \For{$i$ \textbf{in} $fault\_idx$}
        \State $keys[i] \gets keys[i] \cap keys\_single\_set[i]$
    \EndFor
    \label{alg:line:ForCTEnd}
\EndFor
\State \textbf{return} $keys$
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

Once the key $K_{10}$ is recovered, the inverse key expansion algorithm~\ref{alg:AESReverseKey} is performed to gain the key $K_{0}$, i.e. the AES encryption key.

\begin{algorithm}
    \caption[Inverse key expansion algorithm]{Inverse key expansion algorithm. Source: preprint by Dusart et al.~\cite{DFAOnAESPreprint}}
    \label{alg:AESReverseKey}
    \begin{algorithmic}[1]
        \Procedure{InverseKeyExpansion}{$K_{10}$}
\State $i \gets Nb \cdot (Nr + 1) - 1$
\State $j \gets Nk - 1$
\While{$j \geq 0$}
    \State $w[i] \gets K_{10}[4 \cdot j \dots 4 \cdot j + 3]$
    \State $i \gets i - 1$
    \State $j \gets j - 1$
\EndWhile
\While{$i \geq 0$}
    \State $temp \gets w[i + Nk - 1]$
    \If{$i \bmod Nk = 0$}
        \State $temp \gets$ SubWord(RotWord($temp$)) $\oplus Rcon[i / Nk + 1]$
    \ElsIf{$Nk > 6 \text{ and } i \bmod Nk = 4$}
        \State $temp \gets$ SubWord($temp$)
    \EndIf
    \State $w[i] \gets w[i + Nk] \oplus temp$
    \State $i \gets i - 1$
\EndWhile
\State \textbf{return} $w$
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

%---------------------------------------------------------------
\subsection[Attack on AES after the 7th \emph{MixColumns}]{Attacks on AES after the 7th \emph{MixColumns} \\ and before the 8th \emph{MixColumns}}
%---------------------------------------------------------------

Dussart et al.~\cite{DFAOnAES} also presented a fault injection attack on AES that requires only ten faults. The main idea is that the fault inserted between the 7th and 8th \emph{MixColumns} is transformed into four faults after the 8th \emph{MixColumns}. Then the key $K_{10}$ is calculated from the faults in the same way as in the attack described in the previous subsection. The only difference is that the attack is carried out for all four columns of every ciphertext. This is the reason why only ten ciphertext pairs are needed.

To update the algorithm~\ref{alg:AESKeyRecovery89} to perform this improved variant of the attack, the lines~\ref{alg:line:ForCTStart}-\ref{alg:line:ForCTEnd} have to be nested into another for loop. The for loop iterates through every combination of four faulty bytes, i.e. $[[0, 13, 10, 7], [4, 1, 14, 11], [8, 5, 2, 15], [12, 9, 6, 3]]$, that are assigned to $faulty\_idx$ variable. Also, the code on the line~\ref{alg:line:InitKeys} has to initialize all 16 sets. After the modifications, the algorithm returns the whole $K_{10}$ instead of four of its bytes.

Piret and Quisquater~\cite{DFAOnAESUsingSPN} proposed an attack on AES that needs only two erroneous ciphertexts. Based on their observation, two ciphertexts are sufficient in 77\% of the cases. The proposed attack applies to ciphers with a \enquote{Substitution-Permutation structure}~\cite{DFAOnAESUsingSPN} such as AES. These ciphers can be described by equation~\ref{eq:SubstPermStructure}. The last round of the cipher does not contain the $\theta$ layer.

\begin{equation}
    \label{eq:SubstPermStructure}
    \sigma[K_{Nr}] \circ \gamma_Nr \circ (\circ_{r=1}^{Nr-1} \sigma[K_{r}] \circ \theta_r \circ \gamma_r) \circ \sigma[K_{0}]
    \text{ where }
\end{equation}

\begin{itemize}
    \item $Nr$ denotes the number of cipher's rounds.
    \item \enquote{$\gamma_r$ layer consists in the parallel application of $n$ $8 \times 8$ S-boxes (not
necessarily identical)}~\cite{DFAOnAESUsingSPN}.
    \item $\sigma_r$ is a key addition layer $\sigma[k](a) = b \Leftrightarrow b_j = a_j \oplus k_j, 1 \leq j \leq n$.
\end{itemize}

In the case of the AES, the $\theta$ layer is represented by the \emph{ShiftRows} and \emph{MixColumns} transformations, and the $\gamma$ layer consists of the \emph{SubBytes} operation. Because the additional \emph{ShiftRows} transformation \enquote{has no cryptographic significance}~\cite{DFAOnAESUsingSPN}, the attack is applicable to AES.

The main idea behind the attack is to check for all possible values of $K_{10}$, if the difference between the correct ciphertext $C$ and the erroneous ciphertext $C'$ could be generated by a single-byte fault transformed by the $\theta$ layer. The equation is given in equation~\ref{eq:AESSystemImprovedSPN}, where D is a 16-byte array with a single non-zero byte that represents the single-byte fault.~\cite{DFAOnAESUsingSPN}

\begin{equation}
    \label{eq:AESSystemImprovedSPN}
    \theta(D) = \gamma^{-1}_{Nr} \circ \sigma[K_{Nr}](C) \oplus \gamma^{-1}_{Nr} \circ \sigma[K_{Nr}](C')
\end{equation}

Again, the equation can be written as equation~\ref{eq:AESSystemImprovedAES} in the case of the AES. Because the AES has \emph{ShiftRows} operation after the last \emph{MixColumns}, the bytes of the ciphertext pairs and of $K_{10}$ must be reordered accordingly.

\begin{equation}
    \label{eq:AESSystemImprovedAES}
    \text{MixColumns(ShiftRows($D$))} = s^{-1}(K_{10} \oplus C) \oplus s^{-1}(K_{10} \oplus C')
\end{equation}

The attack consists of four phases. In the first phase, faults are injected between the 7th and the 8th \emph{MixColumns} of the AES. Then, the key space is reduced by finding possible values of the key $K_{10}$ byte by byte, if equation~\ref{eq:AESSystemImprovedAES} is satisfied for selected bytes for two pairs of correct and faulty ciphertexts. All obtained values of the key $K_{10}$ are tested, whether they satisfy the same conditions as in the search space reduction step, but this time only for all bytes at once. Keys that fail the test are no longer considered. This is repeated until only one candidate for $K_{10}$ is left. Finally, an inverse key expansion is performed to recover the AES encryption key.~\cite{DFAOnAESUsingSPN}

The algorithm for steps two and three is located in appendix~\ref{app:AttackAlgorithm} due to its length. Instead, the algorithm~\ref{alg:AESRecoveryCourses} shows a simplified version~\cite{DFACourses} of the attack that does not have the key space reduction step. 

The algorithm~\ref{alg:AESRecoveryCourses} solves the system of equations~\ref{eq:AESSystemImproved}~\cite{DFACourses}. The system of equations~\ref{eq:AESSystemImproved} is from article~\cite{AESDFALimits}.

The system of equations~\ref{eq:AESSystemImproved} is similar to equation~\ref{eq:AESSystemImprovedAES}. The difference is that the system of equations~\ref{eq:AESSystemImproved} contains an equation only for four bytes of the last round key $K_{10}$. Therefore, $K_{10}$ is recovered four bytes at a time.

\begin{equation}
    \label{eq:AESSystemImproved}
    \begin{cases}
        2 \bullet \varepsilon = s^{-1}(C_{0, 0} \oplus K_{10, 0, 0}) \oplus s^{-1}(C'_{0, 0} \oplus K_{10, 0, 0}) \\
        \varepsilon = s^{-1}(C_{1, 3} \oplus K_{10, 1, 3}) \oplus s^{-1}(C'_{1, 3} \oplus K_{10, 1, 3}) \\
        \varepsilon = s^{-1}(C_{2, 2} \oplus K_{10, 2, 2}) \oplus s^{-1}(C'_{2, 2} \oplus K_{10, 2, 2}) \\
        3 \bullet \varepsilon = s^{-1}(C_{3, 1} \oplus K_{10, 3, 1}) \oplus s^{-1}(C'_{3, 1} \oplus K_{10, 3, 1}) \\
    \end{cases}
\end{equation}

Algorithm~\ref{alg:AESRecoveryCourses} takes as an input an array of correct ciphertexts $cts$, and an array of corresponding erroneous ciphertexts $faulty\_cts$ that were obtained by injecting a fault between the 7th and the 8th \emph{MixColumns} transformation. It returns the key $K_{10}$.

% TODO
% The equation can be written as a system of equations~\ref{eq:AESSystemImproved}. The system of equation is solved for pairs of correct and erroneous ciphertexts to recover the last round key. The $s(x)$ denotes \emph{SubBytes} transformation, $C$ is the correct ciphertext, and $C'$ is the erroneous ciphertext. The coefficients and positions, which are denoted by the last two numbers in subscript, are different based on the location of the fault.~\cite{AESDFALimits}



\begin{algorithm}
    \caption[Simplified recovery of $K_{10}$ with fault between the 7th and the 8th \emph{MixColumns}]{Simplified recovery of $K_{10}$ with fault between the 7th and the 8th \emph{MixColumns}. Source: tutorial by Shchavleva~\cite{DFACourses}, modified for fault between the 7th and the 8th \emph{MixColumns}}
    \label{alg:AESRecoveryCourses}
    \begin{algorithmic}[1]
        \Procedure{Recover78Simple}{$cts$, $faulty\_cts$}
\State $keys \gets \{[]\}$
\Comment{Possible values of $K_{10}$}
\For{$mask$ \textbf{in} $[[0, 13, 10, 7], [4, 1, 14, 11], [8, 5, 2, 15], [12, 9, 6, 3]]$}
    \Statex \Comment{For indices of every column after \emph{ShiftRows}}
    \For{$ct, faulty\_ct$ \textbf{in} $cts, faulty\_cts$}
        \State $key\_set \gets [\{\}, \{\}, \{\}, \{\}]$
        \Comment{For every ciphertext pair}
        \For{$\varepsilon$ \textbf{in} $\{00, \dots , \text{FF}\}$}
            \Comment{For possible faults that affected the column}
            \For{$coeffs$ \textbf{in} $[[02, 01, 01, 03], [03, 02, 01, 01], [01, 03, 02, 01], [01, 01, 03, 02]]$}
                \For{$i$ \textbf{in} $\{0, 1, 2, 3\}$}
                    \State $c \gets coeffs[i]$
                    \State $ct\_byte \gets ct[mask[i]]$
                    \State $faulty\_byte \gets faulty\_ct[mask[i]]$
                    \State $key\_byte \gets \{ k \in GF(2^8): \varepsilon \bullet c = s^{-1}(k \oplus faulty\_byte) \oplus s^{-1}(k \oplus ct\_byte) \}$
                    \State $key\_set[i] \gets key\_set[i] \cup key\_byte$
                \EndFor
            \EndFor
        \EndFor
        \State $keys \gets {[key, k_0, k_1, k_2, k_3]: key \in keys, k_j \in key\_set[j], j \in {0, \dots, 3}}$
        \Statex \Comment{Extend values in $keys$ by all combinations of key's bytes in $key\_set$}
    \EndFor
\EndFor
\State \textbf{return} ShiftRows($keys[0]$)
\Comment{Arrange the bytes in the correct order}
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

%---------------------------------------------------------------
\subsection[Attacks on debug interface protection of microcontrollers]{Attacks on debug interface protection \\ of microcontrollers}
%---------------------------------------------------------------

% TODO A side-channel timing attack of the msp430 - probably the earliest used, different fault injection technique
% or more attacks from ShapingTheGlitch?

One of the microcontroller's attack vectors is the debug interface. A debug interface enables, for example, firmware programming and firmware reading. Therefore, it is necessary to secure these interfaces.~\cite{ShapingTheGlitch}

There are many different types of microcontrollers made by various manufacturers. Each microcontroller might have its own debug interface protection mechanism. Due to this, attacks on microcontrollers do not apply to all of them. This subsection describes a few security mechanisms that can be defeated with voltage glitching.

STM32F103 microcontroller includes a \emph{Readout Protect} command that enables the read protection feature. When an STM32F103 receives the \emph{Readout Protect} command, it checks a readout protection value before sending back a part of the firmware. This check can be overcome by injecting a glitch during the check.~\cite{ShapingTheGlitch}

Wiersma and Pareja~\cite{MCSafetySecurity} used voltage glitching to defeat different debug interface protections. A different type of microcontroller protects the debug interface with a 128-bit password. Such protections can be overcome with voltage glitching. Attack on another microcontroller tried to change a JTAG configuration fetched from Read-Only memory via voltage glitching. The debug interface of that microcontroller could also be unlocked by affecting the fetched value.

% Wiersma and Pareja also analyzed some protection mechanisms against voltage glitching. They concluded that error correction code memory and lockstep mechanism, which performs code twice in succession and compares the results, are quite effective against voltage glitching.~\cite{MCSafetySecurity}

%---------------------------------------------------------------
\subsection{Buffer overflow attacks with fault injection}
%---------------------------------------------------------------

Nashimoto et al.~\cite{HWBufferOverflow} proposed an attack that combines software and hardware attacks. They demonstrated that the attack defeats input size limitation protections, i.e. functions that can limit the input size. An example of such function is \lstinline{strncpy()}.

Nashimoto et al. used clock glitching fault injection technique to insert faults into \lstinline{strncpy()} function that was running on an 8-bit ATmega163. They were targeting instruction in a for loop that held the number of bytes to be written into a buffer. By skipping the decrement operation of the mentioned variable five times, they were able to write five extra bytes out of the buffer. The caused buffer overflow allowed them to hijack the program flow. They also successfully performed the attack on a 32-bit ARM Cortex-M0+ microcontroller.~\cite{HWBufferOverflow}