# Key recovery from captured ciphertexts

dfa_dusart_78.ipynb     - Recovery of AES key with faults injected between the 7th and the 8th MixColumns based on algorithm by Dusart et al.
dfa_78_piret_78.ipynb   - Recovery of AES key with faults injected between the 7th and the 8th MixColumns based on algorithm by Piret et al.
dfa_78_simple_78.ipynb  - Recovery of AES key with faults injected between the 7th and the 8th MixColumns based on algorithm by Shchavleva
dfa_89.ipynb            - Recovery of AES key with faults injected between the 8th and the 9th MixColumns based on algorithm by Shchavleva

# Fault insertion code

fi_89_cw_nano.ipynb     - Injection of faults between the 8th and the 9th MixColumns using ChipWhisperer-Nano
fi_cmod.ipynb           - Injection of faults between the 7th and 8th or between the 8th and the 9th MixColumns using our platform

Other files contain helper functions.