# Bachelor's thesis
# Active non-invasive attack on a microcontroller by fault injection

Jakub Kučera
kucerj59@fit.cvut.cz
Czech Technical University - Faculty of Information Technology
2024

## Contents of the attachment

README.txt ............................................brief description of the contents
LICENSE.txt .................................................................... license
src
    attacks_on_aes ................................ implementation of attacks on the AES
    cmod ............ source code of our fault injection platform implemented on Cmod S7
        cmod_sw ................................... software source code of our platform
        cmod_hw_and_fw ............... firmware and software source code of our platform
        notebooks ..................... tutorial and examples of how to use our platform
    thesis ............................................ source code of the text in LATEX
collected_data ................ collected ciphertexts and data on glitching success rate
firmware_mega8_aes ...................................... ATmega8L firmware and AES code
cw_nano ................................. firmware for ChipWhisperer-Nano and its target
text
    thesis.pdf ........................................ text of the thesis in PDF format

## End of contents

The cmod_hw_and_fw folder contains source code for Vitis and Vivado tools.