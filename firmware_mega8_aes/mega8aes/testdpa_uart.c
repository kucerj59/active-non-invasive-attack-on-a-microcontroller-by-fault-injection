#include <avr/io.h>
#include <compat/deprecated.h>
#include <avr/sleep.h>                  /* pro _SLEEP() */
#include <avr/interrupt.h>
#include "aes.h"

#define LED_INIT //sbi(DDRB, 0) // DIODA SVITI (PB0)
#define LED_ON //sbi(PORTB, 0) // DIODA SVITI (PB0)
#define LED_OFF //cbi(PORTB, 0) // NESVITI

#define PULSE_PIN 0 // puvodne 1
#define PULSE_INIT sbi(DDRB, PULSE_PIN);
#define PULSE sbi(PORTB, PULSE_PIN); cbi(PORTB, PULSE_PIN) // synchronization pulse
#define PULSE_LONG sbi(PORTB, PULSE_PIN); __asm("nop;"); __asm("nop;"); cbi(PORTB, PULSE_PIN) // synchronization pulse

typedef unsigned char uint8;
typedef unsigned int uint16;
typedef unsigned long uint32;

uint8 pt[16]={
	0x50, 0xdb, 0xe8, 0xe2, 0x4b, 0x20, 0xb1, 0x70, 
	0x8e, 0x1e, 0xfb, 0xe7, 0xb3, 0x1f, 0xda, 0xdb}; //test value
uint8 ct[16];
uint8 key[16]={
	0x12, 0x34, 0x56, 0xab, 0x25, 0x46, 0x00, 0xff,
	0xde, 0xad, 0xbe, 0xef, 0x00, 0xca, 0xfe, 0x34};

/* -------------------------------------------------------------------- */

uint32 rstate = 1234567890L;

uint8 rand_byte(void) {
	rstate = rstate * 1664525 + 1013904223; // http://en.wikipedia.org/wiki/Linear_congruential_generator#LCGs_in_common_use
	return (rstate>>8)&0xff;
}


/* -------------------------------------------------------------------- */
void USART_Init()
{
	// UBRRH = 0;
	// //UBRRL = 0x33;   // 0x33 = 8MHz = ~9600b/s
	// UBRRL = 12;   // 12: 2MHz = 19200 b/s or 1MHz = 9600 b/s !!U2X!!
	// UCSRA = (1<<U2X); //!!

	#if !defined(F_CPU)
		#define F_CPU 1000000UL
	#endif
	#if !defined(USE_U2X)
		#define USE_U2X 1
	#endif
	#if !defined(BAUD)
		#define BAUD 9600UL
	#endif
	#define UBRR_VALUE (F_CPU/(BAUD*8UL*(2UL-USE_U2X)) - 1UL)
	#define UBRRH_VALUE ((UBRR_VALUE>>8)&0x0F)
	#define UBRRL_VALUE (UBRR_VALUE&0xFF)
	// #define STR_HELPER(x) #x
	// #define STR(x) STR_HELPER(x)
	// #pragma message "UBRR_VALUE = " STR(UBRR_VALUE)
	// #pragma message "UBRRH_VALUE = " STR(UBRRH_VALUE)
	// #pragma message "UBRRL_VALUE = " STR(UBRRL_VALUE)
	// #pragma message "F_CPU = " STR(F_CPU)
	// #pragma message "BAUD = " STR(BAUD)
	// #pragma message "USE_U2X = " STR(USE_U2X)
	UBRRH = UBRRH_VALUE;
	UBRRL = UBRRL_VALUE;
	UCSRA = 0;
	UCSRA = (USE_U2X<<U2X);
	/* Enable receiver and transmitter */
	UCSRB = (1<<RXEN)|(1<<TXEN);
	/* Set frame format: 8data, 1stop bit */
	UCSRC = (1<<URSEL)|(3<<UCSZ0); // same as UBRRH, distinguished by URSEL
}


/* -------------------------------------------------------------------- */
void USART_Transmit( uint8 data )
{
	/* Wait for empty transmit buffer */
	while ( !( UCSRA & (1<<UDRE)) )
		;
	/* Put data into buffer, sends the data */
	UDR = data;
}

/* -------------------------------------------------------------------- */
uint8 USART_Receive( void )
{
	/* Wait for data to be received */
	while ( !(UCSRA & (1<<RXC)) )
		;
	/* Get and return received data from buffer */
	return UDR;
}

/* -------------------------------------------------------------------- */

void send_string( char const * s ) {
	while (*s) USART_Transmit(*s++);
}

/* -------------------------------------------------------------------- */
void trochusleep()
{
	uint32 i;
	for (i = 0; i<10000; i++) asm volatile ("nop");
}

/* -------------------------------------------------------------------- */
void pidisleep()
{
	uint32 i;
	for (i = 0; i<3; i++) asm volatile ("nop");
}

/* -------------------------------------------------------------------- */
void vypocet(void)
{
	uint32 i, j;
	uint8 * o;
	//for (i=0; i<sizeof pt; i++) pt[i]=rand_byte();
	cli();
	PULSE; // synchronization pulse
	__asm("nop;");
	__asm("nop;");
	// a sifrujem
	//for (i=0; i<sizeof pt; i++) ct[i]=(pt[i]&key[i&3]);
	o = aes(pt, key);
	for (i=0; i<16; i++) ct[i]=o[i];
	__asm("nop;");
	__asm("nop;");
	PULSE_LONG; // synchronization pulse
	PULSE_LONG; // synchronization pulse
	sei();
}


/* -------------------------------------------------------------------- */
int main(void)
{
	uint8 r, i, j, b;
	uint8 buf[250];
	USART_Init();
	PULSE_INIT;
	LED_INIT;

	vypocet();
	USART_Transmit('C');
	USART_Transmit(':');
	for (i=0; i<sizeof ct; i++) USART_Transmit(ct[i]);
	
	LED_ON;
	send_string("AVRGLI\r\n");
	LED_OFF;
	
	do {
		b = USART_Receive();
		switch(b) {
			case 'p':
				for (i=0; i<sizeof pt; i++) pt[i] = USART_Receive();
				break;
			case 'P':
				for (i=0; i<sizeof pt; i++) USART_Transmit(pt[i]);
				break;
			case 'c':
				for (i=0; i<sizeof ct; i++) ct[i] = USART_Receive();
				break;
			case 'C':
				for (i=0; i<sizeof ct; i++) USART_Transmit(ct[i]);
				break;
			case 'k':
				for (i=0; i<sizeof key; i++) key[i] = USART_Receive();
				break;
			case 'K':
				for (i=0; i<sizeof key; i++) USART_Transmit(key[i]);
				break;
			case 'r':
				vypocet();
				USART_Transmit('.');
				break;
			case 'R':
				for (i=0; i<sizeof pt; i++) pt[i]=rand_byte();
				break;
			case 'b': // call bootloader
				asm ("ldi r31, 0x0e\n clr r30\n ijmp");
			default: send_string("Usage: p<plaintext> / P (read plaintext) / c<ciphertext> / C / k<key> / K /\r\n"
								"r (run, sends '.') / R (random plaintext) / b (bootloader)\r\n");
		}
	} while (b != 3);

	send_string("Break\r\n");
	
	i = 0;
	do {
		LED_OFF;
		b = USART_Receive();
		LED_ON;
		if (b == 0x0d)
		{
			USART_Transmit(0x0a);
			buf[i++] = 0x0a;
		}
		USART_Transmit(b);
		buf[i++] = b;
	}
	while (b != 0x0a);

	for(;;)
	{
		LED_ON;
		trochusleep();
		LED_OFF;
		for (j = 0; j < i; j++)
		{
			USART_Transmit(buf[j]);
			trochusleep();
		}
	}

	LED_OFF;
	LED_ON; // puls spotreby
	LED_OFF;
	vypocet();

	return 0;
}




