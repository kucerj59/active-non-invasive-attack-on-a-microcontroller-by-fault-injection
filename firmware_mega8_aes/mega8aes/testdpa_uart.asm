
testdpa_uart:     file format elf32-avr


Disassembly of section .text:

00000000 <__vectors>:
   0:	12 c0       	rjmp	.+36     	; 0x26 <__ctors_end>
   2:	2c c0       	rjmp	.+88     	; 0x5c <__bad_interrupt>
   4:	2b c0       	rjmp	.+86     	; 0x5c <__bad_interrupt>
   6:	2a c0       	rjmp	.+84     	; 0x5c <__bad_interrupt>
   8:	29 c0       	rjmp	.+82     	; 0x5c <__bad_interrupt>
   a:	28 c0       	rjmp	.+80     	; 0x5c <__bad_interrupt>
   c:	27 c0       	rjmp	.+78     	; 0x5c <__bad_interrupt>
   e:	26 c0       	rjmp	.+76     	; 0x5c <__bad_interrupt>
  10:	25 c0       	rjmp	.+74     	; 0x5c <__bad_interrupt>
  12:	24 c0       	rjmp	.+72     	; 0x5c <__bad_interrupt>
  14:	23 c0       	rjmp	.+70     	; 0x5c <__bad_interrupt>
  16:	22 c0       	rjmp	.+68     	; 0x5c <__bad_interrupt>
  18:	21 c0       	rjmp	.+66     	; 0x5c <__bad_interrupt>
  1a:	20 c0       	rjmp	.+64     	; 0x5c <__bad_interrupt>
  1c:	1f c0       	rjmp	.+62     	; 0x5c <__bad_interrupt>
  1e:	1e c0       	rjmp	.+60     	; 0x5c <__bad_interrupt>
  20:	1d c0       	rjmp	.+58     	; 0x5c <__bad_interrupt>
  22:	1c c0       	rjmp	.+56     	; 0x5c <__bad_interrupt>
  24:	1b c0       	rjmp	.+54     	; 0x5c <__bad_interrupt>

00000026 <__ctors_end>:
  26:	11 24       	eor	r1, r1
  28:	1f be       	out	0x3f, r1	; 63
  2a:	cf e5       	ldi	r28, 0x5F	; 95
  2c:	d4 e0       	ldi	r29, 0x04	; 4
  2e:	de bf       	out	0x3e, r29	; 62
  30:	cd bf       	out	0x3d, r28	; 61

00000032 <__do_copy_data>:
  32:	12 e0       	ldi	r17, 0x02	; 2
  34:	a0 e6       	ldi	r26, 0x60	; 96
  36:	b0 e0       	ldi	r27, 0x00	; 0
  38:	e8 e2       	ldi	r30, 0x28	; 40
  3a:	f5 e0       	ldi	r31, 0x05	; 5
  3c:	02 c0       	rjmp	.+4      	; 0x42 <__SREG__+0x3>
  3e:	05 90       	lpm	r0, Z+
  40:	0d 92       	st	X+, r0
  42:	ac 32       	cpi	r26, 0x2C	; 44
  44:	b1 07       	cpc	r27, r17
  46:	d9 f7       	brne	.-10     	; 0x3e <__SP_H__>

00000048 <__do_clear_bss>:
  48:	22 e0       	ldi	r18, 0x02	; 2
  4a:	ac e2       	ldi	r26, 0x2C	; 44
  4c:	b2 e0       	ldi	r27, 0x02	; 2
  4e:	01 c0       	rjmp	.+2      	; 0x52 <.do_clear_bss_start>

00000050 <.do_clear_bss_loop>:
  50:	1d 92       	st	X+, r1

00000052 <.do_clear_bss_start>:
  52:	ac 35       	cpi	r26, 0x5C	; 92
  54:	b2 07       	cpc	r27, r18
  56:	e1 f7       	brne	.-8      	; 0x50 <.do_clear_bss_loop>
  58:	74 d1       	rcall	.+744    	; 0x342 <main>
  5a:	64 c2       	rjmp	.+1224   	; 0x524 <_exit>

0000005c <__bad_interrupt>:
  5c:	d1 cf       	rjmp	.-94     	; 0x0 <__vectors>

0000005e <rand_byte>:
/* -------------------------------------------------------------------- */

uint32 rstate = 1234567890L;

uint8 rand_byte(void) {
	rstate = rstate * 1664525 + 1013904223; // http://en.wikipedia.org/wiki/Linear_congruential_generator#LCGs_in_common_use
  5e:	20 91 60 00 	lds	r18, 0x0060	; 0x800060 <__DATA_REGION_ORIGIN__>
  62:	30 91 61 00 	lds	r19, 0x0061	; 0x800061 <__DATA_REGION_ORIGIN__+0x1>
  66:	40 91 62 00 	lds	r20, 0x0062	; 0x800062 <__DATA_REGION_ORIGIN__+0x2>
  6a:	50 91 63 00 	lds	r21, 0x0063	; 0x800063 <__DATA_REGION_ORIGIN__+0x3>
  6e:	6d e0       	ldi	r22, 0x0D	; 13
  70:	76 e6       	ldi	r23, 0x66	; 102
  72:	89 e1       	ldi	r24, 0x19	; 25
  74:	90 e0       	ldi	r25, 0x00	; 0
  76:	31 d2       	rcall	.+1122   	; 0x4da <__mulsi3>
  78:	dc 01       	movw	r26, r24
  7a:	cb 01       	movw	r24, r22
  7c:	81 5a       	subi	r24, 0xA1	; 161
  7e:	9c 40       	sbci	r25, 0x0C	; 12
  80:	a1 49       	sbci	r26, 0x91	; 145
  82:	b3 4c       	sbci	r27, 0xC3	; 195
  84:	80 93 60 00 	sts	0x0060, r24	; 0x800060 <__DATA_REGION_ORIGIN__>
  88:	90 93 61 00 	sts	0x0061, r25	; 0x800061 <__DATA_REGION_ORIGIN__+0x1>
  8c:	a0 93 62 00 	sts	0x0062, r26	; 0x800062 <__DATA_REGION_ORIGIN__+0x2>
  90:	b0 93 63 00 	sts	0x0063, r27	; 0x800063 <__DATA_REGION_ORIGIN__+0x3>
	return (rstate>>8)&0xff;
}
  94:	89 2f       	mov	r24, r25
  96:	08 95       	ret

00000098 <USART_Init>:
	// #pragma message "UBRRH_VALUE = " STR(UBRRH_VALUE)
	// #pragma message "UBRRL_VALUE = " STR(UBRRL_VALUE)
	// #pragma message "F_CPU = " STR(F_CPU)
	// #pragma message "BAUD = " STR(BAUD)
	// #pragma message "USE_U2X = " STR(USE_U2X)
	UBRRH = UBRRH_VALUE;
  98:	10 bc       	out	0x20, r1	; 32
	UBRRL = UBRRL_VALUE;
  9a:	8c e0       	ldi	r24, 0x0C	; 12
  9c:	89 b9       	out	0x09, r24	; 9
	UCSRA = 0;
  9e:	1b b8       	out	0x0b, r1	; 11
	UCSRA = (USE_U2X<<U2X);
  a0:	82 e0       	ldi	r24, 0x02	; 2
  a2:	8b b9       	out	0x0b, r24	; 11
	/* Enable receiver and transmitter */
	UCSRB = (1<<RXEN)|(1<<TXEN);
  a4:	88 e1       	ldi	r24, 0x18	; 24
  a6:	8a b9       	out	0x0a, r24	; 10
	/* Set frame format: 8data, 1stop bit */
	UCSRC = (1<<URSEL)|(3<<UCSZ0); // same as UBRRH, distinguished by URSEL
  a8:	86 e8       	ldi	r24, 0x86	; 134
  aa:	80 bd       	out	0x20, r24	; 32
  ac:	08 95       	ret

000000ae <USART_Transmit>:

/* -------------------------------------------------------------------- */
void USART_Transmit( uint8 data )
{
	/* Wait for empty transmit buffer */
	while ( !( UCSRA & (1<<UDRE)) )
  ae:	5d 9b       	sbis	0x0b, 5	; 11
  b0:	fe cf       	rjmp	.-4      	; 0xae <USART_Transmit>
		;
	/* Put data into buffer, sends the data */
	UDR = data;
  b2:	8c b9       	out	0x0c, r24	; 12
  b4:	08 95       	ret

000000b6 <USART_Receive>:

/* -------------------------------------------------------------------- */
uint8 USART_Receive( void )
{
	/* Wait for data to be received */
	while ( !(UCSRA & (1<<RXC)) )
  b6:	5f 9b       	sbis	0x0b, 7	; 11
  b8:	fe cf       	rjmp	.-4      	; 0xb6 <USART_Receive>
		;
	/* Get and return received data from buffer */
	return UDR;
  ba:	8c b1       	in	r24, 0x0c	; 12
}
  bc:	08 95       	ret

000000be <send_string>:

/* -------------------------------------------------------------------- */

void send_string( char const * s ) {
  be:	cf 93       	push	r28
  c0:	df 93       	push	r29
  c2:	ec 01       	movw	r28, r24
	while (*s) USART_Transmit(*s++);
  c4:	89 91       	ld	r24, Y+
  c6:	88 23       	and	r24, r24
  c8:	11 f0       	breq	.+4      	; 0xce <send_string+0x10>
  ca:	f1 df       	rcall	.-30     	; 0xae <USART_Transmit>
  cc:	fb cf       	rjmp	.-10     	; 0xc4 <send_string+0x6>
}
  ce:	df 91       	pop	r29
  d0:	cf 91       	pop	r28
  d2:	08 95       	ret

000000d4 <trochusleep>:

/* -------------------------------------------------------------------- */
void trochusleep()
{
  d4:	80 e0       	ldi	r24, 0x00	; 0
  d6:	90 e0       	ldi	r25, 0x00	; 0
	uint32 i;
	for (i = 0; i<10000; i++) asm volatile ("nop");
  d8:	00 00       	nop
  da:	01 96       	adiw	r24, 0x01	; 1
  dc:	80 31       	cpi	r24, 0x10	; 16
  de:	27 e2       	ldi	r18, 0x27	; 39
  e0:	92 07       	cpc	r25, r18
  e2:	d1 f7       	brne	.-12     	; 0xd8 <trochusleep+0x4>
}
  e4:	08 95       	ret

000000e6 <pidisleep>:

/* -------------------------------------------------------------------- */
void pidisleep()
{
	uint32 i;
	for (i = 0; i<3; i++) asm volatile ("nop");
  e6:	00 00       	nop
  e8:	00 00       	nop
  ea:	00 00       	nop
  ec:	08 95       	ret

000000ee <vypocet>:
void vypocet(void)
{
	uint32 i, j;
	uint8 * o;
	//for (i=0; i<sizeof pt; i++) pt[i]=rand_byte();
	cli();
  ee:	f8 94       	cli
	PULSE; // synchronization pulse
  f0:	c0 9a       	sbi	0x18, 0	; 24
  f2:	c0 98       	cbi	0x18, 0	; 24
	__asm("nop;");
  f4:	00 00       	nop
	__asm("nop;");
  f6:	00 00       	nop
	// a sifrujem
	//for (i=0; i<sizeof pt; i++) ct[i]=(pt[i]&key[i&3]);
	o = aes(pt, key);
  f8:	64 e6       	ldi	r22, 0x64	; 100
  fa:	70 e0       	ldi	r23, 0x00	; 0
  fc:	84 e7       	ldi	r24, 0x74	; 116
  fe:	90 e0       	ldi	r25, 0x00	; 0
 100:	ef d0       	rcall	.+478    	; 0x2e0 <aes>
 102:	ec e3       	ldi	r30, 0x3C	; 60
 104:	f2 e0       	ldi	r31, 0x02	; 2
	for (i=0; i<16; i++) ct[i]=o[i];
 106:	dc 01       	movw	r26, r24
 108:	2d 91       	ld	r18, X+
 10a:	cd 01       	movw	r24, r26
 10c:	21 93       	st	Z+, r18
 10e:	b2 e0       	ldi	r27, 0x02	; 2
 110:	ec 34       	cpi	r30, 0x4C	; 76
 112:	fb 07       	cpc	r31, r27
 114:	c1 f7       	brne	.-16     	; 0x106 <vypocet+0x18>
	__asm("nop;");
 116:	00 00       	nop
	__asm("nop;");
 118:	00 00       	nop
	PULSE_LONG; // synchronization pulse
 11a:	c0 9a       	sbi	0x18, 0	; 24
 11c:	00 00       	nop
 11e:	00 00       	nop
 120:	c0 98       	cbi	0x18, 0	; 24
	PULSE_LONG; // synchronization pulse
 122:	c0 9a       	sbi	0x18, 0	; 24
 124:	00 00       	nop
 126:	00 00       	nop
 128:	c0 98       	cbi	0x18, 0	; 24
	sei();
 12a:	78 94       	sei
 12c:	08 95       	ret

0000012e <addRoundKey>:

	return state;
}

void addRoundKey(void)
{
 12e:	ac e2       	ldi	r26, 0x2C	; 44
 130:	b2 e0       	ldi	r27, 0x02	; 2
 132:	ec e4       	ldi	r30, 0x4C	; 76
 134:	f2 e0       	ldi	r31, 0x02	; 2
	int i;

	for(i=0; i < 16; i++)
	{
		state[i] ^= key[i];
 136:	8d 91       	ld	r24, X+
 138:	90 81       	ld	r25, Z
 13a:	89 27       	eor	r24, r25
 13c:	81 93       	st	Z+, r24

void addRoundKey(void)
{
	int i;

	for(i=0; i < 16; i++)
 13e:	82 e0       	ldi	r24, 0x02	; 2
 140:	ec 35       	cpi	r30, 0x5C	; 92
 142:	f8 07       	cpc	r31, r24
 144:	c1 f7       	brne	.-16     	; 0x136 <addRoundKey+0x8>
	{
		state[i] ^= key[i];
	}
}
 146:	08 95       	ret

00000148 <subBytes>:

/* subBytes
 * Table Lookup
 */
void subBytes(void)
{
 148:	ac e4       	ldi	r26, 0x4C	; 76
 14a:	b2 e0       	ldi	r27, 0x02	; 2
	int i;

	for(i = 0; i < 16; i++)
	{
		state[i] = sbox[ state[i] ];
 14c:	ec 91       	ld	r30, X
 14e:	f0 e0       	ldi	r31, 0x00	; 0
 150:	ef 56       	subi	r30, 0x6F	; 111
 152:	ff 4f       	sbci	r31, 0xFF	; 255
 154:	80 81       	ld	r24, Z
 156:	8d 93       	st	X+, r24
 */
void subBytes(void)
{
	int i;

	for(i = 0; i < 16; i++)
 158:	82 e0       	ldi	r24, 0x02	; 2
 15a:	ac 35       	cpi	r26, 0x5C	; 92
 15c:	b8 07       	cpc	r27, r24
 15e:	b1 f7       	brne	.-20     	; 0x14c <subBytes+0x4>
	{
		state[i] = sbox[ state[i] ];
	}
}
 160:	08 95       	ret

00000162 <shiftRows>:
void shiftRows(void)
{
	byte temp;

	//Row 2
	temp = state[1]; state[1] = state[5]; state[5] = state[9];
 162:	ec e4       	ldi	r30, 0x4C	; 76
 164:	f2 e0       	ldi	r31, 0x02	; 2
 166:	81 81       	ldd	r24, Z+1	; 0x01
 168:	95 81       	ldd	r25, Z+5	; 0x05
 16a:	91 83       	std	Z+1, r25	; 0x01
 16c:	91 85       	ldd	r25, Z+9	; 0x09
 16e:	95 83       	std	Z+5, r25	; 0x05
    state[9] = state[13]; state[13] = temp;
 170:	95 85       	ldd	r25, Z+13	; 0x0d
 172:	91 87       	std	Z+9, r25	; 0x09
 174:	85 87       	std	Z+13, r24	; 0x0d
	//Row 3
	temp = state[10]; state[10] = state[2]; state[2] = temp;
 176:	82 85       	ldd	r24, Z+10	; 0x0a
 178:	92 81       	ldd	r25, Z+2	; 0x02
 17a:	92 87       	std	Z+10, r25	; 0x0a
 17c:	82 83       	std	Z+2, r24	; 0x02
	temp = state[14]; state[14] = state[6]; state[6] = temp;
 17e:	86 85       	ldd	r24, Z+14	; 0x0e
 180:	96 81       	ldd	r25, Z+6	; 0x06
 182:	96 87       	std	Z+14, r25	; 0x0e
 184:	86 83       	std	Z+6, r24	; 0x06
	//Row 4
	temp = state[3]; state[3] = state[15]; state[15] = state[11]; 
 186:	83 81       	ldd	r24, Z+3	; 0x03
 188:	97 85       	ldd	r25, Z+15	; 0x0f
 18a:	93 83       	std	Z+3, r25	; 0x03
 18c:	93 85       	ldd	r25, Z+11	; 0x0b
 18e:	97 87       	std	Z+15, r25	; 0x0f
    state[11] = state[7]; state[7] = temp;
 190:	97 81       	ldd	r25, Z+7	; 0x07
 192:	93 87       	std	Z+11, r25	; 0x0b
 194:	87 83       	std	Z+7, r24	; 0x07
 196:	08 95       	ret

00000198 <xtime_vuln>:
/* MixColumns Shortcut 
 * Vunerable to timing attack, must add nop
 */
byte xtime_vuln(byte x)
{
	return (x & 0x80) ? ((x << 1) ^ 0x1b) : (x<<1);
 198:	87 ff       	sbrs	r24, 7
 19a:	04 c0       	rjmp	.+8      	; 0x1a4 <xtime_vuln+0xc>
 19c:	88 0f       	add	r24, r24
 19e:	9b e1       	ldi	r25, 0x1B	; 27
 1a0:	89 27       	eor	r24, r25
 1a2:	08 95       	ret
 1a4:	88 0f       	add	r24, r24
}
 1a6:	08 95       	ret

000001a8 <xtime>:
{
	//register byte msk = -((x & 0x80) >> 7);
	register byte msk;
	msk = x & 0x80;
	msk >>= 7;
	msk = -msk;
 1a8:	98 2f       	mov	r25, r24
 1aa:	99 0f       	add	r25, r25
 1ac:	99 0b       	sbc	r25, r25
	msk &= 0x1b;
	return (x << 1) ^ msk;
 1ae:	88 0f       	add	r24, r24
 1b0:	9b 71       	andi	r25, 0x1B	; 27
	//return (byte)(x << (byte)1) ^ (byte)((byte)0x1b & (byte)- ((byte)(x & (byte)0x80) >> (byte)7));
	//return (x << 1) ^ (0x1b & - ((x & 0x80) >> 7));
}
 1b2:	89 27       	eor	r24, r25
 1b4:	08 95       	ret

000001b6 <mixColumns>:

/* Mix Columns */
void mixColumns(void)
{
 1b6:	af 92       	push	r10
 1b8:	bf 92       	push	r11
 1ba:	cf 92       	push	r12
 1bc:	df 92       	push	r13
 1be:	ef 92       	push	r14
 1c0:	ff 92       	push	r15
 1c2:	0f 93       	push	r16
 1c4:	1f 93       	push	r17
 1c6:	cf 93       	push	r28
 1c8:	df 93       	push	r29
 1ca:	cc e4       	ldi	r28, 0x4C	; 76
 1cc:	d2 e0       	ldi	r29, 0x02	; 2
 1ce:	8c e5       	ldi	r24, 0x5C	; 92
 1d0:	c8 2e       	mov	r12, r24
 1d2:	82 e0       	ldi	r24, 0x02	; 2
 1d4:	d8 2e       	mov	r13, r24
	byte i, a, b, c, d, e;
	
	/* Process a column at a time */
	for(i = 0; i < 16; i+=4)
	{
		a = state[i]; b = state[i+1]; c = state[i+2]; d = state[i+3];
 1d6:	b8 80       	ld	r11, Y
 1d8:	e9 80       	ldd	r14, Y+1	; 0x01
 1da:	fa 80       	ldd	r15, Y+2	; 0x02
 1dc:	0b 81       	ldd	r16, Y+3	; 0x03
		e = a ^ b ^ c ^ d;
 1de:	8b 2d       	mov	r24, r11
 1e0:	8e 25       	eor	r24, r14
 1e2:	af 2c       	mov	r10, r15
 1e4:	a0 26       	eor	r10, r16
 1e6:	1a 2d       	mov	r17, r10
 1e8:	18 27       	eor	r17, r24
		state[i]   ^= e ^ xtime(a^b);
 1ea:	de df       	rcall	.-68     	; 0x1a8 <xtime>
 1ec:	81 27       	eor	r24, r17
 1ee:	8b 25       	eor	r24, r11
 1f0:	88 83       	st	Y, r24
		state[i+1] ^= e ^ xtime(b^c);
 1f2:	8e 2d       	mov	r24, r14
 1f4:	8f 25       	eor	r24, r15
 1f6:	d8 df       	rcall	.-80     	; 0x1a8 <xtime>
 1f8:	81 27       	eor	r24, r17
 1fa:	e8 26       	eor	r14, r24
 1fc:	e9 82       	std	Y+1, r14	; 0x01
		state[i+2] ^= e ^ xtime(c^d);
 1fe:	8a 2d       	mov	r24, r10
 200:	d3 df       	rcall	.-90     	; 0x1a8 <xtime>
 202:	81 27       	eor	r24, r17
 204:	f8 26       	eor	r15, r24
 206:	fa 82       	std	Y+2, r15	; 0x02
		state[i+3] ^= e ^ xtime(d^a);
 208:	8b 2d       	mov	r24, r11
 20a:	80 27       	eor	r24, r16
 20c:	cd df       	rcall	.-102    	; 0x1a8 <xtime>
 20e:	81 27       	eor	r24, r17
 210:	80 27       	eor	r24, r16
 212:	8b 83       	std	Y+3, r24	; 0x03
 214:	24 96       	adiw	r28, 0x04	; 4
void mixColumns(void)
{
	byte i, a, b, c, d, e;
	
	/* Process a column at a time */
	for(i = 0; i < 16; i+=4)
 216:	cc 16       	cp	r12, r28
 218:	dd 06       	cpc	r13, r29
 21a:	e9 f6       	brne	.-70     	; 0x1d6 <mixColumns+0x20>
		state[i]   ^= e ^ xtime(a^b);
		state[i+1] ^= e ^ xtime(b^c);
		state[i+2] ^= e ^ xtime(c^d);
		state[i+3] ^= e ^ xtime(d^a);
	}
}
 21c:	df 91       	pop	r29
 21e:	cf 91       	pop	r28
 220:	1f 91       	pop	r17
 222:	0f 91       	pop	r16
 224:	ff 90       	pop	r15
 226:	ef 90       	pop	r14
 228:	df 90       	pop	r13
 22a:	cf 90       	pop	r12
 22c:	bf 90       	pop	r11
 22e:	af 90       	pop	r10
 230:	08 95       	ret

00000232 <computeKey>:
 * 1 5  9 13
 * 2 6 10 14
 * 3 7 11 15
 */
void computeKey(byte rcon)
{
 232:	0f 93       	push	r16
 234:	1f 93       	push	r17
 236:	cf 93       	push	r28
 238:	df 93       	push	r29
	byte buf0, buf1, buf2, buf3;
	buf0 = sbox[ key[13] ];
 23a:	ec e2       	ldi	r30, 0x2C	; 44
 23c:	f2 e0       	ldi	r31, 0x02	; 2
 23e:	c5 85       	ldd	r28, Z+13	; 0x0d
	buf1 = sbox[ key[14] ];
 240:	16 85       	ldd	r17, Z+14	; 0x0e
 242:	21 2f       	mov	r18, r17
 244:	30 e0       	ldi	r19, 0x00	; 0
 246:	2f 56       	subi	r18, 0x6F	; 111
 248:	3f 4f       	sbci	r19, 0xFF	; 255
 24a:	d9 01       	movw	r26, r18
 24c:	7c 91       	ld	r23, X
	buf2 = sbox[ key[15] ];
 24e:	07 85       	ldd	r16, Z+15	; 0x0f
 250:	20 2f       	mov	r18, r16
 252:	30 e0       	ldi	r19, 0x00	; 0
 254:	2f 56       	subi	r18, 0x6F	; 111
 256:	3f 4f       	sbci	r19, 0xFF	; 255
 258:	d9 01       	movw	r26, r18
 25a:	6c 91       	ld	r22, X
	buf3 = sbox[ key[12] ];
 25c:	d4 85       	ldd	r29, Z+12	; 0x0c
 25e:	2d 2f       	mov	r18, r29
 260:	30 e0       	ldi	r19, 0x00	; 0
 262:	2f 56       	subi	r18, 0x6F	; 111
 264:	3f 4f       	sbci	r19, 0xFF	; 255
 266:	d9 01       	movw	r26, r18
 268:	5c 91       	ld	r21, X
 * 3 7 11 15
 */
void computeKey(byte rcon)
{
	byte buf0, buf1, buf2, buf3;
	buf0 = sbox[ key[13] ];
 26a:	2c 2f       	mov	r18, r28
 26c:	30 e0       	ldi	r19, 0x00	; 0
 26e:	2f 56       	subi	r18, 0x6F	; 111
 270:	3f 4f       	sbci	r19, 0xFF	; 255
	buf1 = sbox[ key[14] ];
	buf2 = sbox[ key[15] ];
	buf3 = sbox[ key[12] ];

	key[0] ^= buf0 ^ rcon;
 272:	d9 01       	movw	r26, r18
 274:	2c 91       	ld	r18, X
 276:	82 27       	eor	r24, r18
 278:	20 81       	ld	r18, Z
 27a:	82 27       	eor	r24, r18
 27c:	80 83       	st	Z, r24
    key[1] ^= buf1;
 27e:	21 81       	ldd	r18, Z+1	; 0x01
 280:	72 27       	eor	r23, r18
 282:	71 83       	std	Z+1, r23	; 0x01
    key[2] ^= buf2;
 284:	22 81       	ldd	r18, Z+2	; 0x02
 286:	62 27       	eor	r22, r18
 288:	62 83       	std	Z+2, r22	; 0x02
    key[3] ^= buf3;
 28a:	23 81       	ldd	r18, Z+3	; 0x03
 28c:	52 27       	eor	r21, r18
 28e:	53 83       	std	Z+3, r21	; 0x03

	key[4] ^= key[0];
 290:	24 81       	ldd	r18, Z+4	; 0x04
 292:	82 27       	eor	r24, r18
 294:	84 83       	std	Z+4, r24	; 0x04
	key[5] ^= key[1];
 296:	25 81       	ldd	r18, Z+5	; 0x05
 298:	72 27       	eor	r23, r18
 29a:	75 83       	std	Z+5, r23	; 0x05
	key[6] ^= key[2];
 29c:	26 81       	ldd	r18, Z+6	; 0x06
 29e:	62 27       	eor	r22, r18
 2a0:	66 83       	std	Z+6, r22	; 0x06
	key[7] ^= key[3];
 2a2:	27 81       	ldd	r18, Z+7	; 0x07
 2a4:	52 27       	eor	r21, r18
 2a6:	57 83       	std	Z+7, r21	; 0x07

	key[8]  ^= key[4];
 2a8:	20 85       	ldd	r18, Z+8	; 0x08
 2aa:	82 27       	eor	r24, r18
 2ac:	80 87       	std	Z+8, r24	; 0x08
	key[9]  ^= key[5];
 2ae:	21 85       	ldd	r18, Z+9	; 0x09
 2b0:	47 2f       	mov	r20, r23
 2b2:	42 27       	eor	r20, r18
 2b4:	41 87       	std	Z+9, r20	; 0x09
	key[10] ^= key[6];
 2b6:	22 85       	ldd	r18, Z+10	; 0x0a
 2b8:	36 2f       	mov	r19, r22
 2ba:	32 27       	eor	r19, r18
 2bc:	32 87       	std	Z+10, r19	; 0x0a
	key[11] ^= key[7];
 2be:	23 85       	ldd	r18, Z+11	; 0x0b
 2c0:	95 2f       	mov	r25, r21
 2c2:	92 27       	eor	r25, r18
 2c4:	93 87       	std	Z+11, r25	; 0x0b

	key[12] ^= key[8];
 2c6:	8d 27       	eor	r24, r29
 2c8:	84 87       	std	Z+12, r24	; 0x0c
	key[13] ^= key[9];
 2ca:	4c 27       	eor	r20, r28
 2cc:	45 87       	std	Z+13, r20	; 0x0d
	key[14] ^= key[10];
 2ce:	31 27       	eor	r19, r17
 2d0:	36 87       	std	Z+14, r19	; 0x0e
	key[15] ^= key[11];
 2d2:	90 27       	eor	r25, r16
 2d4:	97 87       	std	Z+15, r25	; 0x0f
}
 2d6:	df 91       	pop	r29
 2d8:	cf 91       	pop	r28
 2da:	1f 91       	pop	r17
 2dc:	0f 91       	pop	r16
 2de:	08 95       	ret

000002e0 <aes>:

byte state[16];
static byte key[16];

byte * aes(byte *in, byte *skey)
{
 2e0:	0f 93       	push	r16
 2e2:	1f 93       	push	r17
 2e4:	cf 93       	push	r28
 2e6:	df 93       	push	r29
 2e8:	cc e4       	ldi	r28, 0x4C	; 76
 2ea:	d2 e0       	ldi	r29, 0x02	; 2
 2ec:	8b 01       	movw	r16, r22
 2ee:	ac e2       	ldi	r26, 0x2C	; 44
 2f0:	b2 e0       	ldi	r27, 0x02	; 2
 2f2:	9c 01       	movw	r18, r24
 2f4:	20 5f       	subi	r18, 0xF0	; 240
 2f6:	3f 4f       	sbci	r19, 0xFF	; 255
	int i;

	for(i=0; i < 16; i++)
	{
		state[i] = in[i];
 2f8:	fc 01       	movw	r30, r24
 2fa:	41 91       	ld	r20, Z+
 2fc:	cf 01       	movw	r24, r30
 2fe:	49 93       	st	Y+, r20
		key[i] = skey[i];
 300:	f8 01       	movw	r30, r16
 302:	41 91       	ld	r20, Z+
 304:	8f 01       	movw	r16, r30
 306:	4d 93       	st	X+, r20

byte * aes(byte *in, byte *skey)
{
	int i;

	for(i=0; i < 16; i++)
 308:	82 17       	cp	r24, r18
 30a:	93 07       	cpc	r25, r19
 30c:	a9 f7       	brne	.-22     	; 0x2f8 <aes+0x18>
	{
		state[i] = in[i];
		key[i] = skey[i];
	}
	addRoundKey();
 30e:	0f df       	rcall	.-482    	; 0x12e <addRoundKey>
 310:	c4 e8       	ldi	r28, 0x84	; 132
 312:	d0 e0       	ldi	r29, 0x00	; 0

	for(i = 0; i < 9; i++)
	{
 		subBytes();
 314:	19 df       	rcall	.-462    	; 0x148 <subBytes>
		shiftRows();
 316:	25 df       	rcall	.-438    	; 0x162 <shiftRows>
		mixColumns();
 318:	4e df       	rcall	.-356    	; 0x1b6 <mixColumns>
		computeKey(rcon[i]);
 31a:	89 91       	ld	r24, Y+
 31c:	8a df       	rcall	.-236    	; 0x232 <computeKey>
		addRoundKey();
 31e:	07 df       	rcall	.-498    	; 0x12e <addRoundKey>
		state[i] = in[i];
		key[i] = skey[i];
	}
	addRoundKey();

	for(i = 0; i < 9; i++)
 320:	f0 e0       	ldi	r31, 0x00	; 0
 322:	cd 38       	cpi	r28, 0x8D	; 141
 324:	df 07       	cpc	r29, r31
 326:	b1 f7       	brne	.-20     	; 0x314 <aes+0x34>
		mixColumns();
		computeKey(rcon[i]);
		addRoundKey();
	}

	subBytes();
 328:	0f df       	rcall	.-482    	; 0x148 <subBytes>
	shiftRows();
 32a:	1b df       	rcall	.-458    	; 0x162 <shiftRows>
	computeKey(rcon[i]);
 32c:	80 91 8d 00 	lds	r24, 0x008D	; 0x80008d <rcon+0x9>
 330:	80 df       	rcall	.-256    	; 0x232 <computeKey>
	addRoundKey();
 332:	fd de       	rcall	.-518    	; 0x12e <addRoundKey>

	return state;
}
 334:	8c e4       	ldi	r24, 0x4C	; 76
 336:	92 e0       	ldi	r25, 0x02	; 2
 338:	df 91       	pop	r29
 33a:	cf 91       	pop	r28
 33c:	1f 91       	pop	r17
 33e:	0f 91       	pop	r16
 340:	08 95       	ret

00000342 <main>:
}


/* -------------------------------------------------------------------- */
int main(void)
{
 342:	cf 93       	push	r28
 344:	df 93       	push	r29
 346:	cd b7       	in	r28, 0x3d	; 61
 348:	de b7       	in	r29, 0x3e	; 62
 34a:	ca 5f       	subi	r28, 0xFA	; 250
 34c:	d1 09       	sbc	r29, r1
 34e:	0f b6       	in	r0, 0x3f	; 63
 350:	f8 94       	cli
 352:	de bf       	out	0x3e, r29	; 62
 354:	0f be       	out	0x3f, r0	; 63
 356:	cd bf       	out	0x3d, r28	; 61
	uint8 r, i, j, b;
	uint8 buf[250];
	USART_Init();
 358:	9f de       	rcall	.-706    	; 0x98 <USART_Init>
	PULSE_INIT;
 35a:	b8 9a       	sbi	0x17, 0	; 23
	LED_INIT;

	vypocet();
 35c:	c8 de       	rcall	.-624    	; 0xee <vypocet>
	USART_Transmit('C');
 35e:	83 e4       	ldi	r24, 0x43	; 67
 360:	a6 de       	rcall	.-692    	; 0xae <USART_Transmit>
	USART_Transmit(':');
 362:	8a e3       	ldi	r24, 0x3A	; 58
 364:	a4 de       	rcall	.-696    	; 0xae <USART_Transmit>
 366:	2c e4       	ldi	r18, 0x4C	; 76
 368:	e2 2e       	mov	r14, r18
 36a:	22 e0       	ldi	r18, 0x02	; 2
 36c:	f2 2e       	mov	r15, r18
 36e:	0c e3       	ldi	r16, 0x3C	; 60
 370:	12 e0       	ldi	r17, 0x02	; 2
	for (i=0; i<sizeof ct; i++) USART_Transmit(ct[i]);
 372:	f8 01       	movw	r30, r16
 374:	81 91       	ld	r24, Z+
 376:	8f 01       	movw	r16, r30
 378:	9a de       	rcall	.-716    	; 0xae <USART_Transmit>
 37a:	e0 16       	cp	r14, r16
 37c:	f1 06       	cpc	r15, r17
 37e:	c9 f7       	brne	.-14     	; 0x372 <main+0x30>
	
	LED_ON;
	send_string("AVRGLI\r\n");
 380:	81 e9       	ldi	r24, 0x91	; 145
 382:	91 e0       	ldi	r25, 0x01	; 1
 384:	9c de       	rcall	.-712    	; 0xbe <send_string>
 386:	84 e7       	ldi	r24, 0x74	; 116
 388:	c8 2e       	mov	r12, r24
 38a:	80 e0       	ldi	r24, 0x00	; 0
 38c:	d8 2e       	mov	r13, r24
 38e:	94 e8       	ldi	r25, 0x84	; 132
 390:	a9 2e       	mov	r10, r25
 392:	90 e0       	ldi	r25, 0x00	; 0
 394:	b9 2e       	mov	r11, r25
	LED_OFF;
	
	do {
		b = USART_Receive();
 396:	8f de       	rcall	.-738    	; 0xb6 <USART_Receive>
 398:	18 2f       	mov	r17, r24
		switch(b) {
 39a:	82 36       	cpi	r24, 0x62	; 98
 39c:	09 f4       	brne	.+2      	; 0x3a0 <main+0x5e>
 39e:	68 c0       	rjmp	.+208    	; 0x470 <__stack+0x11>
 3a0:	90 f4       	brcc	.+36     	; 0x3c6 <main+0x84>
 3a2:	8b 34       	cpi	r24, 0x4B	; 75
 3a4:	09 f4       	brne	.+2      	; 0x3a8 <main+0x66>
 3a6:	51 c0       	rjmp	.+162    	; 0x44a <__DATA_REGION_LENGTH__+0x4a>
 3a8:	30 f4       	brcc	.+12     	; 0x3b6 <main+0x74>
 3aa:	83 34       	cpi	r24, 0x43	; 67
 3ac:	09 f0       	breq	.+2      	; 0x3b0 <main+0x6e>
 3ae:	63 c0       	rjmp	.+198    	; 0x476 <__stack+0x17>
 3b0:	0c e3       	ldi	r16, 0x3C	; 60
 3b2:	12 e0       	ldi	r17, 0x02	; 2
 3b4:	37 c0       	rjmp	.+110    	; 0x424 <__DATA_REGION_LENGTH__+0x24>
 3b6:	80 35       	cpi	r24, 0x50	; 80
 3b8:	11 f1       	breq	.+68     	; 0x3fe <main+0xbc>
 3ba:	82 35       	cpi	r24, 0x52	; 82
 3bc:	09 f0       	breq	.+2      	; 0x3c0 <main+0x7e>
 3be:	5b c0       	rjmp	.+182    	; 0x476 <__stack+0x17>
 3c0:	04 e7       	ldi	r16, 0x74	; 116
 3c2:	10 e0       	ldi	r17, 0x00	; 0
 3c4:	4c c0       	rjmp	.+152    	; 0x45e <__DATA_REGION_LENGTH__+0x5e>
 3c6:	8b 36       	cpi	r24, 0x6B	; 107
 3c8:	b1 f1       	breq	.+108    	; 0x436 <__DATA_REGION_LENGTH__+0x36>
 3ca:	30 f4       	brcc	.+12     	; 0x3d8 <main+0x96>
 3cc:	83 36       	cpi	r24, 0x63	; 99
 3ce:	09 f0       	breq	.+2      	; 0x3d2 <main+0x90>
 3d0:	52 c0       	rjmp	.+164    	; 0x476 <__stack+0x17>
 3d2:	0c e3       	ldi	r16, 0x3C	; 60
 3d4:	12 e0       	ldi	r17, 0x02	; 2
 3d6:	1e c0       	rjmp	.+60     	; 0x414 <__DATA_REGION_LENGTH__+0x14>
 3d8:	80 37       	cpi	r24, 0x70	; 112
 3da:	39 f0       	breq	.+14     	; 0x3ea <main+0xa8>
 3dc:	82 37       	cpi	r24, 0x72	; 114
 3de:	09 f0       	breq	.+2      	; 0x3e2 <main+0xa0>
 3e0:	4a c0       	rjmp	.+148    	; 0x476 <__stack+0x17>
				break;
			case 'K':
				for (i=0; i<sizeof key; i++) USART_Transmit(key[i]);
				break;
			case 'r':
				vypocet();
 3e2:	85 de       	rcall	.-758    	; 0xee <vypocet>
				USART_Transmit('.');
 3e4:	8e e2       	ldi	r24, 0x2E	; 46
 3e6:	63 de       	rcall	.-826    	; 0xae <USART_Transmit>
				break;
 3e8:	d6 cf       	rjmp	.-84     	; 0x396 <main+0x54>
 3ea:	04 e7       	ldi	r16, 0x74	; 116
 3ec:	10 e0       	ldi	r17, 0x00	; 0
	
	do {
		b = USART_Receive();
		switch(b) {
			case 'p':
				for (i=0; i<sizeof pt; i++) pt[i] = USART_Receive();
 3ee:	63 de       	rcall	.-826    	; 0xb6 <USART_Receive>
 3f0:	f8 01       	movw	r30, r16
 3f2:	81 93       	st	Z+, r24
 3f4:	8f 01       	movw	r16, r30
 3f6:	ae 16       	cp	r10, r30
 3f8:	bf 06       	cpc	r11, r31
 3fa:	c9 f7       	brne	.-14     	; 0x3ee <main+0xac>
 3fc:	cc cf       	rjmp	.-104    	; 0x396 <main+0x54>
 3fe:	04 e7       	ldi	r16, 0x74	; 116
 400:	10 e0       	ldi	r17, 0x00	; 0
				break;
			case 'P':
				for (i=0; i<sizeof pt; i++) USART_Transmit(pt[i]);
 402:	f8 01       	movw	r30, r16
 404:	81 91       	ld	r24, Z+
 406:	8f 01       	movw	r16, r30
 408:	52 de       	rcall	.-860    	; 0xae <USART_Transmit>
 40a:	f0 e0       	ldi	r31, 0x00	; 0
 40c:	04 38       	cpi	r16, 0x84	; 132
 40e:	1f 07       	cpc	r17, r31
 410:	c1 f7       	brne	.-16     	; 0x402 <__DATA_REGION_LENGTH__+0x2>
 412:	c1 cf       	rjmp	.-126    	; 0x396 <main+0x54>
				break;
			case 'c':
				for (i=0; i<sizeof ct; i++) ct[i] = USART_Receive();
 414:	50 de       	rcall	.-864    	; 0xb6 <USART_Receive>
 416:	f8 01       	movw	r30, r16
 418:	81 93       	st	Z+, r24
 41a:	8f 01       	movw	r16, r30
 41c:	ee 16       	cp	r14, r30
 41e:	ff 06       	cpc	r15, r31
 420:	c9 f7       	brne	.-14     	; 0x414 <__DATA_REGION_LENGTH__+0x14>
 422:	b9 cf       	rjmp	.-142    	; 0x396 <main+0x54>
				break;
			case 'C':
				for (i=0; i<sizeof ct; i++) USART_Transmit(ct[i]);
 424:	f8 01       	movw	r30, r16
 426:	81 91       	ld	r24, Z+
 428:	8f 01       	movw	r16, r30
 42a:	41 de       	rcall	.-894    	; 0xae <USART_Transmit>
 42c:	f2 e0       	ldi	r31, 0x02	; 2
 42e:	0c 34       	cpi	r16, 0x4C	; 76
 430:	1f 07       	cpc	r17, r31
 432:	c1 f7       	brne	.-16     	; 0x424 <__DATA_REGION_LENGTH__+0x24>
 434:	b0 cf       	rjmp	.-160    	; 0x396 <main+0x54>
 436:	04 e6       	ldi	r16, 0x64	; 100
 438:	10 e0       	ldi	r17, 0x00	; 0
				break;
			case 'k':
				for (i=0; i<sizeof key; i++) key[i] = USART_Receive();
 43a:	3d de       	rcall	.-902    	; 0xb6 <USART_Receive>
 43c:	f8 01       	movw	r30, r16
 43e:	81 93       	st	Z+, r24
 440:	8f 01       	movw	r16, r30
 442:	ce 16       	cp	r12, r30
 444:	df 06       	cpc	r13, r31
 446:	c9 f7       	brne	.-14     	; 0x43a <__DATA_REGION_LENGTH__+0x3a>
 448:	a6 cf       	rjmp	.-180    	; 0x396 <main+0x54>
 44a:	04 e6       	ldi	r16, 0x64	; 100
 44c:	10 e0       	ldi	r17, 0x00	; 0
				break;
			case 'K':
				for (i=0; i<sizeof key; i++) USART_Transmit(key[i]);
 44e:	f8 01       	movw	r30, r16
 450:	81 91       	ld	r24, Z+
 452:	8f 01       	movw	r16, r30
 454:	2c de       	rcall	.-936    	; 0xae <USART_Transmit>
 456:	c0 16       	cp	r12, r16
 458:	d1 06       	cpc	r13, r17
 45a:	c9 f7       	brne	.-14     	; 0x44e <__DATA_REGION_LENGTH__+0x4e>
 45c:	9c cf       	rjmp	.-200    	; 0x396 <main+0x54>
			case 'r':
				vypocet();
				USART_Transmit('.');
				break;
			case 'R':
				for (i=0; i<sizeof pt; i++) pt[i]=rand_byte();
 45e:	ff dd       	rcall	.-1026   	; 0x5e <rand_byte>
 460:	f8 01       	movw	r30, r16
 462:	81 93       	st	Z+, r24
 464:	8f 01       	movw	r16, r30
 466:	f0 e0       	ldi	r31, 0x00	; 0
 468:	04 38       	cpi	r16, 0x84	; 132
 46a:	1f 07       	cpc	r17, r31
 46c:	c1 f7       	brne	.-16     	; 0x45e <__DATA_REGION_LENGTH__+0x5e>
 46e:	93 cf       	rjmp	.-218    	; 0x396 <main+0x54>
				break;
			case 'b': // call bootloader
				asm ("ldi r31, 0x0e\n clr r30\n ijmp");
 470:	fe e0       	ldi	r31, 0x0E	; 14
 472:	ee 27       	eor	r30, r30
 474:	09 94       	ijmp
			default: send_string("Usage: p<plaintext> / P (read plaintext) / c<ciphertext> / C / k<key> / K /\r\n"
 476:	8a e9       	ldi	r24, 0x9A	; 154
 478:	91 e0       	ldi	r25, 0x01	; 1
 47a:	21 de       	rcall	.-958    	; 0xbe <send_string>
								"r (run, sends '.') / R (random plaintext) / b (bootloader)\r\n");
		}
	} while (b != 3);
 47c:	13 30       	cpi	r17, 0x03	; 3
 47e:	09 f0       	breq	.+2      	; 0x482 <__stack+0x23>
 480:	8a cf       	rjmp	.-236    	; 0x396 <main+0x54>

	send_string("Break\r\n");
 482:	84 e2       	ldi	r24, 0x24	; 36
 484:	92 e0       	ldi	r25, 0x02	; 2
 486:	1b de       	rcall	.-970    	; 0xbe <send_string>
	
	i = 0;
 488:	10 e0       	ldi	r17, 0x00	; 0
 48a:	ce 01       	movw	r24, r28
 48c:	01 96       	adiw	r24, 0x01	; 1
 48e:	7c 01       	movw	r14, r24
	do {
		LED_OFF;
		b = USART_Receive();
 490:	12 de       	rcall	.-988    	; 0xb6 <USART_Receive>
 492:	08 2f       	mov	r16, r24
		LED_ON;
		if (b == 0x0d)
 494:	8d 30       	cpi	r24, 0x0D	; 13
 496:	41 f4       	brne	.+16     	; 0x4a8 <__stack+0x49>
		{
			USART_Transmit(0x0a);
 498:	8a e0       	ldi	r24, 0x0A	; 10
 49a:	09 de       	rcall	.-1006   	; 0xae <USART_Transmit>
			buf[i++] = 0x0a;
 49c:	f7 01       	movw	r30, r14
 49e:	e1 0f       	add	r30, r17
 4a0:	f1 1d       	adc	r31, r1
 4a2:	8a e0       	ldi	r24, 0x0A	; 10
 4a4:	80 83       	st	Z, r24
 4a6:	1f 5f       	subi	r17, 0xFF	; 255
		}
		USART_Transmit(b);
 4a8:	80 2f       	mov	r24, r16
 4aa:	01 de       	rcall	.-1022   	; 0xae <USART_Transmit>
		buf[i++] = b;
 4ac:	dd 24       	eor	r13, r13
 4ae:	d3 94       	inc	r13
 4b0:	d1 0e       	add	r13, r17
 4b2:	f7 01       	movw	r30, r14
 4b4:	e1 0f       	add	r30, r17
 4b6:	f1 1d       	adc	r31, r1
 4b8:	00 83       	st	Z, r16
	}
	while (b != 0x0a);
 4ba:	0a 30       	cpi	r16, 0x0A	; 10
 4bc:	11 f0       	breq	.+4      	; 0x4c2 <__stack+0x63>
		{
			USART_Transmit(0x0a);
			buf[i++] = 0x0a;
		}
		USART_Transmit(b);
		buf[i++] = b;
 4be:	1d 2d       	mov	r17, r13
 4c0:	e7 cf       	rjmp	.-50     	; 0x490 <__stack+0x31>
	while (b != 0x0a);

	for(;;)
	{
		LED_ON;
		trochusleep();
 4c2:	08 de       	rcall	.-1008   	; 0xd4 <trochusleep>
		LED_OFF;
		for (j = 0; j < i; j++)
 4c4:	87 01       	movw	r16, r14
 4c6:	80 2f       	mov	r24, r16
 4c8:	8e 19       	sub	r24, r14
 4ca:	8d 15       	cp	r24, r13
 4cc:	d0 f7       	brcc	.-12     	; 0x4c2 <__stack+0x63>
		{
			USART_Transmit(buf[j]);
 4ce:	f8 01       	movw	r30, r16
 4d0:	81 91       	ld	r24, Z+
 4d2:	8f 01       	movw	r16, r30
 4d4:	ec dd       	rcall	.-1064   	; 0xae <USART_Transmit>
			trochusleep();
 4d6:	fe dd       	rcall	.-1028   	; 0xd4 <trochusleep>
 4d8:	f6 cf       	rjmp	.-20     	; 0x4c6 <__stack+0x67>

000004da <__mulsi3>:
 4da:	db 01       	movw	r26, r22
 4dc:	8f 93       	push	r24
 4de:	9f 93       	push	r25
 4e0:	0b d0       	rcall	.+22     	; 0x4f8 <__muluhisi3>
 4e2:	bf 91       	pop	r27
 4e4:	af 91       	pop	r26
 4e6:	a2 9f       	mul	r26, r18
 4e8:	80 0d       	add	r24, r0
 4ea:	91 1d       	adc	r25, r1
 4ec:	a3 9f       	mul	r26, r19
 4ee:	90 0d       	add	r25, r0
 4f0:	b2 9f       	mul	r27, r18
 4f2:	90 0d       	add	r25, r0
 4f4:	11 24       	eor	r1, r1
 4f6:	08 95       	ret

000004f8 <__muluhisi3>:
 4f8:	09 d0       	rcall	.+18     	; 0x50c <__umulhisi3>
 4fa:	a5 9f       	mul	r26, r21
 4fc:	90 0d       	add	r25, r0
 4fe:	b4 9f       	mul	r27, r20
 500:	90 0d       	add	r25, r0
 502:	a4 9f       	mul	r26, r20
 504:	80 0d       	add	r24, r0
 506:	91 1d       	adc	r25, r1
 508:	11 24       	eor	r1, r1
 50a:	08 95       	ret

0000050c <__umulhisi3>:
 50c:	a2 9f       	mul	r26, r18
 50e:	b0 01       	movw	r22, r0
 510:	b3 9f       	mul	r27, r19
 512:	c0 01       	movw	r24, r0
 514:	a3 9f       	mul	r26, r19
 516:	01 d0       	rcall	.+2      	; 0x51a <__umulhisi3+0xe>
 518:	b2 9f       	mul	r27, r18
 51a:	70 0d       	add	r23, r0
 51c:	81 1d       	adc	r24, r1
 51e:	11 24       	eor	r1, r1
 520:	91 1d       	adc	r25, r1
 522:	08 95       	ret

00000524 <_exit>:
 524:	f8 94       	cli

00000526 <__stop_program>:
 526:	ff cf       	rjmp	.-2      	; 0x526 <__stop_program>
