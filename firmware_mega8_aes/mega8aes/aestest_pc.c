#include <stdio.h>
#include "aes.h"


typedef unsigned char uint8;
typedef unsigned int uint16;
typedef unsigned long uint32;

uint8 pt[16]={
	0x50, 0xdb, 0xe8, 0xe2, 0x4b, 0x20, 0xb1, 0x70, 
	0x8e, 0x1e, 0xfb, 0xe7, 0xb3, 0x1f, 0xda, 0xdb};
uint8 ct[16];
uint8 key[16]={
	0x12, 0x34, 0x56, 0xab, 0x25, 0x46, 0x00, 0xff,
	0xde, 0xad, 0xbe, 0xef, 0x00, 0xca, 0xfe, 0x34};


void fprinthex (FILE * f, unsigned char * tx, int n) {
	int i;
	for (i = 0; i < n; i++) fprintf(f, "0x%02x ", (unsigned)tx[i]);
}

/* -------------------------------------------------------------------- */
void vypocet(void)
{
	uint32 i, j;
	uint8 * o;
	//for (i=0; i<sizeof pt; i++) pt[i]=rand_byte();
	// a sifrujem
	//for (i=0; i<sizeof pt; i++) ct[i]=(pt[i]&key[i&3]);
	o = aes(pt, key);
	for (i=0; i<16; i++) ct[i]=o[i];
}


/* -------------------------------------------------------------------- */
int main(void)
{
	uint8 r, i, j, b;
	uint8 buf[250];
	fprinthex(stdout, key, 16);
	fprinthex(stdout, pt, 16);
	vypocet();
	fprinthex(stdout, ct, 16);
	return 0;
}




